<?php 
include ( 'includes/header.php' ); ?>
<?php
$itemCount = $_POST['itemCount'];

$totalAmount = $_POST['shipping'] + $_POST['tax'];
$order = new stdClass();
$order->totalAmount = $_POST['shipping'] + $_POST['tax'];
$order->InstallmentId = '';
$order->ContactId = '';
$order->AccountId = '';
$order->BillingStreet = '';
$order->BillingZipcode = '';
$order->BillingCity = '';
$order->ShippingStreet = '';
$order->ShippingZipcode = '';
$order->ShippingCity = '';
$order->products = array();
for($i = 1;$i <= $itemCount;$i++) {
	$productAmount = $_POST['item_price_'.$i] * $_POST['item_quantity_'.$i];
	$product = new stdClass();
	$product->quantity = $_POST['item_quantity_'.$i];
	$product->name = $_POST['item_name_'.$i];
	$product->price = $_POST['item_price_'.$i];
	$product->subtotal = $productAmount;
	$options = array();
	$explodedOptions = explode(',',$_POST['item_options_'.$i]);
	foreach($explodedOptions AS $option) {
		$keyValue = explode(':', $option);
		$options[trim($keyValue[0])] = trim($keyValue[1]);
	}
	$product->options = $options;
	$totalAmount += $productAmount;
	array_push($order->products, $product);
}
$order->totalAmount = $totalAmount;
?>
    <?
    $header = '4kvBBiGesxiyuiHuQir7Vtyclz7x5ytxzvAB9p24GK5HUF3eKwYMMQSVCfk30ADKP10MyXseXhhjFjltE81soQKWBFUeJcMSTriUJyC38g3jXrgEiOQ0UxxRLLodoAEyEzL0MhKOjzV5lSla2vJz51EvYFCSRbhWnyu3Mijv7hCXWkcCBH0j8DVwHGehYoLuPVJ56NMw';
    $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/cpm/v1.0/PaymentMethods');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, false);
    //curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
    //$headers['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . $header));
    $curl_response = curl_exec($curl);
    if ($curl_response === false) {
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additioanl info: ' . var_export($info));
    }
    curl_close($curl);
    $temp = json_decode ( $curl_response );
    $paymentMethods = $temp->PaymentMethods;
?>
        <section class="about-us-st section-padding">
            <div class="container">
                <h2>Checkout</h2>
                <div class="col-sm-10 col-sm-offset-2">
                    <div id="feedback">
                    </div>
                </div>
                <form class="form-horizontal" method="post" action="" id="donateform">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Firstname</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="" name="firstname" id="firstname" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Lastname</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="" name="Lastname" id="Lastname" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="email" value="" id="email" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Shipping Address</label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Street" class="form-control" name="BillingStreet" value="" id="BillingStreet" />
                        </div>
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <input type="text" placeholder="Zipcode" class="form-control" name="BillingZipcode" value="" id="BillingZipcode" />
                        </div>
                        <div class="col-sm-6">
                            <input type="text" placeholder="City" class="form-control" name="BillingCity" value="" id="BillingCity" />
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control currency" name="selectedAmount" value="<?php echo $totalAmount; ?>" id="selectedAmount" />
                        <label for="inputEmail3" class="col-sm-2 control-label">Payment method</label>
                        <div class="col-sm-10">
                            <select name="paymentmethod" id="paymentmethod" class="form-control">
                                <option> -- Select your Payment Method -- </option>
                                <?php
                                foreach ( $paymentMethods as $key=>$value ) {
                                    echo "<option>" . $value->Name ."</option>";
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="pull-right">
                                <input type="hidden" name="type" id="type" value="order" />
                                <input type="submit" class="btn theme-btn" id="donate" value="Pay" />
                                <div id="donate-load" style="display:none;">
                                    <img src="images/loading.gif" /> Processing order.
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
            </div>
        </section>
        <?php include ( 'includes/footer.php' ); ?>
        <script type="text/javascript">
        $(document).ready(function() {
            $('form#donateform').submit(function(event) {
                $('input#donate').hide();
                $('#donate-load').toggle();
                var datatemp = $('form#donateform').serialize();
                // var js_data = '<?php echo json_encode($order); ?>';
                // var order = JSON.parse(js_data);
                // order.BillingStreet = $('BillingStreet').val();
                // order.BillingZipcode = $('BillingZipcode').val();
                // order.BillingCity = $('BillingCity').val();
                // order.ShippingStreet = $('BillingStreet').val();
                // order.ShippingZipcode = $('BillingZipcode').val();
                // order.ShippingCity = $('BillingCity').val();
                //var payment = {firstname: $('#firstname').val(), Lastname: $('#Lastname').val(), email: $('#email').val(), BillingStreet: $('#BillingStreet').val(), BillingZipcode: $('#BillingZipcode').val(), BillingCity: $('#BillingCity').val(), selectedAmount: $('#selectedAmount').val(), paymentmethod: $('#paymentmethod').val()}
                //var datatemp = {type: 'order', order: JSON.stringify(order), payment: payment}
                console.log('datatemp'+JSON.stringify(datatemp));
                $.ajax({
                    type: 'POST',
                    url: 'post.php',
                    crossDomain: true,
                    data: datatemp,
                    success: function(data) {
                        console.log('response: ' + data);
                        var obj = jQuery.parseJSON(data);

                        if (obj.IsSuccess) {
                            //$(location).attr('href', obj.RedirectURL);
                                               		var installmentId = obj.Payment.InstallmentId;
                                                	var contactId = obj.Payer.ContactId;
                                                	var accountId = obj.Payer.AccountId;
                                       		// var installmentId = 'a0n0Y0000018om2QAA';
                                         //         	var contactId = '0030Y00000SshwqQAB';
                                         //         	var accountId = '0010Y00000YEIg6QAH';

                            var js_data = '<?php echo json_encode($order); ?>';
                            var order = JSON.parse(js_data );
                            order.InstallmentId = installmentId;
                            order.ContactId = contactId;
                            order.AccountId = accountId;
                            order.BillingStreet = $('#BillingStreet').val();
                            order.BillingZipcode = $('#BillingZipcode').val();
                            order.BillingCity = $('#BillingCity').val();
                            order.ShippingStreet = $('#BillingStreet').val();
                            order.ShippingZipcode = $('#BillingZipcode').val();
                            order.ShippingCity = $('#BillingCity').val();

					$.ajax({
                                          type: 'POST',
                                          url: 'post.php',
                                          crossDomain: true,
                                          data: {type: 'registerOrder', order: JSON.stringify(order)},
                                          success: function (data) {
                                              console.log('data: '+data);
                                              var orderResult = jQuery.parseJSON( data );
                                              //console.log('response:'+obj);
                                              if (orderResult.isSuccess ) {
                                                  $(location).attr('href', obj.RedirectURL);
                                              } else {
                                                  $('#feedback').html ('');
                                                  $('#feedback').html ('<span style="color:#ff0000;">Something went wrong: ' + orderResult.error+ '</span>');
                                                  $('input#donate').show();
                                                  $('#donate-load').toggle();
                                              }
                                          }
                                      });
                        } else {
                            $('#feedback').html('');
                            $('#feedback').html('<span style="color:#ff0000;">Something went wrong: ' + obj.Error.error_message + '</span>');
                            $('input#donate').show();
                            $('#donate-load').toggle();
                        }
                    }
                });

                event.preventDefault();

            });
        });
        </script>
        </body>

        </html>