<?php include ( 'includes/header.php' ); ?>

        <div class="container">
            <div class="row error-404-content">
                <div class="col col-xs-12">
                    <center>
                        <img src="images/error-404.png" alt class="img img-reponsive">
                        <h1>Error 404</h1>
                        <p>The page you requested can’t be found in our system</p>
                        <a href="index.php" class="btn theme-btn"><i class="fa fa-angle-left"></i> Go Back</a>
                    </center>
                </div>
            </div>
        </div> <!-- end container -->
    
<?php include ( 'includes/footer.php' ); ?>
    
</body>
</html>
