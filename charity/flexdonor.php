<?php include ( 'includes/header.php' ); 


?>
    <?php
    if ( $_GET['cid'] != '' ) {
        //die ('jorrit');
        $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/v1/FlexDonor/' . $_GET['cid']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, false);
        //curl_setopt(£curl, CURLOPT_POSTFIELDS, JSON_ENCODE(£array)) ;
        //£headers['Authorization: OAuth '.£_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . $header));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $temp = json_decode ( $curl_response );
    }   
    ?>
    <!-- start latest-causes -->
        <section class="latest-causes section-padding" id="recent">
            <div class="container">
                <div class="row section-title-s2">
                    <div class="col col-xs-12">
                        <h2><span>recent</span> causes</h2>
                    </div>
                </div> <!-- end section-title -->
                <div id="allcauses">
                    Hi <?php echo $temp->firstName; ?>,
                    <br /><br />
                    Please choose the project you want to contribute to. We ask for a donation of £ 5,-
                    <br /><br />
                    When you click the button, we will automatically charge your account (<?php echo $temp->accountNumber;?>) using Direct Debit
                    <br /><br />
                    <div class="row content">
                        <div class="col col-md-4 col-xs-6">
                            <div class="title">
                                <ul>
                                    <li><h3>Kids playground</h3></li>
                                    <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                                </ul>                            
                            </div>
                            <div class="img-progress">
                                <div class="img-holder">
                                    <img src="images/latest-causes/img-1.jpg" alt class="img img-responsive">
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" data-percent="85"></div>
                                </div>
                            </div>
                            <div class="donate-amount">
                                <ul>
                                    <li>Raised: <span>£52,872</span> / £70,000</li>
                                    <li><i class="fa fa-clock-o"></i> 95 days</li>
                                </ul>
                            </div>
                            <div class="text">
                                <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                                <a href="#" class="btn theme-btn"><?php echo CLICK;?></a>
                            </div>
                        </div>

                        <div class="col col-md-4 col-xs-6">
                            <div class="title">
                                <ul>
                                    <li><h3>Kids playground</h3></li>
                                    <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                                </ul>                            
                            </div>
                            <div class="img-progress">
                                <div class="img-holder">
                                    <img src="images/latest-causes/img-2.jpg" alt class="img img-responsive">
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" data-percent="55"></div>
                                </div>
                            </div>
                            <div class="donate-amount">
                                <ul>
                                    <li>Raised: <span>£52,872</span> / £70,000</li>
                                    <li><i class="fa fa-clock-o"></i> 95 days</li>
                                </ul>
                            </div>
                            <div class="text">
                                <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                                <a href="#" class="btn theme-btn"><?php echo CLICK;?></a>
                            </div>
                        </div>

                        <div class="col col-md-4 col-xs-6">
                            <div class="title">
                                <ul>
                                    <li><h3>Kids playground</h3></li>
                                    <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                                </ul>                            
                            </div>
                            <div class="img-progress">
                                <div class="img-holder">
                                    <img src="images/latest-causes/img-3.jpg" alt class="img img-responsive">
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" data-percent="25"></div>
                                </div>
                            </div>
                            <div class="donate-amount">
                                <ul>
                                    <li>Raised: <span>£52,872</span> / £70,000</li>
                                    <li><i class="fa fa-clock-o"></i> 95 days</li>
                                </ul>
                            </div>
                            <div class="text">
                                <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                                <a href="#" class="btn theme-btn"><?php echo CLICK;?></a>
                            </div>
                        </div>

                        <div class="col col-md-4 col-xs-6">
                            <div class="title">
                                <ul>
                                    <li><h3>Kids playground</h3></li>
                                    <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                                </ul>                            
                            </div>
                            <div class="img-progress">
                                <div class="img-holder">
                                    <img src="images/latest-causes/img-4.jpg" alt class="img img-responsive">
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" data-percent="95"></div>
                                </div>
                            </div>
                            <div class="donate-amount">
                                <ul>
                                    <li>Raised: <span>£52,872</span> / £70,000</li>
                                    <li><i class="fa fa-clock-o"></i> 95 days</li>
                                </ul>
                            </div>
                            <div class="text">
                                <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                                <a href="#" class="btn theme-btn"><?php echo CLICK;?></a>
                            </div>
                        </div>

                        <div class="col col-md-4 col-xs-6">
                            <div class="title">
                                <ul>
                                    <li><h3>Kids playground</h3></li>
                                    <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                                </ul>                            
                            </div>
                            <div class="img-progress">
                                <div class="img-holder">
                                    <img src="images/latest-causes/img-5.jpg" alt class="img img-responsive">
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" data-percent="70"></div>
                                </div>
                            </div>
                            <div class="donate-amount">
                                <ul>
                                    <li>Raised: <span>£52,872</span> / £70,000</li>
                                    <li><i class="fa fa-clock-o"></i> 95 days</li>
                                </ul>
                            </div>
                            <div class="text">
                                <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                                <a href="#" class="btn theme-btn"><?php echo CLICK;?></a>
                            </div>
                        </div>

                        <div class="col col-md-4 col-xs-6">
                            <div class="title">
                                <ul>
                                    <li><h3>Kids playground</h3></li>
                                    <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                                </ul>                            
                            </div>
                            <div class="img-progress">
                                <div class="img-holder">
                                    <img src="images/latest-causes/img-6.jpg" alt class="img img-responsive">
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" data-percent="15"></div>
                                </div>
                            </div>
                            <div class="donate-amount">
                                <ul>
                                    <li>Raised: <span>£52,872</span> / £70,000</li>
                                    <li><i class="fa fa-clock-o"></i> 95 days</li>
                                </ul>
                            </div>
                            <div class="text">
                                <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                                <a href="#" class="btn theme-btn"><?php echo CLICK;?></a>
                            </div>
                        </div>
                    </div> <!-- end content -->
                </div>
                <div id="allcauses-thanks" style="display:none;">
                    <h3>Thanks <?php echo $temp->firstname;?>!</h3> Great choice!
                    <br/> A payment of £ 5,- will be deducted from your account <?php echo $temp->accountNumber;?>.
                    <br/> <span id="cancelLink">If you didn't intend to do this, <a href="#">
                    Click here to cancel...<span style="display: none;" id="timer"></span></a></span>
                </div>
                <form method="post">
                    <input type="hidden" name="contactid" id="contactid" value="<?php echo $temp->contactId; ?>" />
                    <input type="hidden" name="accountid" id="accountid" value="<?php echo $temp->accountId; ?>" />
                    <input type="hidden" name="ppid" id="ppid" value="<?php echo $temp->paymentProfileId;?>" />
                    <input type="hidden" name="cid" id="cid" value="<?php echo $_GET['cid']; ?>" />
                </form>
            </div> <!-- end container -->
        </section>
        <!-- end latest-causes -->

<?php include ( 'includes/footer.php' ); ?>
    <script type="text/javascript">
        $( document ).ready( function() {
            $('.theme-btn').on('click', function (e){
                $(this).hide();
                $(this).parent().append('<img src="images/loading.gif" />');
                
                $.ajax({
                    type: 'POST',
                    url: 'post.php',
                    crossDomain: true,
                    data: { cmid :  $('#cid').val(), contactid: $('#contactid').val(), accountid: $('#accountid').val(), ppid: $('#ppid').val(), type: 'flexdonor' },
                    success: function (data) {
                        var obj = jQuery.parseJSON( data );
                        $('#allcauses').hide();
                        $('#allcauses-thanks').show();
                    }
                });
                
                e.preventDefault();

            });
        });
    </script>
</body>
</html>
