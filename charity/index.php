<?php 
    include ( 'includes/header.php' ); 
?>
        <!-- start of hero -->   
        <section class="hero hero-style-three hero-slider-wrapper">
            <div class="hero-slider">
                <div class="slide">
                    <img src="images/slider/slide-3.jpg" alt>
                    <div class="container">
                        <div class="col col-sm-8 slider-title">
                            <h1>The most elegant <span>creature</span> is in grave <span>danger.</span></h1>
                            <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>
                            <a href="#" class="btn theme-btn">Case study</a>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <img src="images/slider/slide-2.jpg" alt>
                    <div class="container">
                        <div class="col col-sm-8 slider-title">
                            <h1>The most elegant <span>creature</span> is in grave <span>danger.</span></h1>
                            <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>
                            <a href="#" class="btn theme-btn">Case study</a>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <img src="images/slider/slide-1.jpg" alt>
                    <div class="container">
                        <div class="col col-sm-8 slider-title">
                            <h1>The most elegant <span>creature</span> is in grave <span>danger.</span></h1>
                            <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>
                            <a href="#" class="btn theme-btn">Case study</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end of hero slider -->

        
        <!-- start urgent-donation -->
        <section class="urgent-donation" id="urgetn-causes">
            <div class="container">
                <div class="row urgent-inner">
                    <div class="col col-lg-10 col-lg-offset-1">
                        <div class="urgent-slider">
                            <div class="urgent-box">
                                <div class="img-holder">
                                    <img src="images/urgent-slider/img-2.jpg" alt class="img img-responsive">
                                </div>
                                <div class="details">
                                    <span>Urgent</span>
                                    <h2>Kids promenade party</h2>
                                    <p class="remaing-date">3 days remaining</p>
                                    <div class="progress-wrapper">
                                        <div class="progress">
                                            <div class="progress-bar" data-percent="85"></div>
                                        </div>
                                    </div>
                                    <div class="goal-raised">
                                        <div class="goal">
                                            <span>Goal</span>
                                            <h4>12,500</h4>
                                        </div>
                                        <div class="raised">
                                            <span>Raised</span>
                                            <h4>12,500</h4>
                                        </div>
                                    </div>
                                    <form action="#" class="form">
                                        <div>
                                            <input type="text" class="form-control" placeholder="- ENTER AMOUNT -" required>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn theme-btn">Donate</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="urgent-box">
                                <div class="img-holder">
                                    <img src="images/urgent-slider/img-2.jpg" alt class="img img-responsive">
                                </div>
                                <div class="details">
                                    <span>Urgent</span>
                                    <h2>Kids promenade party</h2>
                                    <p class="remaing-date">3 days remaining</p>
                                    <div class="progress-wrapper">
                                        <div class="progress">
                                            <div class="progress-bar" data-percent="85"></div>
                                        </div>
                                        <div class="goal-raised">
                                            <div class="goal">
                                                <span>Goal</span>
                                                <h4>12,500</h4>
                                            </div>
                                            <div class="raised">
                                                <span>Raised</span>
                                                <h4>12,500</h4>
                                            </div>
                                        </div>
                                        <form action="#" class="form">
                                            <div>
                                                <input type="text" class="form-control" placeholder="- Enter Amount -" required>
                                            </div>
                                            <div>
                                                <button type="submit" class="btn theme-btn">Donate</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>                        
                        </div> <!-- end urgent-slider -->
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end urgent-donation -->


        <!-- start cta-4 --> 
        <section class="cta-4 section-padding" id="process">
            <h2 class="hidden">CTA 4</h2>
            <div class="container">
                <div class="row">
                    <div class="col col-sm-4">
                        <div class="wow fadeInLeftSlow">
                            <span class="icon"><i class="fi flaticon-money-1"></i></span>
                            <h3>Donate money</h3>
                            <p>Perspiciatis unde omnis iste natus error sit vo luptatem</p>
                            <a href="#" class="read-more">Read more</a>
                        </div>
                    </div>
                    <div class="col col-sm-4">
                        <div class="wow fadeInLeftSlow" data-wow-delay="0.5s">
                            <span class="icon"><i class="fi flaticon-heart"></i></span>
                            <h3>Become volunteer</h3>
                            <p>Perspiciatis unde omnis iste natus error sit vo luptatem</p>
                            <a href="#" class="read-more">Read more</a>
                        </div>
                    </div>
                    <div class="col col-sm-4">
                        <div class="wow fadeInLeftSlow" data-wow-delay="1s">
                            <span class="icon"><i class="fi flaticon-business-1"></i></span>
                            <h3>Sponsorship</h3>
                            <p>Perspiciatis unde omnis iste natus error sit vo luptatem</p>
                            <a href="#" class="read-more">Read more</a>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end cta-4 -->

       
        <!-- start latest-causes -->
        <section class="latest-causes section-padding" id="recent">
            <div class="container">
                <div class="row section-title-s2">
                    <div class="col col-xs-12">
                        <h2><span>recent</span> causes</h2>
                    </div>
                </div> <!-- end section-title -->

                <div class="row content">
                    <div class="col col-md-4 col-xs-6">
                        <div class="title">
                            <ul>
                                <li><h3>Kids playground</h3></li>
                                <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                            </ul>                            
                        </div>
                        <div class="img-progress">
                            <div class="img-holder">
                                <img src="images/latest-causes/img-1.jpg" alt class="img img-responsive">
                            </div>
                            <div class="progress">
                                <div class="progress-bar" data-percent="85"></div>
                            </div>
                        </div>
                        <div class="donate-amount">
                            <ul>
                                <li>Raised: <span>£52,872</span> / £70,000</li>
                                <li><i class="fa fa-clock-o"></i> 95 days</li>
                            </ul>
                        </div>
                        <div class="text">
                            <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                            <a href="#" class="btn theme-btn">Donate now</a>
                        </div>
                    </div>

                    <div class="col col-md-4 col-xs-6">
                        <div class="title">
                            <ul>
                                <li><h3>Kids playground</h3></li>
                                <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                            </ul>                            
                        </div>
                        <div class="img-progress">
                            <div class="img-holder">
                                <img src="images/latest-causes/img-2.jpg" alt class="img img-responsive">
                            </div>
                            <div class="progress">
                                <div class="progress-bar" data-percent="55"></div>
                            </div>
                        </div>
                        <div class="donate-amount">
                            <ul>
                                <li>Raised: <span>£52,872</span> / £70,000</li>
                                <li><i class="fa fa-clock-o"></i> 95 days</li>
                            </ul>
                        </div>
                        <div class="text">
                            <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                            <a href="#" class="btn theme-btn">Donate now</a>
                        </div>
                    </div>

                    <div class="col col-md-4 col-xs-6">
                        <div class="title">
                            <ul>
                                <li><h3>Kids playground</h3></li>
                                <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                            </ul>                            
                        </div>
                        <div class="img-progress">
                            <div class="img-holder">
                                <img src="images/latest-causes/img-3.jpg" alt class="img img-responsive">
                            </div>
                            <div class="progress">
                                <div class="progress-bar" data-percent="25"></div>
                            </div>
                        </div>
                        <div class="donate-amount">
                            <ul>
                                <li>Raised: <span>£52,872</span> / £70,000</li>
                                <li><i class="fa fa-clock-o"></i> 95 days</li>
                            </ul>
                        </div>
                        <div class="text">
                            <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                            <a href="#" class="btn theme-btn">Donate now</a>
                        </div>
                    </div>

                    <div class="col col-md-4 col-xs-6">
                        <div class="title">
                            <ul>
                                <li><h3>Kids playground</h3></li>
                                <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                            </ul>                            
                        </div>
                        <div class="img-progress">
                            <div class="img-holder">
                                <img src="images/latest-causes/img-4.jpg" alt class="img img-responsive">
                            </div>
                            <div class="progress">
                                <div class="progress-bar" data-percent="95"></div>
                            </div>
                        </div>
                        <div class="donate-amount">
                            <ul>
                                <li>Raised: <span>£52,872</span> / £70,000</li>
                                <li><i class="fa fa-clock-o"></i> 95 days</li>
                            </ul>
                        </div>
                        <div class="text">
                            <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                            <a href="#" class="btn theme-btn">Donate now</a>
                        </div>
                    </div>

                    <div class="col col-md-4 col-xs-6">
                        <div class="title">
                            <ul>
                                <li><h3>Kids playground</h3></li>
                                <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                            </ul>                            
                        </div>
                        <div class="img-progress">
                            <div class="img-holder">
                                <img src="images/latest-causes/img-5.jpg" alt class="img img-responsive">
                            </div>
                            <div class="progress">
                                <div class="progress-bar" data-percent="70"></div>
                            </div>
                        </div>
                        <div class="donate-amount">
                            <ul>
                                <li>Raised: <span>£52,872</span> / £70,000</li>
                                <li><i class="fa fa-clock-o"></i> 95 days</li>
                            </ul>
                        </div>
                        <div class="text">
                            <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                            <a href="#" class="btn theme-btn">Donate now</a>
                        </div>
                    </div>

                    <div class="col col-md-4 col-xs-6">
                        <div class="title">
                            <ul>
                                <li><h3>Kids playground</h3></li>
                                <li><i class="fa fa-map-marker"></i> Boston, Texas</li>
                            </ul>                            
                        </div>
                        <div class="img-progress">
                            <div class="img-holder">
                                <img src="images/latest-causes/img-6.jpg" alt class="img img-responsive">
                            </div>
                            <div class="progress">
                                <div class="progress-bar" data-percent="15"></div>
                            </div>
                        </div>
                        <div class="donate-amount">
                            <ul>
                                <li>Raised: <span>£52,872</span> / £70,000</li>
                                <li><i class="fa fa-clock-o"></i> 95 days</li>
                            </ul>
                        </div>
                        <div class="text">
                            <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                            <a href="#" class="btn theme-btn">Donate now</a>
                        </div>
                    </div>
                </div> <!-- end content -->
            </div> <!-- end container -->
        </section>
        <!-- end latest-causes -->


        <!-- start cta -->
        <section class="cta">
            <div class="container">
                <div class="row">
                    <div class="col col-md-5 col-sm-5">
                        <img src="images/cta-cartoon.png" alt class="img img-responsive">
                    </div>

                    <div class="col col-md-6 col-md-offset-1 col-sm-7">
                        <div class="cta-details wow fadeInRightSlow">
                            <h2>Buy and Sell <span>campaign</span> items.</h2>
                            <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam volupta tem quia voluptas sit aspernatur aut odit.</p>
                            <a href="#" class="btn theme-btn">Explore shop</a>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end cta -->


        <!-- start about-details -->
        <section class="about-us-st section-padding" id="about">
            <div class="container">
                <h2><span>About</span> us</h2>
                <div class="row">
                    <div class="col col-md-6">
                        <div class="left-col">
                            <div class="company">
                                <h3>We are <span>Charity++</span></h3>
                                <span>Making the world a better place</span>
                            </div>
                            <p>Perspiciatis unde omnis iste natus error sit voluptatem accusan tium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et.</p>

                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default current">
                                    <div class="panel-heading" id="headingOne">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">Voluptatem sequi nesciunt <i class="fa fa-angle-down"></i></a>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="img-holder">
                                                <img src="images/about/thumb/img-1.jpg" alt>
                                            </div>
                                            <div class="details">
                                                <p>Accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolo res.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading" id="headingTwo">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Similique sunt in culpa <i class="fa fa-angle-down"></i></a>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="img-holder">
                                                <img src="images/about/thumb/img-1.jpg" alt>
                                            </div>
                                            <div class="details">
                                                <p>Accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolo res.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading" id="headingThree">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Officia deserunt mollitia animi <i class="fa fa-angle-down"></i></a>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="img-holder">
                                                <img src="images/about/thumb/img-1.jpg" alt>
                                            </div>
                                            <div class="details">
                                                <p>Accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolo res.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end left-col -->
                    </div> <!-- end col -->

                    <div class="col col-md-6">
                        <div class="right-col">
                            <div class="video">
                                <img src="images/about/video-poster-2.jpg" alt="" class="img img-responsive">
                                <a href="https://www.youtube.com/embed/opj24KnzrWo?autoplay=1" class="video-btn" data-type="iframe"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end about-details -->


        <!-- start cta-2 --> 
        <section class="cta-2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col col-md-5 col-sm-4 join-us"></div>

                    <div class="col col-md-7 col-sm-8 sing-up  wow fadeInRightSlow">
                        <h3><span><img src="images/sing-up-icon.png" alt></span> Sign up for volunteer program</h3>
                        <span>Serve the humanity</span>
                        <p>Iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        <a href="#" class="btn theme-btn">Sign up</a>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end cta-2 -->


        <!-- start events-nearby -->
        <section class="events-nearby section-padding" id="events">
            <div class="container">
                <div class="row section-title-s2">
                    <div class="col col-md-8 col-md-offset-2">
                        <h2><span>Events</span> nearby</h2>
                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                    </div>
                </div> <!-- end section-title -->

                <div class="row">
                    <div class="col col-xs-12">
                        <div class="events-nearby-slider event-grid-wrapper">
                            <div class="event-grid">
                                <div class="event-box">
                                    <div class="img-holder">
                                        <img src="images/event/img-1.jpg" alt="" class="img img-responsive">
                                    </div>
                                    <div class="event-details">
                                        <span class="date">27,oct</span>
                                        <h3><a href="#">Water pump setting</a></h3>
                                        <span class="location"><i class="fa fa-map-marker"></i> Ambriz, africa</span>
                                        <a href="#" class="btn">Join event</a>
                                    </div>
                                </div>
                            </div>
                            <div class="event-grid">
                                <div class="event-box">
                                    <div class="img-holder">
                                        <img src="images/event/img-2.jpg" alt="" class="img img-responsive">
                                    </div>
                                    <div class="event-details">
                                        <span class="date">27,oct</span>
                                        <h3><a href="#">Water pump setting</a></h3>
                                        <span class="location"><i class="fa fa-map-marker"></i> Ambriz, africa</span>
                                        <a href="#" class="btn">Join event</a>
                                    </div>
                                </div>
                            </div>
                            <div class="event-grid">
                                <div class="event-box">
                                    <div class="img-holder">
                                        <img src="images/event/img-3.jpg" alt="" class="img img-responsive">
                                    </div>
                                    <div class="event-details">
                                        <span class="date">27,oct</span>
                                        <h3><a href="#">Water pump setting</a></h3>
                                        <span class="location"><i class="fa fa-map-marker"></i> Ambriz, africa</span>
                                        <a href="#" class="btn">Join event</a>
                                    </div>
                                </div>
                            </div>
                            <div class="event-grid">
                                <div class="event-box">
                                    <div class="img-holder">
                                        <img src="images/event/img-4.jpg" alt="" class="img img-responsive">
                                    </div>
                                    <div class="event-details">
                                        <span class="date">27,oct</span>
                                        <h3><a href="#">Water pump setting</a></h3>
                                        <span class="location"><i class="fa fa-map-marker"></i> Ambriz, africa</span>
                                        <a href="#" class="btn">Join event</a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end events-nearby-slider -->
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end events-nearby -->


        <!-- start cta-3 --> 
        <section class="cta-3">
            <div class="container">
                <div class="row">
                    <div class="col col-md-7 col-md-offset-5 details-text">
                        <div class="wow fadeInRightSlow">
                            <h2>Your little <span>awareness</span> means a lot</h2>
                            <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime pla ceat facere possimus.</p>
                            <a href="#" class="btn theme-btn">Sign up</a>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end cta-3 -->


        <!-- start shop-main-content -->
        <section class="shop-main-content section-padding" id="shop">
            <div class="container">
                <div class="row">
                    <div class="col col-md-12">
                        <div class="shop-content latest-product-slider">
                            <div class="grid">
                                <div class="box">
                                    <div class="img-holder">
                                        <img src="images/shop/img-1.jpg" alt class="img img-responsive">
                                    </div>
                                    <div class="details">
                                        <h3><a href="#">Skincare Product</a></h3>
                                        <a href="#" class="add-to-cart"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <div class="price">
                                            <span class="old-price">£45</span>
                                            <span class="current-price">£35</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="box">
                                    <div class="img-holder">
                                        <img src="images/shop/img-2.jpg" alt class="img img-responsive">
                                    </div>
                                    <div class="details">
                                        <h3><a href="#">Skincare Product</a></h3>
                                        <a href="#" class="add-to-cart"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <div class="price">
                                            <span class="current-price">£35</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="box">
                                    <div class="img-holder">
                                        <img src="images/shop/img-3.jpg" alt class="img img-responsive">
                                    </div>
                                    <div class="details">
                                        <h3><a href="#">Skincare Product</a></h3>
                                        <a href="#" class="add-to-cart"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <div class="price">
                                            <span class="current-price">£35</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="box">
                                    <div class="img-holder">
                                        <img src="images/shop/img-4.jpg" alt class="img img-responsive">
                                    </div>
                                    <div class="details">
                                        <h3><a href="#">Skincare Product</a></h3>
                                        <a href="#" class="add-to-cart"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <div class="price">
                                            <span class="current-price">£35</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="box">
                                    <div class="img-holder">
                                        <img src="images/shop/img-5.jpg" alt class="img img-responsive">
                                    </div>
                                    <div class="details">
                                        <h3><a href="#">Skincare Product</a></h3>
                                        <a href="#" class="add-to-cart"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <div class="price">
                                            <span class="current-price">£35</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="box">
                                    <div class="img-holder">
                                        <img src="images/shop/img-6.jpg" alt class="img img-responsive">
                                    </div>
                                    <div class="details">
                                        <h3><a href="#">Skincare Product</a></h3>
                                        <a href="#" class="add-to-cart"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <div class="price">
                                            <span class="current-price">£35</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end shop-content -->
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end shop-main-content -->



        <!-- start newsletter -->    
        <section class="newsletter">
            <div class="container-fluid">
                <div class="row">
                    <div class="col col-md-5 children-holder"></div>
                    <div class="col col-md-7 subscribe">
                        <h3>Subscribe us</h3>
                        <p>For <span>news</span> updates and promotional <span>events</span></p>

                        <form action="#">
                            <div>
                                <input class="form-control" type="email" required placeholder="email address">
                                <button type="submit" class="btn theme-btn">Subscribe</button>
                            </div>
                        </form>
                        <div class="pluses">
                            <i class="fa fa-plus"></i>
                            <i class="fa fa-plus"></i>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container-fluid -->
        </section>
        <!-- end newsletter -->


        <?php include ('includes/footer.php'); ?>

</body>
</html>