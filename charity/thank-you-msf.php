<!--https://www.msf.es/exito/donativo-->
<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7"  lang="es" dir="ltr"><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7"  lang="es" dir="ltr"><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8"  lang="es" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9"  lang="es" dir="ltr"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html  lang="es" dir="ltr" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->

<head>
  <meta charset="utf-8" /><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={xpid:"VQcGVl5TDhABVFZWAQMCXw=="};window.NREUM||(NREUM={}),__nr_require=function(t,n,e){function r(e){if(!n[e]){var o=n[e]={exports:{}};t[e][0].call(o.exports,function(n){var o=t[e][1][n];return r(o||n)},o,o.exports)}return n[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({1:[function(t,n,e){function r(t){try{s.console&&console.log(t)}catch(n){}}var o,i=t("ee"),a=t(15),s={};try{o=localStorage.getItem("__nr_flags").split(","),console&&"function"==typeof console.log&&(s.console=!0,o.indexOf("dev")!==-1&&(s.dev=!0),o.indexOf("nr_dev")!==-1&&(s.nrDev=!0))}catch(c){}s.nrDev&&i.on("internal-error",function(t){r(t.stack)}),s.dev&&i.on("fn-err",function(t,n,e){r(e.stack)}),s.dev&&(r("NR AGENT IN DEVELOPMENT MODE"),r("flags: "+a(s,function(t,n){return t}).join(", ")))},{}],2:[function(t,n,e){function r(t,n,e,r,s){try{p?p-=1:o(s||new UncaughtException(t,n,e),!0)}catch(f){try{i("ierr",[f,c.now(),!0])}catch(d){}}return"function"==typeof u&&u.apply(this,a(arguments))}function UncaughtException(t,n,e){this.message=t||"Uncaught error with no additional information",this.sourceURL=n,this.line=e}function o(t,n){var e=n?null:c.now();i("err",[t,e])}var i=t("handle"),a=t(16),s=t("ee"),c=t("loader"),f=t("gos"),u=window.onerror,d=!1,l="nr@seenError",p=0;c.features.err=!0,t(1),window.onerror=r;try{throw new Error}catch(h){"stack"in h&&(t(8),t(7),"addEventListener"in window&&t(5),c.xhrWrappable&&t(9),d=!0)}s.on("fn-start",function(t,n,e){d&&(p+=1)}),s.on("fn-err",function(t,n,e){d&&!e[l]&&(f(e,l,function(){return!0}),this.thrown=!0,o(e))}),s.on("fn-end",function(){d&&!this.thrown&&p>0&&(p-=1)}),s.on("internal-error",function(t){i("ierr",[t,c.now(),!0])})},{}],3:[function(t,n,e){t("loader").features.ins=!0},{}],4:[function(t,n,e){function r(t){}if(window.performance&&window.performance.timing&&window.performance.getEntriesByType){var o=t("ee"),i=t("handle"),a=t(8),s=t(7),c="learResourceTimings",f="addEventListener",u="resourcetimingbufferfull",d="bstResource",l="resource",p="-start",h="-end",m="fn"+p,w="fn"+h,v="bstTimer",y="pushState",g=t("loader");g.features.stn=!0,t(6);var b=NREUM.o.EV;o.on(m,function(t,n){var e=t[0];e instanceof b&&(this.bstStart=g.now())}),o.on(w,function(t,n){var e=t[0];e instanceof b&&i("bst",[e,n,this.bstStart,g.now()])}),a.on(m,function(t,n,e){this.bstStart=g.now(),this.bstType=e}),a.on(w,function(t,n){i(v,[n,this.bstStart,g.now(),this.bstType])}),s.on(m,function(){this.bstStart=g.now()}),s.on(w,function(t,n){i(v,[n,this.bstStart,g.now(),"requestAnimationFrame"])}),o.on(y+p,function(t){this.time=g.now(),this.startPath=location.pathname+location.hash}),o.on(y+h,function(t){i("bstHist",[location.pathname+location.hash,this.startPath,this.time])}),f in window.performance&&(window.performance["c"+c]?window.performance[f](u,function(t){i(d,[window.performance.getEntriesByType(l)]),window.performance["c"+c]()},!1):window.performance[f]("webkit"+u,function(t){i(d,[window.performance.getEntriesByType(l)]),window.performance["webkitC"+c]()},!1)),document[f]("scroll",r,{passive:!0}),document[f]("keypress",r,!1),document[f]("click",r,!1)}},{}],5:[function(t,n,e){function r(t){for(var n=t;n&&!n.hasOwnProperty(u);)n=Object.getPrototypeOf(n);n&&o(n)}function o(t){s.inPlace(t,[u,d],"-",i)}function i(t,n){return t[1]}var a=t("ee").get("events"),s=t(18)(a,!0),c=t("gos"),f=XMLHttpRequest,u="addEventListener",d="removeEventListener";n.exports=a,"getPrototypeOf"in Object?(r(document),r(window),r(f.prototype)):f.prototype.hasOwnProperty(u)&&(o(window),o(f.prototype)),a.on(u+"-start",function(t,n){var e=t[1],r=c(e,"nr@wrapped",function(){function t(){if("function"==typeof e.handleEvent)return e.handleEvent.apply(e,arguments)}var n={object:t,"function":e}[typeof e];return n?s(n,"fn-",null,n.name||"anonymous"):e});this.wrapped=t[1]=r}),a.on(d+"-start",function(t){t[1]=this.wrapped||t[1]})},{}],6:[function(t,n,e){var r=t("ee").get("history"),o=t(18)(r);n.exports=r,o.inPlace(window.history,["pushState","replaceState"],"-")},{}],7:[function(t,n,e){var r=t("ee").get("raf"),o=t(18)(r),i="equestAnimationFrame";n.exports=r,o.inPlace(window,["r"+i,"mozR"+i,"webkitR"+i,"msR"+i],"raf-"),r.on("raf-start",function(t){t[0]=o(t[0],"fn-")})},{}],8:[function(t,n,e){function r(t,n,e){t[0]=a(t[0],"fn-",null,e)}function o(t,n,e){this.method=e,this.timerDuration=isNaN(t[1])?0:+t[1],t[0]=a(t[0],"fn-",this,e)}var i=t("ee").get("timer"),a=t(18)(i),s="setTimeout",c="setInterval",f="clearTimeout",u="-start",d="-";n.exports=i,a.inPlace(window,[s,"setImmediate"],s+d),a.inPlace(window,[c],c+d),a.inPlace(window,[f,"clearImmediate"],f+d),i.on(c+u,r),i.on(s+u,o)},{}],9:[function(t,n,e){function r(t,n){d.inPlace(n,["onreadystatechange"],"fn-",s)}function o(){var t=this,n=u.context(t);t.readyState>3&&!n.resolved&&(n.resolved=!0,u.emit("xhr-resolved",[],t)),d.inPlace(t,y,"fn-",s)}function i(t){g.push(t),h&&(x?x.then(a):w?w(a):(E=-E,O.data=E))}function a(){for(var t=0;t<g.length;t++)r([],g[t]);g.length&&(g=[])}function s(t,n){return n}function c(t,n){for(var e in t)n[e]=t[e];return n}t(5);var f=t("ee"),u=f.get("xhr"),d=t(18)(u),l=NREUM.o,p=l.XHR,h=l.MO,m=l.PR,w=l.SI,v="readystatechange",y=["onload","onerror","onabort","onloadstart","onloadend","onprogress","ontimeout"],g=[];n.exports=u;var b=window.XMLHttpRequest=function(t){var n=new p(t);try{u.emit("new-xhr",[n],n),n.addEventListener(v,o,!1)}catch(e){try{u.emit("internal-error",[e])}catch(r){}}return n};if(c(p,b),b.prototype=p.prototype,d.inPlace(b.prototype,["open","send"],"-xhr-",s),u.on("send-xhr-start",function(t,n){r(t,n),i(n)}),u.on("open-xhr-start",r),h){var x=m&&m.resolve();if(!w&&!m){var E=1,O=document.createTextNode(E);new h(a).observe(O,{characterData:!0})}}else f.on("fn-end",function(t){t[0]&&t[0].type===v||a()})},{}],10:[function(t,n,e){function r(t){var n=this.params,e=this.metrics;if(!this.ended){this.ended=!0;for(var r=0;r<d;r++)t.removeEventListener(u[r],this.listener,!1);if(!n.aborted){if(e.duration=a.now()-this.startTime,4===t.readyState){n.status=t.status;var i=o(t,this.lastSize);if(i&&(e.rxSize=i),this.sameOrigin){var c=t.getResponseHeader("X-NewRelic-App-Data");c&&(n.cat=c.split(", ").pop())}}else n.status=0;e.cbTime=this.cbTime,f.emit("xhr-done",[t],t),s("xhr",[n,e,this.startTime])}}}function o(t,n){var e=t.responseType;if("json"===e&&null!==n)return n;var r="arraybuffer"===e||"blob"===e||"json"===e?t.response:t.responseText;return h(r)}function i(t,n){var e=c(n),r=t.params;r.host=e.hostname+":"+e.port,r.pathname=e.pathname,t.sameOrigin=e.sameOrigin}var a=t("loader");if(a.xhrWrappable){var s=t("handle"),c=t(11),f=t("ee"),u=["load","error","abort","timeout"],d=u.length,l=t("id"),p=t(14),h=t(13),m=window.XMLHttpRequest;a.features.xhr=!0,t(9),f.on("new-xhr",function(t){var n=this;n.totalCbs=0,n.called=0,n.cbTime=0,n.end=r,n.ended=!1,n.xhrGuids={},n.lastSize=null,p&&(p>34||p<10)||window.opera||t.addEventListener("progress",function(t){n.lastSize=t.loaded},!1)}),f.on("open-xhr-start",function(t){this.params={method:t[0]},i(this,t[1]),this.metrics={}}),f.on("open-xhr-end",function(t,n){"loader_config"in NREUM&&"xpid"in NREUM.loader_config&&this.sameOrigin&&n.setRequestHeader("X-NewRelic-ID",NREUM.loader_config.xpid)}),f.on("send-xhr-start",function(t,n){var e=this.metrics,r=t[0],o=this;if(e&&r){var i=h(r);i&&(e.txSize=i)}this.startTime=a.now(),this.listener=function(t){try{"abort"===t.type&&(o.params.aborted=!0),("load"!==t.type||o.called===o.totalCbs&&(o.onloadCalled||"function"!=typeof n.onload))&&o.end(n)}catch(e){try{f.emit("internal-error",[e])}catch(r){}}};for(var s=0;s<d;s++)n.addEventListener(u[s],this.listener,!1)}),f.on("xhr-cb-time",function(t,n,e){this.cbTime+=t,n?this.onloadCalled=!0:this.called+=1,this.called!==this.totalCbs||!this.onloadCalled&&"function"==typeof e.onload||this.end(e)}),f.on("xhr-load-added",function(t,n){var e=""+l(t)+!!n;this.xhrGuids&&!this.xhrGuids[e]&&(this.xhrGuids[e]=!0,this.totalCbs+=1)}),f.on("xhr-load-removed",function(t,n){var e=""+l(t)+!!n;this.xhrGuids&&this.xhrGuids[e]&&(delete this.xhrGuids[e],this.totalCbs-=1)}),f.on("addEventListener-end",function(t,n){n instanceof m&&"load"===t[0]&&f.emit("xhr-load-added",[t[1],t[2]],n)}),f.on("removeEventListener-end",function(t,n){n instanceof m&&"load"===t[0]&&f.emit("xhr-load-removed",[t[1],t[2]],n)}),f.on("fn-start",function(t,n,e){n instanceof m&&("onload"===e&&(this.onload=!0),("load"===(t[0]&&t[0].type)||this.onload)&&(this.xhrCbStart=a.now()))}),f.on("fn-end",function(t,n){this.xhrCbStart&&f.emit("xhr-cb-time",[a.now()-this.xhrCbStart,this.onload,n],n)})}},{}],11:[function(t,n,e){n.exports=function(t){var n=document.createElement("a"),e=window.location,r={};n.href=t,r.port=n.port;var o=n.href.split("://");!r.port&&o[1]&&(r.port=o[1].split("/")[0].split("@").pop().split(":")[1]),r.port&&"0"!==r.port||(r.port="https"===o[0]?"443":"80"),r.hostname=n.hostname||e.hostname,r.pathname=n.pathname,r.protocol=o[0],"/"!==r.pathname.charAt(0)&&(r.pathname="/"+r.pathname);var i=!n.protocol||":"===n.protocol||n.protocol===e.protocol,a=n.hostname===document.domain&&n.port===e.port;return r.sameOrigin=i&&(!n.hostname||a),r}},{}],12:[function(t,n,e){function r(){}function o(t,n,e){return function(){return i(t,[f.now()].concat(s(arguments)),n?null:this,e),n?void 0:this}}var i=t("handle"),a=t(15),s=t(16),c=t("ee").get("tracer"),f=t("loader"),u=NREUM;"undefined"==typeof window.newrelic&&(newrelic=u);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],l="api-",p=l+"ixn-";a(d,function(t,n){u[n]=o(l+n,!0,"api")}),u.addPageAction=o(l+"addPageAction",!0),u.setCurrentRouteName=o(l+"routeName",!0),n.exports=newrelic,u.interaction=function(){return(new r).get()};var h=r.prototype={createTracer:function(t,n){var e={},r=this,o="function"==typeof n;return i(p+"tracer",[f.now(),t,e],r),function(){if(c.emit((o?"":"no-")+"fn-start",[f.now(),r,o],e),o)try{return n.apply(this,arguments)}catch(t){throw c.emit("fn-err",[arguments,this,t],e),t}finally{c.emit("fn-end",[f.now()],e)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,n){h[n]=o(p+n)}),newrelic.noticeError=function(t){"string"==typeof t&&(t=new Error(t)),i("err",[t,f.now()])}},{}],13:[function(t,n,e){n.exports=function(t){if("string"==typeof t&&t.length)return t.length;if("object"==typeof t){if("undefined"!=typeof ArrayBuffer&&t instanceof ArrayBuffer&&t.byteLength)return t.byteLength;if("undefined"!=typeof Blob&&t instanceof Blob&&t.size)return t.size;if(!("undefined"!=typeof FormData&&t instanceof FormData))try{return JSON.stringify(t).length}catch(n){return}}}},{}],14:[function(t,n,e){var r=0,o=navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/);o&&(r=+o[1]),n.exports=r},{}],15:[function(t,n,e){function r(t,n){var e=[],r="",i=0;for(r in t)o.call(t,r)&&(e[i]=n(r,t[r]),i+=1);return e}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],16:[function(t,n,e){function r(t,n,e){n||(n=0),"undefined"==typeof e&&(e=t?t.length:0);for(var r=-1,o=e-n||0,i=Array(o<0?0:o);++r<o;)i[r]=t[n+r];return i}n.exports=r},{}],17:[function(t,n,e){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],18:[function(t,n,e){function r(t){return!(t&&t instanceof Function&&t.apply&&!t[a])}var o=t("ee"),i=t(16),a="nr@original",s=Object.prototype.hasOwnProperty,c=!1;n.exports=function(t,n){function e(t,n,e,o){function nrWrapper(){var r,a,s,c;try{a=this,r=i(arguments),s="function"==typeof e?e(r,a):e||{}}catch(f){l([f,"",[r,a,o],s])}u(n+"start",[r,a,o],s);try{return c=t.apply(a,r)}catch(d){throw u(n+"err",[r,a,d],s),d}finally{u(n+"end",[r,a,c],s)}}return r(t)?t:(n||(n=""),nrWrapper[a]=t,d(t,nrWrapper),nrWrapper)}function f(t,n,o,i){o||(o="");var a,s,c,f="-"===o.charAt(0);for(c=0;c<n.length;c++)s=n[c],a=t[s],r(a)||(t[s]=e(a,f?s+o:o,i,s))}function u(e,r,o){if(!c||n){var i=c;c=!0;try{t.emit(e,r,o,n)}catch(a){l([a,e,r,o])}c=i}}function d(t,n){if(Object.defineProperty&&Object.keys)try{var e=Object.keys(t);return e.forEach(function(e){Object.defineProperty(n,e,{get:function(){return t[e]},set:function(n){return t[e]=n,n}})}),n}catch(r){l([r])}for(var o in t)s.call(t,o)&&(n[o]=t[o]);return n}function l(n){try{t.emit("internal-error",n)}catch(e){}}return t||(t=o),e.inPlace=f,e.flag=a,e}},{}],ee:[function(t,n,e){function r(){}function o(t){function n(t){return t&&t instanceof r?t:t?c(t,s,i):i()}function e(e,r,o,i){if(!l.aborted||i){t&&t(e,r,o);for(var a=n(o),s=h(e),c=s.length,f=0;f<c;f++)s[f].apply(a,r);var d=u[y[e]];return d&&d.push([g,e,r,a]),a}}function p(t,n){v[t]=h(t).concat(n)}function h(t){return v[t]||[]}function m(t){return d[t]=d[t]||o(e)}function w(t,n){f(t,function(t,e){n=n||"feature",y[e]=n,n in u||(u[n]=[])})}var v={},y={},g={on:p,emit:e,get:m,listeners:h,context:n,buffer:w,abort:a,aborted:!1};return g}function i(){return new r}function a(){(u.api||u.feature)&&(l.aborted=!0,u=l.backlog={})}var s="nr@context",c=t("gos"),f=t(15),u={},d={},l=n.exports=o();l.backlog=u},{}],gos:[function(t,n,e){function r(t,n,e){if(o.call(t,n))return t[n];var r=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return t[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(t,n,e){function r(t,n,e,r){o.buffer([t],r),o.emit(t,n,e)}var o=t("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(t,n,e){function r(t){var n=typeof t;return!t||"object"!==n&&"function"!==n?-1:t===window?0:a(t,i,function(){return o++})}var o=1,i="nr@id",a=t("gos");n.exports=r},{}],loader:[function(t,n,e){function r(){if(!x++){var t=b.info=NREUM.info,n=l.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(t&&t.licenseKey&&t.applicationID&&n))return u.abort();f(y,function(n,e){t[n]||(t[n]=e)}),c("mark",["onload",a()+b.offset],null,"api");var e=l.createElement("script");e.src="https://"+t.agent,n.parentNode.insertBefore(e,n)}}function o(){"complete"===l.readyState&&i()}function i(){c("mark",["domContent",a()+b.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(s=Math.max((new Date).getTime(),s))-b.offset}var s=(new Date).getTime(),c=t("handle"),f=t(15),u=t("ee"),d=window,l=d.document,p="addEventListener",h="attachEvent",m=d.XMLHttpRequest,w=m&&m.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:m,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1071.min.js"},g=m&&w&&w[p]&&!/CriOS/.test(navigator.userAgent),b=n.exports={offset:s,now:a,origin:v,features:{},xhrWrappable:g};t(12),l[p]?(l[p]("DOMContentLoaded",i,!1),d[p]("load",r,!1)):(l[h]("onreadystatechange",o),d[h]("onload",r)),c("mark",["firstbyte",s],null,"api");var x=0,E=t(17)},{}]},{},["loader",2,10,4,3]);</script>
<!--[if lte IE 10]><script src="/profiles/msfes/libraries/matchmedia/matchMedia.js" />
</script><![endif]--><meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="image_src" href="https://www.msf.es/sites/default/files/styles/summary_image_desktop/https/media.msf.org/Docs/MSF/Media/TR1/2/d/1/2/MSF149018.jpg?itok=Yx7H1w8y&amp;timestamp=1458663021" />
<link rel="canonical" href="https://www.msf.es/exito/donativo" />
<link rel="shortlink" href="https://www.msf.es/node/1" />
<meta property="og:site_name" content="Médicos Sin Fronteras" />
<meta property="og:type" content="article" />
<meta property="og:title" content="¡Muchas gracias por tu apoyo!" />
<meta property="og:url" content="https://www.msf.es/exito/donativo" />
<meta property="og:updated_time" content="2017-06-16T14:38:41+02:00" />
<meta property="og:image" content="https://www.msf.es/sites/default/files/styles/summary_image_desktop/https/media.msf.org/Docs/MSF/Media/TR1/2/d/1/2/MSF149018.jpg?itok=Yx7H1w8y&amp;timestamp=1458663021" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.msf.es/exito/donativo" />
<meta name="twitter:title" content="¡Muchas gracias por tu apoyo!" />
<meta name="twitter:image" content="https://www.msf.es/sites/default/files/styles/summary_image_desktop/https/media.msf.org/Docs/MSF/Media/TR1/2/d/1/2/MSF149018.jpg?itok=Yx7H1w8y&amp;timestamp=1458663021" />
<meta property="article:published_time" content="2015-09-22T15:56:35+02:00" />
<meta property="article:modified_time" content="2017-06-16T14:38:41+02:00" />
  <title>¡Muchas gracias por tu apoyo! | Médicos Sin Fronteras</title>

  <link rel="apple-touch-icon" sizes="57x57" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-57x57.png"><link rel="apple-touch-icon" sizes="60x60" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-60x60.png"><link rel="apple-touch-icon" sizes="72x72" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-72x72.png"><link rel="apple-touch-icon" sizes="76x76" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-76x76.png"><link rel="apple-touch-icon" sizes="114x114" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-114x114.png"><link rel="apple-touch-icon" sizes="120x120" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-120x120.png"><link rel="apple-touch-icon" sizes="144x144" href="/profiles/msfes/themes/custom/msfes_zen/favicon/>apple-touch-icon-144x144.png"><link rel="apple-touch-icon" sizes="152x152" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-152x152.png"><link rel="apple-touch-icon" sizes="180x180" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-180x180.png"><link rel="icon" type="image/png" href="/profiles/msfes/themes/custom/msfes_zen/favicon/favicon-32x32.png" sizes="32x32"><link rel="icon" type="image/png" href="/profiles/msfes/themes/custom/msfes_zen/favicon/favicon-194x194.png" sizes="194x194"><link rel="icon" type="image/png" href="/profiles/msfes/themes/custom/msfes_zen/favicon/favicon-96x96.png" sizes="96x96"><link rel="icon" type="image/png" href="/profiles/msfes/themes/custom/msfes_zen/favicon/android-chrome-192x192.png" sizes="192x192"><link rel="icon" type="image/png" href="/profiles/msfes/themes/custom/msfes_zen/favicon/favicon-16x16.png" sizes="16x16"><link rel="manifest" href="/profiles/msfes/themes/custom/msfes_zen/favicon/manifest.json"><link rel="shortcut icon" href="/profiles/msfes/themes/custom/msfes_zen/favicon/favicon.ico"><meta name="msapplication-TileColor" content="#ffffff"><meta name="msapplication-TileImage" content="/profiles/msfes/themes/custom/msfes_zen/favicon/mstile-144x144.png"><meta name="msapplication-config" content="/profiles/msfes/themes/custom/msfes_zen/favicon/browserconfig.xml"><meta name="theme-color" content="#ffffff">
      <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, user-scalable=no">
  
  <link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_vJDwuVafHSnJ-rf4kraQgzerHSdd0llY4WCmeI3Iqxg.css" media="all" />
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_1kcFxlSNcGp0v-loHEAsgu2rUXBn8L_MPPdtDYuz-rU.css" media="all" />
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_USqOob_wULozz2-VscbRySiDeZUxR9BRiAFMuTucczo.css" media="all" />
<style>#sliding-popup.sliding-popup-bottom{background:#ffffff;}#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text p{color:#8E8E8E !important;}
</style>
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_BvvK_KNm42iC6LDPtoh9O47Yf8iRq3GvK8n4N09yieo.css" media="all" />
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_I_DTWsfymF0V1dVUXqFXS_IwJq09115G-Rwl5d56Da4.css" media="all" />

<!--[if lte IE 8]>
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_nSdmrrLNUnzK-9BsernIuIssNyrEybzC3ijIy_b9LQs.css" media="all" />
<![endif]-->

<!--[if lte IE 9]>
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_Rs6Z2DzsFt8vNgrbz__Mp2azj_dwTHbeUBKhEgj2iSY.css" media="all" />
<![endif]-->
  <script src="https://www.msf.es/sites/default/files/js/js_urCmzDS98vJbRW2n7PNOexmM8xf5cAhVBbHnM2cToXk.js"></script>
<script>var dataLayer = [];function msfesGetDevice(){
          var md = new MobileDetect(window.navigator.userAgent);
          var device = 'desktop';
          if (md.tablet()) {
            device = 'tablet';
          }
          else if (md.mobile()) {
            device = 'mobile';
          }
          return device;
        }; dataLayer = [{
                 'categoria':'gracias',
                 'subcategoria':'gracias',
                 'dispositivo': msfesGetDevice()
               }];window.addEventListener('load', function msfesPartner() {
           var becomePartnerButtons = document.querySelectorAll('.become-partner__link');

           for (var i = 0; i < becomePartnerButtons.length; i++) {
             becomePartnerButtons[i].addEventListener('click', function () {
               partnerDatalayer();
             });
           }

           var partnerDatalayer = function() {
             dataLayer.push({
               'eventCat':'clic_hazte_socio',
               'eventAct':'header',
               'eventLbl':'home',
               'event':'eventoGA'
             });
           };
         });window.addEventListener('load', function msfesSocial() {
          var socialFollowItems = document.querySelectorAll('#social-slider .on-the-web');
          var socialShareItems = document.querySelectorAll('#social-share .social-share__item');

          for (var i = 0; i < socialFollowItems.length; i++) {
            socialFollowItems[i].addEventListener('click', function () {
              var socialNetwork = '';
              if (this.classList.contains('otw-twitter')) {
                socialNetwork = 'twitter';
              }
              else if (this.classList.contains('otw-facebook')) {
                socialNetwork = 'facebook';
              }
              else if (this.classList.contains('otw-instagram')) {
                socialNetwork = 'instagram';
              }
              else if (this.classList.contains('otw-youtube')) {
                socialNetwork = 'youtube';
              }
              else if (this.classList.contains('otw-google')) {
                socialNetwork = 'google';
              }
              else if (this.classList.contains('otw-flickr')) {
                socialNetwork = 'flickr';
              }
              else if (this.classList.contains('otw-tumblr')) {
                socialNetwork = 'tumblr';
              }
              else if (this.classList.contains('otw-vimeo')) {
                socialNetwork = 'vimeo';
              }
              socialDatalayer(socialNetwork, 'follow');
            });
          }

          for (var i = 0; i < socialShareItems.length; i++) {
            socialShareItems[i].addEventListener('click', function () {
              var socialNetwork = '';
              if (this.classList.contains('twitter')) {
                socialNetwork = 'twitter';
              }
              else if (this.classList.contains('facebook')) {
                socialNetwork = 'facebook';
              }
              else if (this.classList.contains('instagram')) {
                socialNetwork = 'instagram';
              }
              else if (this.classList.contains('youtube')) {
                socialNetwork = 'youtube';
              }
              else if (this.classList.contains('google')) {
                socialNetwork = 'google';
              }
              else if (this.classList.contains('flickr')) {
                socialNetwork = 'flickr';
              }
              else if (this.classList.contains('tumblr')) {
                socialNetwork = 'tumblr';
              }
              else if (this.classList.contains('vimeo')) {
                socialNetwork = 'vimeo';
              }
              else if (this.classList.contains('email')) {
                socialNetwork = 'email';
              }
              socialDatalayer(socialNetwork, 'share');
            });
          }

          var socialDatalayer = function(social, action) {
            dataLayer.push({
              'socialNetwork':social,
              'socialAction':action,
              'socialTarget':'https://www.msf.es/exito/donativo',
              'event':'eventoSocial'
            });
          };
        }); window.addEventListener('load', function msfesNewsletter() {
           var newsletterSubmitButtons = document.querySelectorAll('.entitytype-newsletter_signup-form .form-submit');

           for (var i = 0; i < newsletterSubmitButtons.length; i++) {
             newsletterSubmitButtons[i].addEventListener('click', function () {
               newsletterDatalayer();
             });
           }

           var newsletterDatalayer = function() {
             dataLayer.push({
               'eventCat':'lead',
               'eventAct':'suscripcion_newsletter',
               'eventLbl':'home',
               'event':'eventoGA'
             });
           };
         });</script>
<script src="https://www.msf.es/sites/default/files/js/js_cSOowGuq2BGYf0Sc296Qel0yaNgMkLvbT4tyy1SrNjw.js"></script>
<script src="https://www.msf.es/sites/default/files/js/js_Jozr3f52u0F-UfC_5Nk-_EZyMvGH4TCdwp10mh0GVK8.js"></script>
<script src="https://www.msf.es/sites/default/files/js/js_a7X_ExYIVsxP3P4sz_Lt_LKlabm-rAFLwNCBbUzW26A.js"></script>
<script src="https://www.msf.es/sites/default/files/js/js_-dKvtC3gB-oTJiQxsHb1CKIUnXhcE_6E7imtuWkqLJk.js"></script>
<script>document.createElement( "picture" );</script>
<script src="https://www.msf.es/sites/default/files/js/js__Qd-e5U3c_QtgxV1OZfs4kCsE5J86q19-eww-g8AN_U.js"></script>
<script src="//maps.googleapis.com/maps/api/js?v=3"></script>
<script src="https://www.msf.es/sites/default/files/js/js_Qsvbqo50NTuBiCY8t9PrlCazvFLN2VObayqBat1NywU.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"msfes_zen","theme_token":"YSkJQMErg3RiMuVqOxY6gOx4jY_0DVFABkhjZHI-cFs","js":{"profiles\/msfes\/modules\/contrib\/picture\/picturefill2\/picturefill.min.js":1,"profiles\/msfes\/modules\/contrib\/picture\/picture.min.js":1,"profiles\/msfes\/modules\/contrib\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"profiles\/msfes\/modules\/contrib\/clientside_validation\/js\/clientside_validation.ie8.js":1,"profiles\/msfes\/modules\/contrib\/clientside_validation\/js\/clientside_validation.js":1,"profiles\/msfes\/libraries\/mobile_detect_js\/mobile-detect.min.js":1,"0":1,"public:\/\/google_tag\/google_tag.script.js":1,"profiles\/msfes\/modules\/contrib\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"profiles\/msfes\/libraries\/fitvids\/jquery.fitvids.js":1,"profiles\/msfes\/libraries\/matchheight\/jquery.matchHeight-min.js":1,"profiles\/msfes\/libraries\/chosen\/chosen.jquery.min.js":1,"profiles\/msfes\/libraries\/mmenu\/src\/js\/jquery.mmenu.min.js":1,"profiles\/msfes\/libraries\/scrolltofixed\/jquery-scrolltofixed.js":1,"profiles\/msfes\/modules\/contrib\/fitvids\/fitvids.js":1,"profiles\/msfes\/libraries\/enquire\/dist\/enquire.min.js":1,"profiles\/msfes\/modules\/contrib\/breakpointsjs\/js\/breakpoints.js":1,"1":1,"public:\/\/languages\/es_UUCcnqA5s3YO9bYHtGZoDcOsNpWtP_Dk14S_8qZhEjA.js":1,"profiles\/msfes\/libraries\/colorbox\/jquery.colorbox-min.js":1,"profiles\/msfes\/modules\/contrib\/colorbox\/js\/colorbox.js":1,"profiles\/msfes\/modules\/contrib\/colorbox\/styles\/plain\/colorbox_style.js":1,"profiles\/msfes\/modules\/features\/content\/ft_event\/js\/image_event.js":1,"profiles\/msfes\/libraries\/jquery-form-submit-single\/src\/jquery.form-submit-single.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_common\/js\/msf.form-submit-single.js":1,"profiles\/msfes\/libraries\/jquery.validate\/dist\/jquery.validate.js":1,"profiles\/msfes\/libraries\/jquery.validate\/dist\/additional-methods.js":1,"profiles\/msfes\/libraries\/jquery.validate\/dist\/localization\/messages_es.js":1,"misc\/textarea.js":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/start_matchheight.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_newsletter_signup\/js\/header-newsletter.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_common\/js\/clientside_validation.common.js":1,"profiles\/msfes\/libraries\/flexslider\/jquery.flexslider-min.js":1,"profiles\/msfes\/modules\/contrib\/field_group\/field_group.js":1,"profiles\/msfes\/modules\/features\/general\/ft_msfes_footer\/js\/site_selector.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_contact\/js\/footer_quick_contact.js":1,"profiles\/msfes\/modules\/features\/general\/ft_social\/js\/social_share.js":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/footer_map.js":1,"profiles\/msfes\/modules\/contrib\/chosen\/chosen.js":1,"\/\/maps.googleapis.com\/maps\/api\/js?v=3":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/rwd_menu.js":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/search_block.js":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/modernizr.touch.js":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/glossary.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"profiles\/msfes\/libraries\/chosen\/chosen.css":1,"profiles\/msfes\/modules\/contrib\/chosen\/css\/chosen-drupal.css":1,"profiles\/msfes\/libraries\/mmenu\/src\/css\/jquery.mmenu.css":1,"modules\/comment\/comment.css":1,"profiles\/msfes\/modules\/contrib\/date\/date_api\/date.css":1,"profiles\/msfes\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"profiles\/msfes\/modules\/contrib\/fitvids\/fitvids.css":1,"modules\/node\/node.css":1,"profiles\/msfes\/modules\/contrib\/picture\/picture_wysiwyg.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"profiles\/msfes\/modules\/platform\/custom\/media_dam\/css\/media-dam.base.css":1,"profiles\/msfes\/modules\/platform\/custom\/media_dam\/css\/media-dam.admin.css":1,"profiles\/msfes\/modules\/contrib\/views\/css\/views.css":1,"profiles\/msfes\/modules\/contrib\/colorbox\/styles\/plain\/colorbox_style.css":1,"profiles\/msfes\/modules\/contrib\/ctools\/css\/ctools.css":1,"profiles\/msfes\/modules\/contrib\/panels\/css\/panels.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/layouts\/2colmsf\/..\/..\/css\/layouts\/2colmsf.css":1,"profiles\/msfes\/modules\/contrib\/field_collection\/field_collection.theme.css":1,"0":1,"profiles\/msfes\/modules\/contrib\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"sites\/default\/files\/honeypot\/honeypot.css":1,"profiles\/msfes\/modules\/contrib\/flexslider\/assets\/css\/flexslider_img.css":1,"profiles\/msfes\/libraries\/flexslider\/flexslider.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/system.base.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/system.menus.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/system.messages.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/system.theme.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/comment.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/node.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/jquery.mmenu.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/field_collection.theme.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/panels.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/css\/styles.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/css\/form.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/css\/flexslider.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/css\/lte_ie8.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/css\/lte_ie9.css":1}},"colorbox":{"opacity":"0.85","current":"{current} de {total}","previous":"\u00ab Anterior","next":"Siguiente \u00bb","close":"Cerrar","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"field_group":{"div":"default","link":"image_background"},"clientsideValidation":{"forms":{"newsletter-signup-entityform-edit-form":{"rules":{"field_form_mail[und][0][email]":{"required":true,"maxlength":50},"main_comments":{"maxlength":128}},"messages":{"field_form_mail[und][0][email]":{"required":"Campo obligatorio","maxlength":"E-mail field has a maximum length of 50."},"main_comments":{"maxlength":"Leave this field blank field has a maximum length of 128."}},"errorPlacement":"5","general":{"errorClass":"error","wrapper":"li","validateTabs":0,"scrollTo":1,"scrollSpeed":"1000","disableHtml5Validation":"1","validateOnBlur":"1","validateOnBlurAlways":"0","validateOnKeyUp":"1","validateBeforeAjax":"1","validateOnSubmit":"1","showMessages":"0","errorElement":"label"}},"contact-entityform-edit-form":{"rules":{"field_form_name[und][0][value]":{"maxlength":255},"field_form_surnames[und][0][value]":{"maxlength":255},"field_form_mail[und][0][email]":{"required":true,"maxlength":50},"field_form_topic[und][0][value]":{"maxlength":255},"main_comments":{"maxlength":128}},"messages":{"field_form_name[und][0][value]":{"maxlength":"Nombre field has a maximum length of 255."},"field_form_surnames[und][0][value]":{"maxlength":"Apellidos field has a maximum length of 255."},"field_form_mail[und][0][email]":{"required":"Campo obligatorio","maxlength":"E-mail field has a maximum length of 50."},"field_form_topic[und][0][value]":{"maxlength":"Asunto field has a maximum length of 255."},"main_comments":{"maxlength":"Leave this field blank field has a maximum length of 128."}},"errorPlacement":"5","general":{"errorClass":"error","wrapper":"li","validateTabs":0,"scrollTo":1,"scrollSpeed":"1000","disableHtml5Validation":"1","validateOnBlur":"1","validateOnBlurAlways":"0","validateOnKeyUp":"1","validateBeforeAjax":"1","validateOnSubmit":"1","showMessages":"0","errorElement":"label"}},"newsletter-signup-entityform-edit-form--2":{"rules":{"field_form_mail[und][0][email]":{"required":true,"maxlength":50},"main_comments":{"maxlength":128}},"messages":{"field_form_mail[und][0][email]":{"required":"Campo obligatorio","maxlength":"E-mail field has a maximum length of 50."},"main_comments":{"maxlength":"Leave this field blank field has a maximum length of 128."}},"errorPlacement":"5","general":{"errorClass":"error","wrapper":"li","validateTabs":0,"scrollTo":1,"scrollSpeed":"1000","disableHtml5Validation":"1","validateOnBlur":"1","validateOnBlurAlways":"0","validateOnKeyUp":"1","validateBeforeAjax":"1","validateOnSubmit":"1","showMessages":"0","errorElement":"label"}}},"general":{"usexregxp":0,"months":{"Enero":1,"Ene":1,"Febrero":2,"Feb":2,"Marzo":3,"Mar":3,"Abril":4,"Abr":4,"Mayo":5,"Junio":6,"Jun":6,"Julio":7,"Jul":7,"Agosto":8,"Ago":8,"Septiembre":9,"Sep":9,"Octubre":10,"Oct":10,"Noviembre":11,"Nov":11,"Diciembre":12,"Dic":12}},"groups":{"newsletter-signup-entityform-edit-form":{},"contact-entityform-edit-form":{},"newsletter-signup-entityform-edit-form--2":{}}},"ckeditorGridTemplates":{"images":"\/profiles\/msfes\/modules\/custom\/ckeditor_grid_templates\/images\/","title_two":"Plantilla de dos columnas","desc_two":"Estructura personalizada de dos columnas para insertar contenido en dos columnas diferentes.","title_three":"Plantilla de tres columnas","desc_three":"Estructura personalizada de tres columnas para insertar contenido en tres columnas diferentes.","col1_example":"Ejemplo de contenido de la primera columna","col2_example":"Ejemplo de contenido de la segunda columna","col3_example":"Ejemplo de contenido de la tercera columna"},"urlIsAjaxTrusted":{"\/exito\/donativo":true,"\/buscador":true,"\/":true},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":0,"popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EEsta web utiliza \u0027cookies\u0027 propias y de terceros para ofrecerte una mejor experiencia y servicio. Al navegar o utilizar nuestros servicios, aceptas el uso que hacemos de las \u0027cookies\u0027. Sin embargo, puedes cambiar la configuraci\u00f3n de \u0027cookies\u0027 en cualquier momento.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button\u0022\u003EAcepto\u003C\/button\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button\u0022\u003EM\u00e1s informaci\u00f3n\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EThank you for accepting cookies\u003C\/h2\u003E\n\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button\u0022\u003EHide\u003C\/button\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button\u0022 \u003EMore info\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/politica-privacidad","popup_link_new_window":1,"popup_position":null,"popup_language":"es","domain":"","cookie_lifetime":"100"},"fitvids":{"custom_domains":[],"selectors":["body"],"simplifymarkup":true},"breakpoints":{"module.picture.empty_srcset":{"disabled":false,"api_version":1,"machine_name":"module.picture.empty_srcset","name":"Empty srcset","breakpoint":"","source":"picture","source_type":"module","status":1,"weight":0,"multipliers":{"1x":"1x"},"export_module":"picture","type":"Predeterminado","export_type":2,"in_code_only":true,"table":"breakpoints"},"breakpoints.theme.msfes_zen.mobile_menu":{"id":"222541","machine_name":"breakpoints.theme.msfes_zen.mobile_menu","name":"mobile_menu","breakpoint":"(min-width: 0px) and (max-width: 58.5em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"0","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.mobile_tablet_s":{"id":"222551","machine_name":"breakpoints.theme.msfes_zen.mobile_tablet_s","name":"mobile_tablet_s","breakpoint":"(min-width: 0px) and (max-device-width: 48em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"1","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.mobile":{"id":"222561","machine_name":"breakpoints.theme.msfes_zen.mobile","name":"mobile","breakpoint":"(min-width: 0px) and (max-width: 29.99em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"2","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.tablet_s":{"id":"222571","machine_name":"breakpoints.theme.msfes_zen.tablet_s","name":"tablet_s","breakpoint":"(min-width: 30em) and (max-device-width: 48em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"3","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.tablet":{"id":"222581","machine_name":"breakpoints.theme.msfes_zen.tablet","name":"tablet","breakpoint":"(min-device-width: 48.06em) and (max-width: 61.19em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"4","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.tablet_touch":{"id":"222591","machine_name":"breakpoints.theme.msfes_zen.tablet_touch","name":"tablet_touch","breakpoint":"(min-device-width: 58.563em) and (orientation landscape)","source":"msfes_zen","source_type":"theme","status":"1","weight":"5","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.desktop":{"id":"222601","machine_name":"breakpoints.theme.msfes_zen.desktop","name":"desktop","breakpoint":"(min-width: 61.2em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"6","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.desktop_m":{"id":"222611","machine_name":"breakpoints.theme.msfes_zen.desktop_m","name":"desktop_m","breakpoint":"(min-width: 61.2em) and (max-width: 70.5em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"7","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.desktop_l":{"id":"222621","machine_name":"breakpoints.theme.msfes_zen.desktop_l","name":"desktop_l","breakpoint":"(min-width: 70.5em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"8","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1}},"basepath":"https:\/\/www.msf.es\/profiles\/msfes\/themes\/custom\/msfes_zen","chosen":{"selector":".page-admin select:visible","minimum_single":20,"minimum_multiple":20,"minimum_width":0,"options":{"allow_single_deselect":false,"disable_search":false,"disable_search_threshold":0,"search_contains":false,"placeholder_text_multiple":"Elija algunas opciones","placeholder_text_single":"Elige una opci\u00f3n","no_results_text":"No hay resultados coincidentes","inherit_select_classes":true}}});</script>
      <!--[if lt IE 9]>
    <script src="/profiles/msfes/themes/contrib/zen/js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-thankyou-page i18n-es section-exito page-panels" >
      <p class="skip-link__wrapper">
      <a href="#main-menu" class="skip-link visually-hidden--focusable" id="skip-link">Jump to navigation</a>
    </p>
      <div class="region region-page-top">
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHSW2Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>  </div>
  
<div class="page">
    <div class="region region-top">
    <div class="block block-ft-profile first odd first odd" id="block-ft-profile-profile-link">

      
  <a href="/msflogin" class="profile-login menu__link" id="profile-login">Iniciar sesión</a>
</div>
<div class="block block-ft-form-newsletter-signup even even newsletter-header" id="block-ft-form-newsletter-signup-newsletter-header">

      
  <form class="entityform entitytype-newsletter_signup-form" action="/" method="post" id="newsletter-signup-entityform-edit-form--2" accept-charset="UTF-8"><div><h2 class="newsletter-header__title"><a href="/colabora/alta-newsletter">Suscríbete a nuestra newsletter</a></h2><div class="required-fields form-group group-newsletter last clearfix"><div class="form-item form-type-emailfield form-item-field_form_mail third-weight">
  <label for="edit-field-form-mail-und-0-email--2"> <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input placeholder="Escribe tu correo electrónico" type="email" id="edit-field-form-mail-und-0-email--2" name="field_form_mail[und][0][email]" value="" size="60" maxlength="50" class="form-text form-email required" />
</div>
</div><input type="hidden" name="form_build_id" value="form-PaBzyK3_V5lwgedKRYhdmUyVbUWVCTRHnKTOOH5cUfw" />
<input type="hidden" name="form_id" value="newsletter_signup_entityform_edit_form" />
<div class="main_comments-textfield"><div class="form-item form-type-textfield">
  <label for="edit-main-comments--2">Leave this field blank </label>
 <input autocomplete="off" type="text" id="edit-main-comments--2" name="main_comments" value="" size="20" maxlength="128" class="form-text" />
</div>
</div><div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Enviar" class="form-submit" /></div></div></form>
</div>
<div class="block block-on-the-web last odd last odd" id="block-on-the-web-0">

      
  <div class="slide" id="social-slider"><span class="on-the-web otw-facebook"><a href="https://www.facebook.com/medicossinfronteras.ong" title="Find Médicos Sin Fronteras on Facebook" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/facebook.png" alt="Find Médicos Sin Fronteras on Facebook" title="Find Médicos Sin Fronteras on Facebook" /><span class="service-name">facebook</span></a></span><span class="on-the-web otw-twitter"><a href="https://twitter.com/msf_espana" title="Find Médicos Sin Fronteras on Twitter" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/twitter.png" alt="Find Médicos Sin Fronteras on Twitter" title="Find Médicos Sin Fronteras on Twitter" /><span class="service-name">twitter</span></a></span><span class="on-the-web otw-instagram"><a href="https://www.instagram.com/msf_en_espanol/" title="Find Médicos Sin Fronteras on Instagram" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/instagram.png" alt="Find Médicos Sin Fronteras on Instagram" title="Find Médicos Sin Fronteras on Instagram" /><span class="service-name">instagram</span></a></span><span class="on-the-web otw-youtube"><a href="https://www.youtube.com/user/MedicosSinFronteras" title="Find Médicos Sin Fronteras on YouTube" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/youtube.png" alt="Find Médicos Sin Fronteras on YouTube" title="Find Médicos Sin Fronteras on YouTube" /><span class="service-name">youtube</span></a></span><span class="on-the-web otw-google"><a href="https://plus.google.com/110981584708327608177/posts" title="Find Médicos Sin Fronteras on Google+" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/google.png" alt="Find Médicos Sin Fronteras on Google+" title="Find Médicos Sin Fronteras on Google+" /><span class="service-name">google</span></a></span><span class="on-the-web otw-tumblr"><a href="http://medicossinfronteras.tumblr.com" title="Find Médicos Sin Fronteras on Tumblr" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/tumblr.png" alt="Find Médicos Sin Fronteras on Tumblr" title="Find Médicos Sin Fronteras on Tumblr" /><span class="service-name">tumblr</span></a></span><span class="on-the-web otw-vimeo"><a href="https://vimeo.com/msf" title="Find Médicos Sin Fronteras on Vimeo" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/vimeo.png" alt="Find Médicos Sin Fronteras on Vimeo" title="Find Médicos Sin Fronteras on Vimeo" /><span class="service-name">vimeo</span></a></span></div>
</div>
  </div>

    <header class="header" id="header" role="banner">

          <a href="/" title="Inicio" rel="home" class="header__logo">
        <img src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/logo.png" alt="Inicio" class="header__logo-image" />
      </a>
    
    
    
       <nav id="navigation" class="region region-navigation">
    <div class="block block-ft-msfes-header become-partner first odd first odd" id="block-ft-msfes-header-msf-become-partner">

      
  <a href="/colabora/hazte-socio" class="become-partner__link">Hazte socio</a>
</div>
<div class="block block-views even even search-block__wrapper" role="search" id="block-views-exp-content-search-results">

      
  <form class="search-block search-block--closed" action="/buscador" method="get" id="views-exposed-form-content-search-results" accept-charset="UTF-8"><div><div class="views-exposed-form">
  <div class="views-exposed-widgets clearfix">
          <div id="edit-text-wrapper" class="views-exposed-widget views-widget-filter-search_api_views_fulltext">
                        <div class="views-widget">
          <div class="form-item form-type-searchfield">
 <input class="search-block__input form-text form-search" type="search" id="edit-text" name="text" value="" size="30" maxlength="128" />
</div>
        </div>
              </div>
                    <div class="views-exposed-widget views-submit-button">
      <input type="submit" id="edit-submit-content-search" name="" value="Buscar" class="form-submit" />    </div>
      </div>
</div>
</div></form>
</div>
<div class="block block-menu-block last odd last odd" role="navigation" id="block-menu-block-msfes-main-menu">

      
  <div class="menu-block-wrapper menu-block-msfes-main-menu menu-name-main-menu parent-mlid-0 menu-level-1">
  <ul class="menu"><li class="menu__item is-expanded first expanded menu-mlid-45171"><a href="/conocenos" title="" class="menu__link">Conócenos</a><ul class="menu"><li class="menu__item is-leaf is-parent first leaf has-children menu-mlid-62171"><a href="/conocenos/quienes-somos" class="menu__link">Quiénes somos</a></li>
<li class="menu__item is-leaf leaf menu-mlid-104821"><a href="/conocenos/cuando-intervenimos" title="" class="menu__link">Cuándo intervenimos</a></li>
<li class="menu__item is-leaf leaf menu-mlid-104751"><a href="/conocenos/que-hacemos" title="" class="menu__link">Qué hacemos</a></li>
<li class="menu__item is-leaf is-parent last leaf has-children menu-mlid-62191"><a href="/conocenos/como-nos-financiamos" class="menu__link">Cómo nos financiamos</a></li>
</ul></li>
<li class="menu__item is-expanded expanded menu-mlid-68071"><a href="/actualidad" title="" class="menu__link">Actualidad</a><ul class="menu"><li class="menu__item is-leaf first leaf menu-mlid-45231"><a href="/actualidad/agenda" class="menu__link">Agenda</a></li>
<li class="menu__item is-leaf leaf menu-mlid-56971"><a href="/actualidad/desde-el-terreno" title="" class="menu__link">Desde el terreno</a></li>
<li class="menu__item is-leaf leaf menu-mlid-56981"><a href="/actualidad/publicaciones" title="" class="menu__link">Publicaciones</a></li>
<li class="menu__item is-leaf last leaf menu-mlid-107681"><a href="/actualidad/noticias" title="" class="menu__link">Noticias</a></li>
</ul></li>
<li class="menu__item is-expanded expanded menu-mlid-75071"><a href="/COLABORa" class="menu__link">Colabora</a><ul class="menu"><li class="menu__item is-leaf first leaf menu-mlid-107641"><a href="/colabora/hazte-socio" title="Tu ayuda nos permite llegar a más personas" class="menu__link">Hazte socio</a></li>
<li class="menu__item is-leaf leaf menu-mlid-107651"><a href="/colabora/doNa" title="Haz llegar acción médica a quien más lo necesita" class="menu__link">Dona</a></li>
<li class="menu__item is-leaf is-parent leaf has-children menu-mlid-92501"><a href="/colabora/empresas" title="Colabora como empresa" class="menu__link">Empresas</a></li>
<li class="menu__item is-leaf leaf menu-mlid-92561"><a href="/colabora/grandes-donativos" title="Un gran donativo tiene un gran impacto." class="menu__link">Grandes Donativos</a></li>
<li class="menu__item is-leaf leaf menu-mlid-107661"><a href="/colabora/herencias-legados" title="Alarga tu compromiso solidario" class="menu__link">Herencias y legados</a></li>
<li class="menu__item is-leaf last leaf menu-mlid-114081"><a href="/colabora/bodas-solidarias" title="¡Sí, quiero!" class="menu__link">BODAS SOLIDARIAS</a></li>
</ul></li>
<li class="menu__item is-expanded last expanded menu-mlid-68181"><a href="/trabaja" title="" class="menu__link">Trabaja</a><ul class="menu"><li class="menu__item is-leaf first leaf menu-mlid-62271"><a href="/trabaja/terreno" class="menu__link">En el terreno</a></li>
<li class="menu__item is-leaf leaf menu-mlid-62281"><a href="/trabaja/oficinas" title="" class="menu__link">En las oficinas</a></li>
<li class="menu__item is-leaf leaf menu-mlid-62291"><a href="/trabaja/captacion-calle" title="" class="menu__link">Captación en calle</a></li>
<li class="menu__item is-leaf leaf menu-mlid-90041"><a href="/trabaja/preguntas-mas-frecuentes" class="menu__link">Preguntas frecuentes</a></li>
<li class="menu__item is-leaf last leaf menu-mlid-111171"><a href="/trabaja/ofertas" title="" class="menu__link">Ofertas de trabajo</a></li>
</ul></li>
</ul></div>

</div>
  </nav>
  <a id="search-block--mobile__toogle" class="search-block--mobile__toogle" href="#">
      Open search form  </a>

  </header>
  
  <div class="main">

    <div class="main-content" role="main">
                  <a href="#skip-link" class="visually-hidden--focusable" id="main-content">Back to top</a>
                              


<div class="panel-display col2 clearfix" >
  <div class="panel-panel panel-col-first">
        <div class="inside">
      <h1 class="page__title not-dotted">¡Muchas gracias por tu apoyo!</h1>
<p class="field-subtitle"> </p><div class="panel-pane pane-entity-field pane-node-field-summary-image"  >
  
      
  
  <div class="field-collection-container clearfix"><div class="field-collection-view clearfix view-mode-summary-image-big field-collection-view-final"><div class="summary-image__image"><picture >
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source srcset="https://www.msf.es/sites/default/files/styles/summary_image_big_mobile/https/media.msf.org/Docs/MSF/Media/TR1/2/d/1/2/MSF149018.jpg?itok=N3JJb2JW&amp;timestamp=1458663021 1x" media="(min-width: 0px) and (max-width: 29.99em)" />
<source srcset="https://www.msf.es/sites/default/files/styles/summary_image_big_tabletsmall/https/media.msf.org/Docs/MSF/Media/TR1/2/d/1/2/MSF149018.jpg?itok=ah03N1zo&amp;timestamp=1458663021 1x" media="(min-width: 30em) and (max-device-width: 48em)" />
<source srcset="https://www.msf.es/sites/default/files/styles/summary_image_big_tablet/https/media.msf.org/Docs/MSF/Media/TR1/2/d/1/2/MSF149018.jpg?itok=oeWOF41N&amp;timestamp=1458663021 1x" media="(min-device-width: 48.06em) and (max-width: 61.19em)" />
<source srcset="https://www.msf.es/sites/default/files/styles/summary_image_big_desktop/https/media.msf.org/Docs/MSF/Media/TR1/2/d/1/2/MSF149018.jpg?itok=ewdnEs6j&amp;timestamp=1458663021 1x" media="(min-width: 61.2em)" />
<!--[if IE 9]></video><![endif]-->
<img  src="https://www.msf.es/sites/default/files/styles/summary_image_big_desktop/https/media.msf.org/Docs/MSF/Media/TR1/2/d/1/2/MSF149018.jpg?itok=ewdnEs6j&amp;timestamp=1458663021" alt="" title="" />
</picture></div><div class="summary-image__copyright"><span class="image__caption">Francesca Mapelli/MSF</span></div></div></div>
  
  </div>
<div class="field-body"><p>¡Muchas gracias por tu contribución!</p>
<p>Te queremos dar las gracias de parte de todo el equipo de Médicos Sin Fronteras y de las personas a la que podremos asistir. Tu colaboración, junto a la de muchas personas en todo el mundo que confían en nuestra misión, hace posible que podamos atender a miles de víctimas de catástrofes naturales, crisis olvidadas y conflictos armados, actuando en la más estricta imparcialidad y con total independencia.</p>
<p>De nuevo, gracias por tu apoyo en nombre de todas aquellas personas a las que atenderemos con tu colaboración y de todos los que trabajamos para hacerlo posible.</p>
<p>Ahora, te pedimos una última cosa, que animes a tus amigos y familiares a colaborar también con nosotros:</p>
<div class="two-col-wysiwyg-template"><a href="http://www.facebook.com/share.php?u=https://www.msf.es/dona&amp;&amp;title=He+atendido+a+una+víctima+con+Médicos+Sin+Fronteras" target="_blank"><img alt="" class="media-element file-100-width" data-delta="1" data-fid="113281" data-media-element="1" height="90" src="https://www.msf.es/sites/default/files/styles/content_scaled/public/facebook.compartir-yomequedo.png?itok=A2Xx-de3" title="" typeof="Image" width="316" /></a><a href="http://twitter.com/share?url=https://www.msf.es/dona&amp;text=He+atendido+a+una+víctima+con+Médicos+Sin+Fronteras" target="_blank"><img alt="" class="media-element file-100-width" data-delta="1" data-fid="113291" data-media-element="1" height="90" src="https://www.msf.es/sites/default/files/styles/content_scaled/public/twitter-compartir-yomequedo.png?itok=Nd4LrGmF" title="" typeof="Image" width="316" /></a></div>
<div class="two-col-wysiwyg-template"> </div>
</div>    </div>
        <div class="panel-panel panel-col-first__extras">
          </div>
  </div>

  <aside class="panel-panel panel-col-last">
    <div class="panel-pane pane-component-search"  >
  
      
  
  <div  about="/component/content_text/1191" typeof="" class="ds-1col entity entity-component component-content-text content-text view-mode-image_background clearfix">

  
  <a href="https://www.msf.es/colabora/difunde" class="content-text__title-link"><h2 class="content-text__title">Difunde</h2><div class="content-text__body"><p>No te quedes en silencio: haz correr la voz</p>
</div><img typeof="foaf:Image" src="https://www.msf.es/sites/default/files/speaker_iconb.gif" width="159" height="115" /></a></div>


  
  </div>
<div class="panel-pane pane-newsletter-component newsletter-sign-up"  >
  
        <h2 class="newsletter-sign-up__title">
      Boletín MSF    </h2>
    
  
  <form class="entityform entitytype-newsletter_signup-form" action="/exito/donativo" method="post" id="newsletter-signup-entityform-edit-form" accept-charset="UTF-8"><div><h3 class="newsletter-sign-up__subtitle">Suscríbete a nuestra newsletter</h3><p class="newsletter-sign-up__summary">Para recibir periódicamente nuestras últimas noticias.</p><div class="required-fields form-group group-newsletter last clearfix"><div class="form-item form-type-emailfield form-item-field_form_mail third-weight">
  <label for="edit-field-form-mail-und-0-email"> <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input placeholder="Escribe tu correo electrónico" type="email" id="edit-field-form-mail-und-0-email" name="field_form_mail[und][0][email]" value="" size="60" maxlength="50" class="form-text form-email required" />
</div>
</div><input type="hidden" name="form_build_id" value="form-J54Nprl27oOdD8CDiQeubnpxqfXtcOFP9b0Mzc6Bctw" />
<input type="hidden" name="form_id" value="newsletter_signup_entityform_edit_form" />
<div class="main_comments-textfield"><div class="form-item form-type-textfield">
  <label for="edit-main-comments">Leave this field blank </label>
 <input autocomplete="off" type="text" id="edit-main-comments" name="main_comments" value="" size="20" maxlength="128" class="form-text" />
</div>
</div><div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Enviar" class="form-submit" /></div></div></form>
  
  </div>
<div class="panel-pane pane-component-search"  >
  
      
  
  
  
  </div>
  </aside>
</div>
          </div>

    
    
  </div>

  <footer id="footer" class="footer__wrapper">
    <div class="footer_top__wrapper">
        <div class="region region-footer-top">
    <div class="block block-menu first last odd first last odd" role="navigation" id="block-menu-menu-footer-highlighted">

      
  <ul class="menu"><li class="menu__item is-expanded first expanded"><a href="/conocenos" title="" class="menu__link">Conócenos</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/conocenos/quienes-somos" title="" class="menu__link">Quiénes somos</a></li>
<li class="menu__item is-leaf leaf"><a href="/conocenos/contextos" title="" class="menu__link">Cuándo intervenimos</a></li>
<li class="menu__item is-leaf leaf"><a href="/conocenos/actividades-medicas" title="" class="menu__link">Qué hacemos</a></li>
<li class="menu__item is-leaf last leaf"><a href="/conocenos/como-nos-financiamos" title="" class="menu__link">Cómo nos financiamos</a></li>
</ul></li>
<li class="menu__item is-expanded expanded"><a href="/actualidad" title="" class="menu__link">Actualidad</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/actualidad/archivo" title="" class="menu__link">Archivo de noticias</a></li>
<li class="menu__item is-leaf leaf"><a href="/actualidad/agenda" title="" class="menu__link">Agenda</a></li>
<li class="menu__item is-leaf leaf"><a href="/actualidad/desde-el-terreno" title="" class="menu__link">Desde el terreno</a></li>
<li class="menu__item is-leaf last leaf"><a href="/actualidad/publicaciones" title="" class="menu__link">Publicaciones</a></li>
</ul></li>
<li class="menu__item is-expanded expanded"><a href="/COLABORa" title="" class="menu__link">Colabora</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/colabora/hazte-socio" title="¡Únete a Médicos Sin Fronteras!" class="menu__link">Hazte socio</a></li>
<li class="menu__item is-leaf leaf"><a href="/colabora/doNa" title="Haznos un donativo" class="menu__link">Dona</a></li>
<li class="menu__item is-leaf leaf"><a href="https://iniciativassolidarias.msf.es/" title="Crea una iniciativa a nuestro favor" class="menu__link">¡Ten iniciativa!</a></li>
<li class="menu__item is-leaf last leaf"><a href="/colabora/empresas" title="Muchas empresas colabora con nosotros, ¡únete a ellas!" class="menu__link">Empresas</a></li>
</ul></li>
<li class="menu__item is-expanded expanded"><a href="/trabaja" title="" class="menu__link">Trabaja</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/trabaja/terreno" title="" class="menu__link">En el terreno</a></li>
<li class="menu__item is-leaf leaf"><a href="/trabaja/oficinas" title="" class="menu__link">En la sede</a></li>
<li class="menu__item is-leaf leaf"><a href="/trabaja/captacion-calle" title="" class="menu__link">Captación en calle</a></li>
<li class="menu__item is-leaf last leaf"><a href="/trabaja/preguntas-mas-frecuentes" title="" class="menu__link">Preguntas frecuentes</a></li>
</ul></li>
<li class="menu__item is-expanded last expanded"><span title="" class="menu__link nolink">Destacados</span><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/conocenos/quienes-somos/enviamos-la-ayuda" title="" class="menu__link">Nuestra logística</a></li>
<li class="menu__item is-leaf leaf"><a href="/conocenos/quienes-somos/historia" title="" class="menu__link">Un poco de historia</a></li>
<li class="menu__item is-leaf leaf"><a href="/colabora/principios-msf-recaudacion-fondos" title="Compromisos y principios que nos definen." class="menu__link">Nuestros principios</a></li>
<li class="menu__item is-leaf last leaf"><a href="/colabora/preguntas-frecuentes" title="Todo lo que necesitas saber" class="menu__link">Preguntas frecuentes</a></li>
</ul></li>
</ul>
</div>
  </div>
    </div>
    <div class="footer_bottom__wrapper clearfix">
        <div class="region region-footer-bottom">
   <div class="block block-ft-msfes-footer first odd first odd contact-info" id="block-ft-msfes-footer-msf-contact-info">

      
  <span class="contact-info__phone"><a href="tel:900 494 269" class="contact-info__phone-link">900 494 269</a><em class="contact-info__phone-text">(Gratuito)</em></span><a href="mailto:oficina@barcelona.msf.org" class="contact-info__mail" id="footer-contact-mail">oficina@barcelona.msf.org</a><a href="http://maps.google.es/maps?q=Nou de la Rambla 26, 08001 Barcelona" class="contact-info__postal" id="postal-address">Nou de la Rambla 26, 08001 Barcelona</a>
</div>
<div class="block block-ft-msfes-footer last even last even site-selector" id="block-ft-msfes-footer-msf-site-selector">

        <h2 class="block__title block__title">Otras webs MSF</h2>
    
  <form action="/" method="post" id="site-selector-form" accept-charset="UTF-8"><div><div class="form-item form-type-select">
 <select id="edit-site-selector" name="site_selector" class="form-select"><option value="0">Seleccionar...</option><optgroup label="Páginas de países"><option value="http://www.aerzte-ohne-grenzen.de/">Alemania
</option><option value="http://www.msf.org.ar/">Argentina
</option><option value="http://www.msf.org.au/">Australia
</option><option value="http://www.aerzte-ohne-grenzen.at/">Austria
</option><option value="http://www.msf.be/">Bélgica
</option><option value="http://www.msf.org.br/">Brasil
</option><option value="http://www.msf.ca/">Canadá
</option><option value="http://www.msf.or.kr/">Corea del Sur
</option><option value="http://msf.dk/">Dinamarca
</option><option value="http://www.msf-me.org/en/intro">Emiratos Árabes Unidos
</option><option value="http://www.msf.es/">España
</option><option value="http://www.doctorswithoutborders.org/">Estados Unidos
</option><option value="http://msf.fr/">Francia
</option><option value="http://www.msf.gr/">Grecia
</option><option value="http://www.artsenzondergrenzen.nl">Holanda
</option><option value="http://www.msf.org.hk/index.php?lang=tc">Hong Kong
</option><option value="http://www.msfindia.in/">India
</option><option value="http://www.msf.ie/">Irlanda
</option><option value="http://www.medicisenzafrontiere.it/">Italia
</option><option value="http://www.msf.or.jp/">Japón
</option><option value="http://www.msf.lu/">Luxemburgo
</option><option value="http://www.msf.mx">México
</option><option value="http://www.legerutengrenser.no/">Noruega
</option><option value="http://www.msf.org.uk/">Reino Unido
</option><option value="http://www.lekari-bez-hranic.cz/">República Checa
</option><option value="http://www.ru.msf.org/">Rusia
</option><option value="http://somalia.msf.org">Somalia
</option><option value="http://www.msf.org.za/">Sudáfrica
</option><option value="http://www.lakareutangranser.se/">Suecia
</option><option value="http://www.msf.ch/">Suiza</option></optgroup><optgroup label="Otras páginas"><option value="http://www.msfaccess.org/">Campaña de Acceso
</option><option value="http://www.msf-crash.org/en">CRASH
</option><option value="http://evaluation.msf.org/">Unidad de Evaluación
</option><option value="http://fieldresearch.msf.org/msf/">Investigación operacional
</option><option value="http://www.refbooks.msf.org/">Biblioteca de MSF
</option><option value="http://www.msf-ureph.ch/">UREPH</option></optgroup></select>
</div>
<input type="submit" id="edit-button" name="op" value="Ir" class="form-submit" /><input type="hidden" name="form_build_id" value="form-KPkwTlcJRbuz43wfgIvsQ85H1tJpstlI_EhaLIYgmGY" />
<input type="hidden" name="form_id" value="site_selector_form" />
</div></form>
</div>
 </div>
 <div id="footer-map" class="footer-map"></div>
        <div class="region region-footer-bottom-hidden">
    <div class="block block-ft-form-contact first last odd first last odd quick-contact" id="block-ft-form-contact-msf-contact-form">

      
  <div class="quick-contact__info">
  <h2 class="quick-contact__title">Desde aquí atenderemos tus preguntas y comentarios sobre temas generales de MSF.</h2>
  <div class="quick-contact__body"></div>
</div>
<form class="entityform entitytype-contact-form" action="/exito/donativo" method="post" id="contact-entityform-edit-form" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-field_form_name third-weight">
  <label for="edit-field-form-name-und-0-value">Nombre </label>
 <input class="text-full form-text" type="text" id="edit-field-form-name-und-0-value" name="field_form_name[und][0][value]" value="" size="60" maxlength="255" />
</div>
<input type="hidden" name="form_build_id" value="form-raMXM8BuoZ704Tb2rd6kcdzWjQlYyfWsqa9RQhfNwWs" />
<input type="hidden" name="form_id" value="contact_entityform_edit_form" />
<div class="form-item form-type-textfield form-item-field_form_surnames third-weight">
  <label for="edit-field-form-surnames-und-0-value">Apellidos </label>
 <input class="text-full form-text" type="text" id="edit-field-form-surnames-und-0-value" name="field_form_surnames[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-emailfield form-item-field_form_mail third-weight">
  <label for="edit-field-form-mail-und-0-email">E-mail <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input type="email" id="edit-field-form-mail-und-0-email" name="field_form_mail[und][0][email]" value="" size="60" maxlength="50" class="form-text form-email required" />
</div>
<div class="form-item form-type-textfield form-item-field_form_topic third-weight">
  <label for="edit-field-form-topic-und-0-value">Asunto </label>
 <input class="text-full form-text" type="text" id="edit-field-form-topic-und-0-value" name="field_form_topic[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textarea form-item-field_form_message">
  <label for="edit-field-form-message-und-0-value">Mensaje </label>
 <div class="form-textarea-wrapper resizable"><textarea class="text-full form-textarea" id="edit-field-form-message-und-0-value" name="field_form_message[und][0][value]" cols="60" rows="5"></textarea></div>
</div>
<div class="main_comments-textfield"><div class="form-item form-type-textfield">
  <label for="edit-main-comments">Leave this field blank </label>
 <input autocomplete="off" type="text" id="edit-main-comments" name="main_comments" value="" size="20" maxlength="128" class="form-text" />
</div>
</div><div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Enviar" class="form-submit" /></div></div></form>
</div>
  </div>
    </div>
    <div class="bottom__wrapper">
        <div class="region region-bottom">
    <div id="social-share" class="social-share__wrapper block block-ft-social first odd first odd">
  <div class="social-share">
              <h2 id="social-share__title" class="social-share__title">Compartir</h2>
            <div class="item-list"><ul class="social-share__list"><li class="social-share__item facebook first"><a href="http://facebook.com/share.php?u=https://www.msf.es/exito/donativo" target="_blank"><img src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/facebook.png" width="31" height="31" alt="Compartir en Facebook" /></a></li>
<li class="social-share__item twitter"><a href="http://twitter.com/intent/tweet?text=¡Muchas gracias por tu apoyo!&amp;url=https://www.msf.es/exito/donativo&amp;via=MSF_espana" target="_blank"><img src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/twitter.png" width="31" height="31" alt="Compartir en Twitter" title="Compartir en Twitter" /></a></li>
<li class="social-share__item google"><a href="http://plus.google.com/share?url=https://www.msf.es/exito/donativo" target="_blank"><img src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/google.png" width="31" height="31" alt="Compartir en Google Plus" title="Compartir en Google Plus" /></a></li>
<li class="social-share__item email last"><a href="mailto:?body=https://www.msf.es/exito/donativo" target="_blank"><img src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/mail.png" width="31" height="31" alt="Compartir por correo electrónico" title="Compartir por correo electrónico" /></a></li>
</ul></div>  </div>
</div>
<div class="block block-menu last even last even" role="navigation" id="block-menu-menu-footer-menu">

      
  <ul class="menu"><li class="menu__item is-leaf first leaf"><a href="http://asociacion.msf.es/" title="" class="menu__link">Web del asociativo</a></li>
<li class="menu__item is-leaf leaf"><a href="/nota-legal" title="" class="menu__link">Nota legal</a></li>
<li class="menu__item is-leaf leaf"><a href="/politica-de-privacidad" class="menu__link">Política de privacidad</a></li>
<li class="menu__item is-leaf last leaf"><a href="/contacta" title="" class="menu__link">Contacta</a></li>
</ul>
</div>
  </div>
    </div>
  </footer>

</div>
  <script src="https://www.msf.es/sites/default/files/js/js_ozYjVG-rUp8TX8E0m7n8fVZPTn3NV-Jx4paMuSf_7LQ.js"></script>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"01415a1329","applicationID":"10746259","transactionName":"bwRaNksHXUtVUxZYVlZOeQFND1xWG0ADVlxnDFkMWAFWSmteDVVcZxdRB045Q1lTVQ==","queueTime":0,"applicationTime":560,"atts":"Q0NZQAMdTkU=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>
