<?php include ( 'includes/header.php' ); ?>
<?php
    $amounts = array(25, 50, 100);

    $header = '4kvBBiGesxiyuiHuQir7Vtyclz7x5ytxzvAB9p24GK5HUF3eKwYMMQSVCfk30ADKP10MyXseXhhjFjltE81soQKWBFUeJcMSTriUJyC38g3jXrgEiOQ0UxxRLLodoAEyEzL0MhKOjzV5lSla2vJz51EvYFCSRbhWnyu3Mijv7hCXWkcCBH0j8DVwHGehYoLuPVJ56NMw';
    $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/cpm/v1.0/PaymentMethods');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, false);
    //curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
    //$headers['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
    $curl_response = curl_exec($curl);
    if ($curl_response === false) {
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additioanl info: ' . var_export($info));
    }
    curl_close($curl);
    $temp = json_decode ( $curl_response );
    $paymentMethods = $temp->PaymentMethods;

    //if ( $_GET['pay'] != '' ) {
    if ( isset($_GET['pay']) ) {
        
        $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/v1/ReversedInstallment/' . $_GET['pay']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, false);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
        //$headers['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $temp = json_decode ( $curl_response );
    }
?>
<style>
.amountSelector input[name=amounts] {
    display: none;
}
.amountSelector {
    list-style-type:none;
    margin:0px;
    padding:0;
}
.amountSelector li {
    float: left;
    border: 1px solid gray;
    padding: 10px;
    box-shadow: 0 0 0 1px rgba(0, 0, 0, .2), 0 1px 2px rgba(0, 0, 0, .2), inset 0 1px 2px rgba(255, 255, 255, .7);

}
.amountSelector li label {
    margin: 0px;
}
.amountSelector li:first-child {
  border-radius: 8px 0px 0px 8px;
}
.amountSelector li:last-child {
  border-radius: 0px 8px 8px 0px;
}
.amountSelector li.checked {
  background-image: linear-gradient(rgba(0,0,0,.1), rgba(0,0,0,0));
    box-shadow: inset 0 0 2px rgba(0,0,0,.2), inset 0 2px 5px rgba(0,0,0,.2), 0 1px rgba(255,255,255,.2);
}
.hide {
  display: none;
}
li input[type=text] {
  display: none;
}
li.checked input[type=text] {
  display: inline;
  padding: 0px 10px;
  height: 26px;
}
li.checked .amounts-other-label {
    display: none;
}
li.custom-amount.checked {
    padding: 8px 10px;
}
.giftaid-details {
    display: none;
}


</style>
    <section class="about-us-st section-padding">
        <div class="container">

            <?php
            if (isset($_GET['pay']) ) { 
                $disabled = ' disabled="disabled"';
                ?>
            <h2>Sorry<span> to bother you</span></h2>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-2">
                    <?php
                    echo "Hi " . $temp->firstName .",<br />";
                    echo "It seems something went wrong with your direct debit for <span class=\"currency\">". round( $temp->amount, 2) ."</span> in ".$temp->month.". The problem reported with this transaction was: <strong>Insufficient funds in your account</strong><br /><br /> You can pay this month using one of the payment methods below";
                    ?>
                    <br /><br />
                </div>
            </div>
            <?php } else { 
                $disabled = '';
                ?>
                <h2>Donate <span> to our cause</span></h2>
            <?php } ?>
            <div class="col-sm-10 col-sm-offset-2">
                <div id="feedback">

                </div>
            </div>
            <form class="form-horizontal" method="post" action="" id="donateform">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Firstname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="<?php echo $temp->firstName; ?>" name="firstname" id="firstname" <?php echo $disabled; ?> />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lastname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="<?php echo $temp->lastName; ?>" name="Lastname" id="Lastname" <?php echo $disabled; ?>/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" value="<?php echo $temp->email; ?>" id="email" <?php echo $disabled; ?>/>
                    </div>
                </div>
                <?php

                if ( isset($_GET['pay']) ) { ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Amount</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control currency" name="selectedAmount" value="<?php echo $temp->amount; ?>" id="selectedAmount" disabled="disabled" />
                        </div>
                    </div>

                <?php
                }else { ?>
                <div class="form-group">
                <label class="col-sm-2 control-label">Amount</label>
                    <div class="col-sm-10">
                        <ul class="amountSelector">
                        <?php 
                        $checked = 'checked';
                        foreach($amounts AS $availableAmount ) { ?>
                        <li class="<?php echo $checked; ?>">
                        <input type="radio" name="amounts" id="amounts-<?php echo $availableAmount; ?>" value="<?php echo $availableAmount; ?>" <?php echo $checked; ?> />
                        <label for="amounts-<?php echo $availableAmount; ?>" class="currency"> <?php echo $availableAmount; ?> </label>
                        </li>
                        <?php 
                        $checked = '';
                        } 
                        ?>
                        <li class="custom-amount">
                        <input type="radio" name="amounts" id="amounts-other" value="other" />
                        <label for="amounts-other" class="amounts-other-label"> Other </label>
                        <input type="text" placeholder="Amount" class="form-control custom-amount-input currency" name="amount" id="custom-amount-input" />
                        </li>
                        </ul>
                    </div>
                </div>
                <input type="hidden" id="selectedAmount" name="selectedAmount" value=""/>
                <?php } ?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Payment method</label>
                    <div class="col-sm-10">
                        <select name="paymentmethod" class="form-control">
                            <option> -- Select your Payment Method -- </option>
                            <?php
                                foreach ( $paymentMethods as $key=>$value ) {
                                    echo "<option>" . $value->Name ."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <?php
                    if ( isset($_GET['pay']) ) {

                    }else {
                        ?>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="giftaid" id="giftaid" /> Yes, I want to Gift Aid all my donation past, present and future.
                                        </label>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group giftaid-details">
                                <label for="giftaid-postalcode" class="col-sm-2 control-label">Postal code</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="giftaid-postalcode" name="giftaid-postalcode" placeholder="postalcode" />
                                </div>
                            </div>
                            <div class="form-group giftaid-details">
                                <label for="giftaid-housenumber" class="col-sm-2 control-label">Housenumber</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="giftaid-housenumber" name="giftaid-housenumber" placeholder="housenumber" />
                                </div>
                            </div>
                            <div class="form-group giftaid-details">
                                <label for="giftaid-housenumber" class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <label id="giftaid-confirmation">I am a UK taxpayer and understand that if I pay less Income Tax and/or Capital Gains Tax than the amount of Gift Aid claimed on all my donations in that tax year it is my responsibility to pay any difference.</label>

                                </div>
                            </div>

                        <?php
                    }
                ?>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="pull-right">
                            <?php
                                if ( isset($_GET['pay']) ) {
                                    echo '<input type="hidden" name="type" value="pay" />';
                                    echo '<input type="hidden" name="installmentId" value="'.$_GET['pay'].'" />';
                                    echo '<input type="submit" class="btn theme-btn" id="donate" value="Pay" />';
                                }else {
                                    echo '<input type="hidden" name="type" value="donate" />';
                                    echo '<input type="submit" class="btn theme-btn" id="donate" value="Donate" />';
                                }

                            ?>
                            <div id="donate-load" style="display:none;">
                                <img src="images/loading.gif" /> Hold on while we're redirect you.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            </form>
        </div>
    </section>

<?php include ( 'includes/footer.php' ); ?>
    <script type="text/javascript">
        $('input[type=radio]').on('change', function () {
            console.log(this.id);
            $('li.checked').removeClass('checked');
            $('li:has(input[name=amounts]:checked)').addClass('checked');
                getAmount();
        });
        $('#custom-amount-input').on('change', function () {
            getAmount();
        });
        function getAmount() {
            var selected = $('input[name=amounts]:checked').val();
              if(selected == 'other') {
                selected = $('#custom-amount-input').val();
                $('#custom-amount-input').focus();
              }
            if(selected != undefined) {
                console.log('amount set to: '+selected);
                $('#selectedAmount').val(selected);
            } else {
                console.log('amount determined by installment: '+$('#selectedAmount').val());
            }
          return selected;
        }
        $( document ).ready( function() {
            getAmount();
            $('form#donateform').submit(function (event) {
                $('input#donate').hide();
                $('#donate-load').toggle();
                var datatemp = $('form#donateform').serialize();
                $.ajax({
                    type: 'POST',
                    url: 'post.php',
                    crossDomain: true,
                    data: datatemp,
                    success: function (data) {
                        console.log('response: '+data);
                        var obj = jQuery.parseJSON( data );
                        //console.log('response:'+obj);
                        if (obj.IsSuccess ) {
                            $(location).attr('href', obj.RedirectURL);
                        }else {
                            $('#feedback').html ('');
                            $('#feedback').html ('<span style="color:#ff0000;">Something went wrong: ' + obj.Error.error_message + '</span>');
                            $('input#donate').show();
                            $('#donate-load').toggle();
                        }
                    }
                });

                event.preventDefault();
                
            });
            $('#giftaid').click(function() {
                if( $(this).is(':checked')) {
                    $(".giftaid-details").show();
                } else {
                    $(".giftaid-details").hide();
                }
            }); 
        });
    </script>
</body>
</html>
