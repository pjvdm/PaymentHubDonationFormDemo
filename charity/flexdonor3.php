<?php include ( 'includes/header.php' ); 
$amounts = array(5, 10, 25);
?>
<style>
.paymentMethodSelection {
    width: 400px;
}
@media (min-width: 500px) {
    .paymentMethodSelection {
        width: 500px;
    }    
}

.paymentMethodSelection .paymentMethodCard {
    display: none;
}
.paymentMethodSelection .paymentMethodCard.checked {
    display: block;
}
.paymentMethodCard {
  width: 97% !important;
  height: auto;
  padding: 5px 10px;
  float: left;
  cursor:pointer;
  border: 1px solid gray;
  border-radius: 8px;
  margin-bottom: 7px !important;
  margin-left: 7px;
  margin-right: 7px;
  opacity: .3;
}
.paymentMethodImage {
    margin-right: 10px;
}
.paymentMethodName {
    margin-right: 10px;
    font-weight: bold;
}
.paymentMethodCard .editIcon {
    float: right;
}
.amountCard {
  width: 130px !important;
  height: 80px;
  padding: 10px 5px;
  vertical-align: top;
  float: left;
  cursor:pointer;
  border: 1px solid gray;
  border-radius: 8px;
  margin-bottom: 7px !important;
  margin-left: 7px;
  opacity: .3;
}
.amountCard .checkmark {
    display: none;
}
.paymentMethodCard .checkmark {
    display: none;
}
.checked {
    border: none;
    box-shadow: 2px 2px 8px #888888;
    opacity: 1;
}
.checked .checkmark {
    display: inline;
    position: absolute;
    top: 10px;
    right: 10px;
}
.paymentMethodCard label {
    cursor: inherit;
    font-weight: inherit;
}


/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
    max-width: 500px;
}
.modal input {
    width: auto;
}

/* The Close Button */
.close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}

/*******
amount selector 
*******/

.amountInput {
    border: none;
    width: auto;
    overflow: visible;
}
.amountInput.editable {
    border: 1px solid #000;
    width: 100px;
}   

.amountSelector input[name=amounts] {
    display: none;
}
.amountSelector {
    list-style-type:none;
    margin:0px;
    padding:0;
}
.amountSelector li {
    float: left;
    border: 1px solid gray;
    padding: 10px;
    box-shadow: 0 0 0 1px rgba(0, 0, 0, .2), 0 1px 2px rgba(0, 0, 0, .2), inset 0 1px 2px rgba(255, 255, 255, .7);

}
.amountSelector li label {
    margin: 0px;
}
.amountSelector li:first-child {
  border-radius: 8px 0px 0px 8px;
}
.amountSelector li:last-child {
  border-radius: 0px 8px 8px 0px;
}
.amountSelector li.checked {
  background-image: linear-gradient(rgba(0,0,0,.1), rgba(0,0,0,0));
    box-shadow: inset 0 0 2px rgba(0,0,0,.2), inset 0 2px 5px rgba(0,0,0,.2), 0 1px rgba(255,255,255,.2);
}
.hide {
  display: none;
}
li input[type=text] {
  display: none;
}
li.checked input[type=text] {
  display: inline;
  padding: 0px 10px;
  height: 26px;
}
li.checked .amounts-other-label {
    display: none;
}
li.custom-amount.checked {
    padding: 8px 10px;
}
</style>
    <?php
    $campaignId = '';
    if ( $_GET['cid'] != '' ) {
        //die ('jorrit');
        $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/v2/FlexDonor/' . $_GET['cid']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, false);
        
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . $header));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $temp = json_decode ( $curl_response );
        $campaignId = $temp->campaignId;
    }   
    ?>
    <style>
    .slidecontainer {
        width: 100%;
    }
    @media (min-width: 600px) {
        .slidecontainer {
            width: 500px;
        }
    }
    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 15px;
        border-radius: 5px;   
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 25px;
        height: 25px;
        border-radius: 50%; 
        background: #ff9900;
        cursor: pointer;
    }

    .slider::-moz-range-thumb {
        width: 25px;
        height: 25px;
        border-radius: 50%;
        background: #ff9900;
        cursor: pointer;
    }
</style>
    
    <!-- The Modal -->
<div id="paymentMethodPopup" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Select your preferred payment method</p>
    <div class="row content">
                    <?php 
                    $checked = true;
                    foreach( $temp->authorizedPaymentMethods AS $authorizedMethod ) { 
                        $checkedClass = '';
                        $checkedIndicator = '';
                        if($checked) {
                            $checkedClass = ' checked';
                            $checkedIndicator = 'checked';
                        }
                    ?>
                    <div class="col col-md-2 col-xs-2 paymentMethodCard<?php echo $checkedClass; ?>">
                    <label for="paymentmethod-<?php echo $authorizedMethod->identifier ?>">
                    <input type="radio" name="paymentmethods" value="<?php echo $authorizedMethod->name; ?>" id="paymentmethod-<?php echo $authorizedMethod->identifier; ?>" class="hide" <?php echo $checkedIndicator ?>>
                    
                    <?php 
                    if($authorizedMethod->imageName != null) {
                        echo '<img src="./images/Logo-'.$authorizedMethod->imageName.'.PNG" class="paymentMethodImage" />';
                    }
                    ?><span class="paymentMethodName"><?php echo $authorizedMethod->name; ?></span>
                    <span class="identifier" style="font-size:9pt;"><?php echo $authorizedMethod->identifier; ?></span>
                    <input type="hidden" name="profileId" id="profileId" class="profileId" value="<?php echo $authorizedMethod->paymentProfile->Id; ?>"/>
                    </label>
                    </div>
                    <?php 
                    $checked = false;
                    } 
                    ?>
                    
                    </div>

  </div>

</div>

    <!-- start latest-causes -->
        <section class="latest-causes section-padding" id="recent">
            <div class="container">
                <div class="row section-title-s2">
                    <div class="col col-xs-12">
                        <h2><span>recent</span> causes</h2>
                    </div>
                </div> <!-- end section-title -->
                
                    Hi <?php echo $temp->firstName; ?>,
                    <br /><br />
                    Thank you for your flex-donorship. We have prepared a selection of projects for you to donate to.

                    First, please choose the amount you would like to donate:
                    <br /><br/><p>
                        <div class="amountSelection">
                        <h3>Selected amount: <img src="images/edit.png" id="amountEdit" /><input type="number" class="currency amountInput" id="selectedAmount" name="selectedAmount" /></h3>
                        <p>use the slider to adjust your donation amount</p>
                        <div class="slidecontainer">
                          <input type="range" min="5" max="250" value="25" class="slider" id="myRange">
                        </div>
                    </div>
                    <br /><br/><br/>
                    <div>
                    Now, when you click one of the projects below, we automatically collect the donation using the selected payment method.
                    </div>
                    <form method="post">
                    <input type="hidden" name="campaignId" id="campaignId" value="<?php echo $campaignId;?>" />
                    <div class="checkmark"></div>
                    <h4>Your payment method</h4>
                    <div class="row content paymentMethodSelection">
                    <?php 
                    $checked = true;
                    foreach( $temp->authorizedPaymentMethods AS $authorizedMethod ) { 
                        $checkedClass = '';
                        $checkedIndicator = '';
                        if($checked) {
                            $checkedClass = ' checked';
                            $checkedIndicator = 'checked';
                        }
                    ?>
                    <div class="col col-md-2 col-xs-2 paymentMethodCard<?php echo $checkedClass; ?>">
                    <img src="images/edit.png" class="editIcon" />
                    <label for="paymentmethod-<?php echo $authorizedMethod->identifier ?>">
                    <input type="radio" name="paymentmethod" value="<?php echo $authorizedMethod->paymentProfile->Id; ?>" id="real-paymentmethod-<?php echo $authorizedMethod->identifier; ?>" class="hide" <?php echo $checkedIndicator ?>>
                    
                    <?php 
                    if($authorizedMethod->imageName != null) {
                        echo '<img src="./images/Logo-'.$authorizedMethod->imageName.'.PNG" class="paymentMethodImage" />';
                    }
                    ?><span class="paymentMethodName"><?php echo $authorizedMethod->name; ?></span>
                    <span class="identifier" style="font-size:9pt;"><?php echo $authorizedMethod->identifier; ?></span>
                    <input type="hidden" name="profileId" id="profileId" class="profileId" value="<?php echo $authorizedMethod->paymentProfile->Id; ?>"/>
                    </label>
                    </div>
                    <?php 
                    $checked = false;
                    } 
                    ?>
                    <div class="col col-md-2 col-xs-2 paymentMethodCard">
                    <label for="paymentmethod-new">
                    <span class="paymentMethodDetails">
                    <br/><strong>   Add new</strong>
                    <br/><br/>
                    <span class="identifier"><br/></span>
                    <input type="hidden" name="profileId" id="profileId" class="profileId" value="<?php echo $authorizedMethod->paymentProfile->Id; ?>"/>
                    </span>
                    </label>
                    </div>
                    
                    </div>
                    <input type="hidden" name="contactid" id="contactid" value="<?php echo $temp->contactId; ?>" />
                    <input type="hidden" name="accountid" id="accountid" value="<?php echo $temp->accountId; ?>" />
                    <input type="hidden" name="cid" id="cid" value="<?php echo $_GET['cid']; ?>" />
                </form>
                <div id="allcauses">
                <br/><br/>
                    <div class="row content">
                        <div class="col col-md-4 col-xs-6">
                            <div class="title">
                                <ul>
                                    <li><h3>Sponsor a school</h3></li>
                                    <li><i class="fa fa-map-marker"></i> Yemen</li>
                                </ul>                            
                            </div>
                            <div class="img-progress">
                                <div class="img-holder">
                                    <img src="images/latest-causes/img-1.jpg" alt class="img img-responsive">
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" data-percent="80"></div>
                                </div>
                            </div>
                            <div class="donate-amount">
                                <ul>
                                    <li>Raised: <span class="currency">8,001</span> / <span  class="currency">10,000</span></li>
                                    <li><i class="fa fa-clock-o"></i> 106 days</li>
                                </ul>
                            </div>
                            <div class="text">
                                <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                                <a href="#" class="btn theme-btn donate-button"><?php echo CLICK;?></a>
                            </div>
                        </div>

                        <div class="col col-md-4 col-xs-6">
                            <div class="title">
                                <ul>
                                    <li><h3>Build a water pump</h3></li>
                                    <li><i class="fa fa-map-marker"></i> Kenya</li>
                                </ul>                            
                            </div>
                            <div class="img-progress">
                                <div class="img-holder">
                                    <img src="images/latest-causes/img-2.jpg" alt class="img img-responsive">
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" data-percent="4"></div>
                                </div>
                            </div>
                            <div class="donate-amount">
                                <ul>
                                    <li>Raised: <span class="currency">567</span> / <span class="currency">15,000</span></li>
                                    <li><i class="fa fa-clock-o"></i> 156 days</li>
                                </ul>
                            </div>
                            <div class="text">
                                <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                                <a href="#" class="btn theme-btn"><?php echo CLICK;?></a>
                            </div>
                        </div>

                        <div class="col col-md-4 col-xs-6">
                            <div class="title">
                                <ul>
                                    <li><h3>Dig a well</h3></li>
                                    <li><i class="fa fa-map-marker"></i> Syria</li>
                                </ul>                            
                            </div>
                            <div class="img-progress">
                                <div class="img-holder">
                                    <img src="images/latest-causes/img-3.jpg" alt class="img img-responsive">
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" data-percent="11"></div>
                                </div>
                            </div>
                            <div class="donate-amount">
                                <ul>
                                    <li>Raised: <span class="currency">128</span> / <span class="currency">5,000</span></li>
                                    <li><i class="fa fa-clock-o"></i> 156 days</li>
                                </ul>
                            </div>
                            <div class="text">
                                <p>Iste natus error sit voluptatem accusantium dolo remque laudantium.</p>
                                <a href="#" class="btn theme-btn"><?php echo CLICK;?></a>
                            </div>
                        </div>
                    </div> <!-- end content -->
                </div>
                <div id="allcauses-thanks" style="display:none;">
                    <h3>Thanks <?php echo $temp->firstname;?>!</h3> Great choice!
                    <br/> A payment of <span class="currency amount-output"></span> will be charged using the selected payment method.
                    <br/> <span id="cancelLink">If you didn't intend to do this, <a href="#">
                    Click here to cancel...<span style="display: none;" id="timer"></span></a></span>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end latest-causes -->

<?php include ( 'includes/footer.php' ); ?>
<script type="text/javascript">
var slider = document.getElementById("myRange");
var output = document.getElementById("sliderAmount");
$('#selectedAmount').val(slider.value);
$('#selectedAmount').prop('readonly', true);
//$('#selectedAmount').val(slider.value);

// Update the current slider value (each time you drag the slider handle)
slider.oninput = function() {
    $('#selectedAmount').val(this.value);
}
$('#selectedAmount').click( function () {
});
$('#selectedAmount').on('change', function () {
    $('#myRange').val(this.value);
});
$('#amountEdit').click( function() {
    $('#selectedAmount').prop('readonly', false);
    $('#selectedAmount').addClass('editable');
    $('#amountEdit').hide();
});
$('#selectedAmount').focusout( function() {
    $('#selectedAmount').prop('readonly', true);
    $('#selectedAmount').removeClass('editable');
    $('#amountEdit').show();
});


</script>
    <script type="text/javascript">
        $( ".paymentMethodSelection .paymentMethodCard label" ).click(function(e) {
            $("#paymentMethodPopup").show();
            e.preventDefault();
        });
        $( ".modal .close").click(function() {
            $("#paymentMethodPopup").hide();
        });
        $( ".paymentMethodSelection .paymentMethodCard").click(function() {
            $("#paymentMethodPopup").show();
        });
        // $( "input[name=paymentmethod]" ).click(function() {
        //     $(".paymentMethodCard, .checked").removeClass( "checked" );
        //     $(".paymentMethodCard:has(input[name=paymentmethod]:checked)").addClass( "checked" );
        // });
        $( "input[name=paymentmethods]" ).click(function(e) {
            $(".paymentMethodSelection .paymentMethodCard.checked").removeClass( "checked" );
            $(".paymentMethodCard.checked").removeClass( "checked" );
            $("#real-"+e.target.id).prop('checked', true);
            $(".paymentMethodCard:has(input[name=paymentmethod]:checked)").addClass( "checked" );
            $(".paymentMethodCard:has(input[name=paymentmethods]:checked)").addClass( "checked" );
            
            console.log($('input[name=paymentmethod]:checked').val());

            $("#paymentMethodPopup").hide();
        });
        // $( "div.amountCard" ).click(function(e) {
        //     $(".amountCard, .checked").removeClass( "checked" );
        //     console.log(e);
        //     $(e.currentTarget).addClass("checked");
        //     //$(".amountCard:has(input[name=amounts]:checked)").addClass( "checked" );
        // });
        $('#custom-amount-input').on('change', function () {
            //getAmount();
        });
        // function getAmount() {
        //     var selected = $('input[name=amounts]:checked').val();
        //       if(selected == 'other') {
        //         selected = $('#custom-amount-input').val();
        //       }
        //     console.log('value: '+selected);
        //     $('#selectedAmount').val(selected);
        //   return selected;
        //}
        $( document ).ready( function() {
            //getAmount();
            $('.theme-btn').on('click', function (e){
                $(this).hide();
                $(this).parent().append('<img src="images/loading.gif" />');
                var selectedPaymentMethod = $( 'input[name="paymentmethod"]:checked' ).val();
                var selectedIdentifier = $( '.paymentMethodCard :checked + .paymentMethodDetails .identifier').text();
                var profileId = $( '.paymentMethodCard :checked + .paymentMethodDetails .profileId').val();
                var amount = $('#selectedAmount').val();
                console.log('pm: '+selectedPaymentMethod);
                console.log('identifier: '+selectedIdentifier);
                console.log('profileId: '+profileId);
                console.log('amount: '+amount);
                $.ajax({
                    type: 'POST',
                    url: 'post.php',
                    crossDomain: true,
                    data: { campaingId: $('#campaignId').val(), cmid :  $('#cid').val(), contactid: $('#contactid').val(), accountid: $('#accountid').val(), ppId: profileId, paymentMethod: selectedPaymentMethod, amount: amount, type: 'flexdonor2' },
                    success: function (data) {
                        var obj = jQuery.parseJSON( data );
                        console.log(obj);
                        $('#allcauses').hide();
                        $('#selectedPaymentMethod').text('using '+selectedPaymentMethod+' ('+selectedIdentifier+')');
                        $('.amount-output').text(amount);
                        $('#allcauses-thanks').show();
                    }
                });
                
                e.preventDefault();

            });
        });
    </script>
</body>
</html>