<?php include ( 'includes/header.php' ); ?>
<!--
<div class="simpleCart_shelfItem">
    <h2 class="item_name"> Awesome T-shirt </h2>
<p>    <select class="item_size">
        <option value="Small"> Small </option>
        <option value="Medium"> Medium </option>
        <option value="Large"> Large </option>
    </select><br>
    <input type="text" value="1" class="item_Quantity"><br>
    <span class="item_price">$35.99</span><br>
<a class="item_add" href="javascript:;"> Add to Cart </a></p>
</div>-->


 <!--start shop-main-content -->
    <section class="shop-main-content section-padding" id="shop">
        <div class="container">
            <div class="row">
                <div class="col col-md-12">
                    <div class="shop-content">
                        <div class="grid">
                            <div class="box simpleCart_shelfItem">
                                    <div class="img-holder">
                                        <img src="images/shop/ticket.jpg" alt class="img img-responsive">
                                    </div>
                                    <div class="details">
                                        <h3 class="item_name"><a href="javascript:;" class="item_add">Event ticket</a></h3>
                                        <a href="javascript:;" class="item_add add-to-cart"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <input type="hidden" value="1" class="item_Quantity" />
                                        <span class="item_SFID hide">01u0Y0000021SvL</span>
                                        <div class="price">
                                            <span class="current-price item_price currency">35</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="box simpleCart_shelfItem">
                                    <div class="img-holder">
                                        <img src="images/shop/puzzle.jpg" alt class="img img-responsive">
                                    </div>
                                    <div class="details">
                                        <h3 class="item_name"><a href="javascript:;" class="item_add">London 2012 Ceremony Puzzle</a></h3>
                                        <a href="javascript:;" class="item_add add-to-cart"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <input type="hidden" value="1" class="item_Quantity">
                                        <span class="item_SFID hide">01u0Y000005Xieh</span>
                                        <div class="price">
                                            <span class="current-price item_price currency">25</span>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                            <div class="grid">
                                <div class="box simpleCart_shelfItem">
                                    <div class="img-holder">
                                        <img src="images/shop/dvd.jpg" alt class="img img-responsive">
                                    </div>
                                    <div class="details">
                                        <h3 class="item_name"><a href="javascript:;" class="item_add">Education DVD</a></h3>
                                        <a href="javascript:;" class="item_add add-to-cart"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                        <input type="hidden" value="1" class="item_Quantity">
                                        <span class="item_SFID hide">01u0Y000005XgrP</span>
                                        <div class="price">
                                            <span class="current-price item_price currency">25</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end shop-content -->
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end shop-main-content -->
        <style >
        	.simpleCart_items {
        		margin-left: 50px;
        	}
	 	.itemContainer{
			width:100%;
			float:left;
		}

		.itemContainer div{
			float:left;
			margin: 5px 20px 5px 20px ;
		}

		.itemContainer a{
			text-decoration:none;
		}

		.cartHeaders{
			width:100%;
			float:left;
		}

		.cartHeaders div{
			float:left;
			margin: 5px 20px 5px 20px ;
		}


	</style>
        
        <!--<div class="simpleCart_items"></div>-->


<?php include ( 'includes/footer.php' ); ?>
    
</body>
</html>
