<?php include ( 'includes/header.php' ); ?>
<style>
.simpleCart_items th {
	padding: 5px 10px;
}
.simpleCart_items td {
	padding: 5px 10px;
}

</style>
<section class="about-us-st section-padding">
        <div class="container">
<h2>Your cart</h2>
<div class="simpleCart_items"></div>
	<br/>
<div class="checkout-btn">
	<a class="btn theme-btn simpleCart_checkout" href="javascript:;" >Proceed to checkout</a>
</div>
</div>
</section>

<?php include ( 'includes/footer.php' ); ?>
</body>
</html>