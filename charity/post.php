<?php
    include('../static.php');
    
    //$data1 = login();
    //$_SESSION['access_token'] = $data1['access_token'];
    //$_SESSION['instance_url'] = $data1['instance_url'];  
    if ( $_POST['type'] == 'donate' ) {
        $array['SuccessURL'] = $_POST['SuccessURL'];
        if($array['SuccessURL'] == '' || !$array['SuccessURL']) {
            $array['SuccessURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';    
        }
        $array['FailureURL'] = $_POST['FailureURL'];
        if($array['FailureURL'] == '' || !$array['FailureURL']) {
            $array['FailureURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';    
        }
        $contact['FirstName'] = $_POST['firstname'];
        $contact['LastName'] = $_POST['Lastname'];
        $contact['MobilePhone'] = $_POST['MobilePhone'];
        $contact['Email'] = $_POST['email'];
        $account['Name'] = $contact['LastName'] . ' Household';

        $payer['AccountRecordTypeName'] = 'HH_Account';
        $payer['AccountUpdate'] = 'Enrich';
        $payer['ContactUpdate'] = 'Enrich';
        $payer['AllowDeduplication'] = true;
        $payer['PrimaryRelation'] = 'Contact';
        $payer['Contact'] = $contact;
        $payer['Account'] = $account;
        $array['Payer'] = $payer;
        // if ($_POST['optionsRadios'] != '' ) {
        //     $array['Payment']['Amount'] = $_POST['optionsRadios'];
        // }else {
        //     $array['Payment']['Amount'] = $_POST['amount'];
        // }
        $array['Payment']['Amount'] = $_POST['selectedAmount'];
        $array['PaymentMethod']['Name'] = $_POST['paymentmethod'];
        $array['SourceConnector']['Name'] = 'PaymentHub-for-NPSP';
        
        $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/cpm/v1.0/Payment');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array));
        //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        if($_POST['giftaid'] == true) {
            $obj = json_decode($curl_response, true);
            $gaarray['gaid__Acquisition_Method__c'] = 'Digital';
            $gaarray['gaid__Type__c'] = 'Ongoing';
            $gaarray['gaid__Active__c'] = true;
            $gaarray['gaid__Contact__c'] = $obj['Payer']['ContactId'];
            $gaarray['gaid__Date_Made__c'] = date("Y-m-d");
            $gaarray['gaid__Housename_or_number__c'] = $_POST['giftaid-housenumber'];
            $gaarray['gaid__Postalcode__c'] = $_POST['giftaid-postalcode'];

            $gacurl = curl_init($_SESSION['instance_url'].'/services/data/v46.0/sobjects/gaid__Gift_Aid_Declaration__c');
            curl_setopt($gacurl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($gacurl, CURLOPT_POST, true);
            curl_setopt($gacurl, CURLOPT_POSTFIELDS, JSON_ENCODE($gaarray)) ;
            //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
            curl_setopt($gacurl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
            $gacurl_response = curl_exec($gacurl);
            if ($gacurl_response === false) {
                $info = curl_getinfo($gacurl);
                curl_close($gacurl);
                die('error occured during gacurl exec. Additioanl info: ' . var_export($info));
            }
            curl_close($gacurl);
            
        }
        echo $curl_response;
    }else if ( $_POST['type'] == 'donateGA' ) {
        $array['SuccessURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';
        $array['FailureURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';
        $contact['FirstName'] = $_POST['firstname'];
        $contact['LastName'] = $_POST['Lastname'];
        $contact['Email'] = $_POST['email'];
        $account['Name'] = $contact['LastName'] . ' Household';

        $payer['AccountRecordTypeName'] = 'HH_Account';
        $payer['AccountUpdate'] = 'Enrich';
        $payer['ContactUpdate'] = 'Enrich';
        $payer['AllowDeduplication'] = true;
        $payer['PrimaryRelation'] = 'Contact';
        $payer['Contact'] = $contact;
        $payer['Account'] = $account;
        $array['Payer'] = $payer;
        // if ($_POST['optionsRadios'] != '' ) {
        //     $array['Payment']['Amount'] = $_POST['optionsRadios'];
        // }else {
        //     $array['Payment']['Amount'] = $_POST['amount'];
        // }
        $array['Payment']['Amount'] = $_POST['selectedAmount'];
        $array['PaymentMethod']['Name'] = $_POST['paymentmethod'];
        $array['SourceConnector']['Name'] = 'PaymentHub-for-NPSP';
        
        $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/cpm/v1.0/Payment');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
        //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        if($_POST['giftaid'] == true) {
            $obj = json_decode($curl_response, true);
            $gaarray['gaid__Acquisition_Method__c'] = 'Digital';
            $gaarray['gaid__Type__c'] = 'Ongoing';
            $gaarray['gaid__Active__c'] = true;
            $gaarray['gaid__Contact__c'] = $obj['Payer']['ContactId'];
            $gaarray['gaid__Date_Made__c'] = date("YYYY-mm-dd");
            $gaarray['gaid__Housename_or_number__c'] = $_POST['giftaid-housenumber'];
            $gaarray['gaid__Postalcode__c'] = $_POST['giftaid-postalcode'];

            $gacurl = curl_init($_SESSION['instance_url'].'/services/apexrest/cpm/v1.0/Payment');
            curl_setopt($gacurl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($gacurl, CURLOPT_POST, true);
            curl_setopt($gacurl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
            //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
            curl_setopt($gacurl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
            $curl_response = curl_exec($gacurl);
            if ($curl_response === false) {
                $info = curl_getinfo($gacurl);
                curl_close($gacurl);
                die('error occured during gacurl exec. Additioanl info: ' . var_export($info));
            }
            curl_close($gacurl);
        }

        echo $curl_response;
    }else if ( $_POST['type'] == 'order' ) {
        $array['SuccessURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';
        $array['FailureURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';
        $contact['FirstName'] = $_POST['firstname'];
        $contact['LastName'] = $_POST['Lastname'];
        $contact['Email'] = $_POST['email'];
        $account['Name'] = $contact['LastName'] . ' Household';

        $payer['AccountRecordTypeName'] = 'HH_Account';
        $payer['AccountUpdate'] = 'Enrich';
        $payer['ContactUpdate'] = 'Enrich';
        $payer['AllowDeduplication'] = true;
        $payer['PrimaryRelation'] = 'Contact';
        $payer['Contact'] = $contact;
        $payer['Account'] = $account;
        $array['Payer'] = $payer;
        // if ($_POST['optionsRadios'] != '' ) {
        //     $array['Payment']['Amount'] = $_POST['optionsRadios'];
        // }else {
        //     $array['Payment']['Amount'] = $_POST['amount'];
        // }
        $array['Payment']['Amount'] = $_POST['selectedAmount'];
        $array['PaymentMethod']['Name'] = $_POST['paymentmethod'];
        $array['SourceConnector']['Name'] = 'PaymentHub-for-NPSP';
        
        $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/cpm/v1.0/Payment');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
        //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        echo $curl_response;
    }else if ( $_POST['type'] == 'registerOrder') {
        
            $order = $_POST['order'];
            $ordercurl = curl_init($_SESSION['instance_url'].'/services/apexrest/v1/Order');
            curl_setopt($ordercurl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ordercurl, CURLOPT_POST, true);
            curl_setopt($ordercurl, CURLOPT_POSTFIELDS, $order) ;
            //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
            curl_setopt($ordercurl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"));
            $ordercurl_response = curl_exec($ordercurl);
            if ($ordercurl_response === false) {
                $info = curl_getinfo($ordercurl);
                curl_close($ordercurl);
                die('error occured during curl exec. Additioanl info: ' . var_export($info));
            }
            $orderResponse = json_decode ( $ordercurl_response );
            if($orderResponse->isSuccess) {
                //nothign to do
            } else {
                die('order not correctly registered: '.$orderResponse->error);
            }
            curl_close($ordercurl);
        
        echo $ordercurl_response;
        
    }else if ( $_POST['type'] == 'flexdonor') {

        $curl1 = curl_init($_SESSION['instance_url'].'/services/apexrest/v1/FlexDonor');
        $array1['campaignMemberId'] = $_POST['cmid'];
        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl1, CURLOPT_POST, true);
        curl_setopt($curl1, CURLOPT_POSTFIELDS, JSON_ENCODE($array1)) ;
        //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl1, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
        $curl_response1 = curl_exec($curl1);
        if ($curl_response1 === false) {
            $info1 = curl_getinfo($curl1);
            curl_close($curl1);
            die('error occured during curl exec. Additioanl info: ' . var_export($info1));
        }
        curl_close($curl1);
        

        $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/cpm/v1.0/Payment');
        $array['SuccessURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';
        $array['FailureURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';
        $contact = new stdClass();
        $account = new stdClass();
        
        $payer['ContactId'] = $_POST['contactid'];
        $payer['AccountId'] = $_POST['accountid'];
        $payer['Contact'] = $contact;
        $payer['Account'] = $account;

        $array['Payer'] = $payer;
        $array['Payment']['Amount'] = '5';
        
        $array['PaymentMethod']['Name'] = 'Direct Debit';
        $array['PaymentMethod']['Processor'] = 'PaymentHub-BACS';
        $array['PaymentMethod']['Target'] = 'SmartDebit';
        $array['PaymentMethod']['PaymentProfile']['Id'] = $_POST['ppid'];
        $array['SourceConnector']['Name'] = 'PaymentHub-for-NPSP';
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
        //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        echo $curl_response;
    }else if ( $_POST['type'] == 'flexdonor2') {

        $curl1 = curl_init($_SESSION['instance_url'].'/services/apexrest/v2/FlexDonor');
        $array1['campaignMemberId'] = $_POST['cmid'];
        curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl1, CURLOPT_POST, true);
        curl_setopt($curl1, CURLOPT_POSTFIELDS, JSON_ENCODE($array1)) ;
        //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl1, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
        $curl_response1 = curl_exec($curl1);
        if ($curl_response1 === false) {
            $info1 = curl_getinfo($curl1);
            curl_close($curl1);
            die('error occured during curl exec. Additioanl info: ' . var_export($info1));
        }
        curl_close($curl1);
        

        $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/cpm/v1.0/Payment');
        $array['SuccessURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';
        $array['FailureURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';
        $contact = new stdClass();
        $account = new stdClass();
        
        $payer['ContactId'] = $_POST['contactid'];
        $payer['AccountId'] = $_POST['accountid'];
        $payer['Contact'] = $contact;
        $payer['Account'] = $account;
        
        $array['Payer'] = $payer;
        //$array['Payment']['Amount'] = '5';
        $array['Payment']['Amount'] = $_POST['amount'];
        $array['Payment']['CustomFields']['Originating_Campaign__c'] = $_POST['campaingId'];
        if($_POST['paymentMethod'] == 'Credit Card') {
            $array['PaymentMethod']['Name'] = 'Creditcard';
            $array['PaymentMethod']['Processor'] = 'PaymentHub-WorldPay';
            $array['PaymentMethod']['Target'] = '';
        } else {
            $array['PaymentMethod']['Name'] = 'Direct Debit';
            $array['PaymentMethod']['Processor'] = 'PaymentHub-BACS';
            $array['PaymentMethod']['Target'] = 'SmartDebit';
        }
        $array['PaymentMethod']['PaymentProfile']['Id'] = $_POST['ppId'];
        $array['SourceConnector']['Name'] = 'PaymentHub-for-NPSP';
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
        //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        echo $curl_response;
    }else if ( $_POST['type'] == 'pay' ) {
        $array['SuccessURL'] = $_POST['SuccessURL'];
        if($array['SuccessURL'] == '' || !$array['SuccessURL']) {
            $array['SuccessURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';    
        }
        $array['FailureURL'] = $_POST['FailureURL'];
        if($array['FailureURL'] == '' || !$array['FailureURL']) {
            $array['FailureURL'] = 'https://videoorg.herokuapp.com/charity/thank-you.php';    
        }
        $array['Payment']['InstallmentId'] = $_POST['installmentId'];
        $array['PaymentMethod']['Name'] = $_POST['paymentmethod'];

        $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/cpm/v1.0/Payment');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
        //API_TOKENs['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . API_TOKEN));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        echo $curl_response;
    }

?>