<?php
    include ('../static.php');
    //print_r($_SESSION);

    $amounts = array(25, 50, 100);

    $header = '4kvBBiGesxiyuiHuQir7Vtyclz7x5ytxzvAB9p24GK5HUF3eKwYMMQSVCfk30ADKP10MyXseXhhjFjltE81soQKWBFUeJcMSTriUJyC38g3jXrgEiOQ0UxxRLLodoAEyEzL0MhKOjzV5lSla2vJz51EvYFCSRbhWnyu3Mijv7hCXWkcCBH0j8DVwHGehYoLuPVJ56NMw';
    $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/cpm/v1.0/PaymentMethods');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, false);
    //curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
    //$headers['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . $header));
    $curl_response = curl_exec($curl);
    if ($curl_response === false) {
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additioanl info: ' . var_export($info));
    }
    curl_close($curl);
    $temp = json_decode ( $curl_response );
    $paymentMethods = $temp->PaymentMethods;

    if ( $_GET['pay'] != '' ) {
        $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/v1/ReversedInstallment/' . $_GET['pay']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, false);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, JSON_ENCODE($array)) ;
        //$headers['Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json"']
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: application/json", "api_token: " . $header));
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $temp = json_decode ( $curl_response );
    }
?>
<!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7"  lang="es" dir="ltr"><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7"  lang="es" dir="ltr"><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8"  lang="es" dir="ltr"><![endif]-->
<!--[if IE 8]><html class="lt-ie9"  lang="es" dir="ltr"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html  lang="es" dir="ltr" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# book: http://ogp.me/ns/book# profile: http://ogp.me/ns/profile# video: http://ogp.me/ns/video# product: http://ogp.me/ns/product# content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#"><!--<![endif]-->

<head>
  <meta charset="utf-8" />
<!--[if lte IE 10]><script src="/profiles/msfes/libraries/matchmedia/matchMedia.js" />
</script><![endif]--><meta name="description" content="El apoyo individual de personas como tú nos permite llevar ayuda médica a quienes más lo necesitan, sean quienes sean y estén donde estén.  Puedes realizar un donativo a través de este formulario por tarjeta, transferencia bancaria online (Trustly) o Paypal. O si lo prefieres puedes realizar una transferencia bancaria convencional a una de nuestras cuentas haciendo clic aquí. ¡Únete a Médicos Sin Fronteras!" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<link rel="image_src" href="https://www.msf.es/sites/default/files/styles/summary_image_desktop/https/media.msf.org/Docs/MSF/Media/TR1/3/d/6/5/MSB6497.jpg?itok=1oY9Vmxo&amp;timestamp=1460471545" />
<link rel="canonical" href="https://www.msf.es/colabora/doNa" />
<link rel="shortlink" href="https://www.msf.es/node/11" />
<meta property="og:site_name" content="Médicos Sin Fronteras" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.msf.es/colabora/doNa" />
<meta property="og:title" content="Colabora con nosotros" />
<meta property="og:description" content="El apoyo individual de personas como tú nos permite llevar ayuda médica a quienes más lo necesitan, sean quienes sean y estén donde estén.  Puedes realizar un donativo a través de este formulario por tarjeta, transferencia bancaria online (Trustly) o Paypal. O si lo prefieres puedes realizar una transferencia bancaria convencional a una de nuestras cuentas haciendo clic aquí. ¡Únete a Médicos Sin Fronteras!" />
<meta property="og:updated_time" content="2018-01-02T17:52:59+01:00" />
<meta property="og:image" content="https://www.msf.es/sites/default/files/styles/summary_image_desktop/https/media.msf.org/Docs/MSF/Media/TR1/3/d/6/5/MSB6497.jpg?itok=1oY9Vmxo&amp;timestamp=1460471545" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="https://www.msf.es/colabora/doNa" />
<meta name="twitter:title" content="Colabora con nosotros" />
<meta name="twitter:description" content="El apoyo individual de personas como tú nos permite llevar ayuda médica a quienes más lo necesitan, sean quienes sean y estén donde estén.  Puedes realizar un donativo a través de este formulario por tarjeta, transferencia bancaria online (Trustly) o Paypal. O si lo prefieres puedes realizar una transferencia bancaria convencional a una de nuestras cuentas haciendo clic aquí. ¡Únete a Médicos Sin Fronteras!" />
<meta name="twitter:image" content="https://www.msf.es/sites/default/files/styles/summary_image_desktop/https/media.msf.org/Docs/MSF/Media/TR1/3/d/6/5/MSB6497.jpg?itok=1oY9Vmxo&amp;timestamp=1460471545" />
<meta property="article:published_time" content="2015-09-22T16:11:40+02:00" />
<meta property="article:modified_time" content="2018-01-02T17:52:59+01:00" />
  <title>Colabora con nosotros | Médicos Sin Fronteras</title>

  <link rel="apple-touch-icon" sizes="57x57" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-57x57.png"><link rel="apple-touch-icon" sizes="60x60" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-60x60.png"><link rel="apple-touch-icon" sizes="72x72" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-72x72.png"><link rel="apple-touch-icon" sizes="76x76" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-76x76.png"><link rel="apple-touch-icon" sizes="114x114" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-114x114.png"><link rel="apple-touch-icon" sizes="120x120" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-120x120.png"><link rel="apple-touch-icon" sizes="144x144" href="/profiles/msfes/themes/custom/msfes_zen/favicon/>apple-touch-icon-144x144.png"><link rel="apple-touch-icon" sizes="152x152" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-152x152.png"><link rel="apple-touch-icon" sizes="180x180" href="/profiles/msfes/themes/custom/msfes_zen/favicon/apple-touch-icon-180x180.png"><link rel="icon" type="image/png" href="/profiles/msfes/themes/custom/msfes_zen/favicon/favicon-32x32.png" sizes="32x32"><link rel="icon" type="image/png" href="/profiles/msfes/themes/custom/msfes_zen/favicon/favicon-194x194.png" sizes="194x194"><link rel="icon" type="image/png" href="/profiles/msfes/themes/custom/msfes_zen/favicon/favicon-96x96.png" sizes="96x96"><link rel="icon" type="image/png" href="/profiles/msfes/themes/custom/msfes_zen/favicon/android-chrome-192x192.png" sizes="192x192"><link rel="icon" type="image/png" href="/profiles/msfes/themes/custom/msfes_zen/favicon/favicon-16x16.png" sizes="16x16"><link rel="manifest" href="/profiles/msfes/themes/custom/msfes_zen/favicon/manifest.json"><link rel="shortcut icon" href="/profiles/msfes/themes/custom/msfes_zen/favicon/favicon.ico"><meta name="msapplication-TileColor" content="#ffffff"><meta name="msapplication-TileImage" content="/profiles/msfes/themes/custom/msfes_zen/favicon/mstile-144x144.png"><meta name="msapplication-config" content="/profiles/msfes/themes/custom/msfes_zen/favicon/browserconfig.xml"><meta name="theme-color" content="#ffffff">
      <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, user-scalable=no">
  
  <link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_lWeEMkGaBCUg1jns3uqiF7Z1ak42hIMbTjZlfTjDQ68.css" media="all" />
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_1kcFxlSNcGp0v-loHEAsgu2rUXBn8L_MPPdtDYuz-rU.css" media="all" />
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_USqOob_wULozz2-VscbRySiDeZUxR9BRiAFMuTucczo.css" media="all" />
<style>#sliding-popup.sliding-popup-bottom{background:#ffffff;}#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text p{color:#8E8E8E !important;}
</style>
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_BvvK_KNm42iC6LDPtoh9O47Yf8iRq3GvK8n4N09yieo.css" media="all" />
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_I_DTWsfymF0V1dVUXqFXS_IwJq09115G-Rwl5d56Da4.css" media="all" />

<!--[if lte IE 8]>
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_nSdmrrLNUnzK-9BsernIuIssNyrEybzC3ijIy_b9LQs.css" media="all" />
<![endif]-->

<!--[if lte IE 9]>
<link rel="stylesheet" href="https://www.msf.es/sites/default/files/css/css_Rs6Z2DzsFt8vNgrbz__Mp2azj_dwTHbeUBKhEgj2iSY.css" media="all" />
<![endif]-->
  <script src="https://www.msf.es/sites/default/files/js/js_urCmzDS98vJbRW2n7PNOexmM8xf5cAhVBbHnM2cToXk.js"></script>
<script>var dataLayer = [];function msfesGetDevice(){
          var md = new MobileDetect(window.navigator.userAgent);
          var device = 'desktop';
          if (md.tablet()) {
            device = 'tablet';
          }
          else if (md.mobile()) {
            device = 'mobile';
          }
          return device;
        }; dataLayer = [{
                'categoria':'formularios',
                'subcategoria':'espontaneos-donante-form-general-menu-desplegable',
                'dispositivo': msfesGetDevice()
              }]; window.addEventListener('load', function msfesVirtualPage() {
          var personalDataFormItems = document.querySelectorAll('.group-personal-data .form-item');
          var paymentFormItems = document.querySelectorAll('.group-payment .form-item');

          function msfesAddClick(elements, step) {
            for (var i = 0; i < elements.length; i++) {
              elements[i].addEventListener('click', step);
            }
          }

          function msfesRemoveClick(elements, step) {
            for (var i = 0; i < elements.length; i++) {
              elements[i].removeEventListener('click', step);
            }
          }

          var virtualPageStep1 = function() {
            dataLayer.push({
              'event':'virtual pageview',
              'pageName': '/colabora/doNa/step1'
            });

            msfesRemoveClick(personalDataFormItems, virtualPageStep1);
          }

          var virtualPageStep2 = function() {
            dataLayer.push({
              'event':'virtual pageview',
              'pageName': '/colabora/doNa/step2'
            });

            msfesRemoveClick(paymentFormItems, virtualPageStep2);
          }

          msfesAddClick(personalDataFormItems, virtualPageStep1);
          msfesAddClick(paymentFormItems, virtualPageStep2);

        }); window.addEventListener('load', function msfesCommunicationsPermission() {
          var acceptCommunications = document.querySelectorAll('.form-item-field_form_newsletter input[value=\'1\']');
          var denyCommunications = document.querySelectorAll('.form-item-field_form_newsletter input[value=\'0\']');

          for (var i = 0; i < acceptCommunications.length; i++) {
            acceptCommunications[i].addEventListener('click', function () {
              dataLayer.push({
                'eventCat':'autorización',
                'eventAct':'Si,quiero',
                'eventLbl':'formularios',
                'event':'eventoGA'
              });
            });
          }

          for (var i = 0; i < denyCommunications.length; i++) {
            denyCommunications[i].addEventListener('click', function () {
              dataLayer.push({
                'eventCat':'autorización',
                'eventAct':'No,gracias',
                'eventLbl':'formularios',
                'event':'eventoGA'
              });
            });
          }

        });window.addEventListener('load', function msfesPartner() {
           var becomePartnerButtons = document.querySelectorAll('.become-partner__link');

           for (var i = 0; i < becomePartnerButtons.length; i++) {
             becomePartnerButtons[i].addEventListener('click', function () {
               partnerDatalayer();
             });
           }

           var partnerDatalayer = function() {
             dataLayer.push({
               'eventCat':'clic_hazte_socio',
               'eventAct':'header',
               'eventLbl':'home',
               'event':'eventoGA'
             });
           };
         });window.addEventListener('load', function msfesSocial() {
          var socialFollowItems = document.querySelectorAll('#social-slider .on-the-web');
          var socialShareItems = document.querySelectorAll('#social-share .social-share__item');

          for (var i = 0; i < socialFollowItems.length; i++) {
            socialFollowItems[i].addEventListener('click', function () {
              var socialNetwork = '';
              if (this.classList.contains('otw-twitter')) {
                socialNetwork = 'twitter';
              }
              else if (this.classList.contains('otw-facebook')) {
                socialNetwork = 'facebook';
              }
              else if (this.classList.contains('otw-instagram')) {
                socialNetwork = 'instagram';
              }
              else if (this.classList.contains('otw-youtube')) {
                socialNetwork = 'youtube';
              }
              else if (this.classList.contains('otw-google')) {
                socialNetwork = 'google';
              }
              else if (this.classList.contains('otw-flickr')) {
                socialNetwork = 'flickr';
              }
              else if (this.classList.contains('otw-tumblr')) {
                socialNetwork = 'tumblr';
              }
              else if (this.classList.contains('otw-vimeo')) {
                socialNetwork = 'vimeo';
              }
              socialDatalayer(socialNetwork, 'follow');
            });
          }

          for (var i = 0; i < socialShareItems.length; i++) {
            socialShareItems[i].addEventListener('click', function () {
              var socialNetwork = '';
              if (this.classList.contains('twitter')) {
                socialNetwork = 'twitter';
              }
              else if (this.classList.contains('facebook')) {
                socialNetwork = 'facebook';
              }
              else if (this.classList.contains('instagram')) {
                socialNetwork = 'instagram';
              }
              else if (this.classList.contains('youtube')) {
                socialNetwork = 'youtube';
              }
              else if (this.classList.contains('google')) {
                socialNetwork = 'google';
              }
              else if (this.classList.contains('flickr')) {
                socialNetwork = 'flickr';
              }
              else if (this.classList.contains('tumblr')) {
                socialNetwork = 'tumblr';
              }
              else if (this.classList.contains('vimeo')) {
                socialNetwork = 'vimeo';
              }
              else if (this.classList.contains('email')) {
                socialNetwork = 'email';
              }
              socialDatalayer(socialNetwork, 'share');
            });
          }

          var socialDatalayer = function(social, action) {
            dataLayer.push({
              'socialNetwork':social,
              'socialAction':action,
              'socialTarget':'https://www.msf.es/colabora/doNa',
              'event':'eventoSocial'
            });
          };
        }); window.addEventListener('load', function msfesNewsletter() {
           var newsletterSubmitButtons = document.querySelectorAll('.entitytype-newsletter_signup-form .form-submit');

           for (var i = 0; i < newsletterSubmitButtons.length; i++) {
             newsletterSubmitButtons[i].addEventListener('click', function () {
               newsletterDatalayer();
             });
           }

           var newsletterDatalayer = function() {
             dataLayer.push({
               'eventCat':'lead',
               'eventAct':'suscripcion_newsletter',
               'eventLbl':'home',
               'event':'eventoGA'
             });
           };
         });</script>
<script src="https://www.msf.es/sites/default/files/js/js_cSOowGuq2BGYf0Sc296Qel0yaNgMkLvbT4tyy1SrNjw.js"></script>
<script src="https://www.msf.es/sites/default/files/js/js_Jozr3f52u0F-UfC_5Nk-_EZyMvGH4TCdwp10mh0GVK8.js"></script>
<script src="https://www.msf.es/sites/default/files/js/js_zHRvVzLloGF3tLamoGwDYPOSch4DKjuEI83L_3dIgbs.js"></script>
<script src="https://www.msf.es/sites/default/files/js/js_-dKvtC3gB-oTJiQxsHb1CKIUnXhcE_6E7imtuWkqLJk.js"></script>
<script>document.createElement( "picture" );</script>
<script src="https://www.msf.es/sites/default/files/js/js_WbXG5tQLEZsZDq0Kk1zSBSucNnLJT3jKNfAsR2NqHbU.js"></script>
<script src="//maps.googleapis.com/maps/api/js?v=3"></script>
<script src="https://www.msf.es/sites/default/files/js/js_Qsvbqo50NTuBiCY8t9PrlCazvFLN2VObayqBat1NywU.js"></script>
<script>jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"msfes_zen","theme_token":"kA76O9hyI3j6SK2H0INjBUyYvznh5ui-YChcDYwf_YI","js":{"profiles\/msfes\/modules\/contrib\/picture\/picturefill2\/picturefill.min.js":1,"profiles\/msfes\/modules\/contrib\/picture\/picture.min.js":1,"profiles\/msfes\/modules\/contrib\/clientside_validation\/clientside_validation_field_validation\/plugins\/validator\/js\/regexmatch.cv.js":1,"profiles\/msfes\/modules\/contrib\/clientside_validation\/plugins\/validator\/js\/blacklist.cv.js":1,"profiles\/msfes\/modules\/contrib\/clientside_validation\/clientside_validation_field_validation\/clientside_validation_field_validation.js":1,"profiles\/msfes\/modules\/contrib\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"profiles\/msfes\/modules\/contrib\/clientside_validation\/js\/clientside_validation.ie8.js":1,"profiles\/msfes\/modules\/contrib\/clientside_validation\/js\/clientside_validation.js":1,"profiles\/msfes\/libraries\/mobile_detect_js\/mobile-detect.min.js":1,"0":1,"public:\/\/google_tag\/google_tag.script.js":1,"profiles\/msfes\/modules\/contrib\/jquery_update\/replace\/jquery\/1.10\/jquery.min.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"profiles\/msfes\/libraries\/fitvids\/jquery.fitvids.js":1,"profiles\/msfes\/modules\/contrib\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.core.min.js":1,"profiles\/msfes\/modules\/contrib\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.widget.min.js":1,"profiles\/msfes\/libraries\/chosen\/chosen.jquery.min.js":1,"profiles\/msfes\/libraries\/matchheight\/jquery.matchHeight-min.js":1,"profiles\/msfes\/modules\/contrib\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.position.min.js":1,"profiles\/msfes\/modules\/contrib\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.menu.min.js":1,"profiles\/msfes\/modules\/contrib\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.autocomplete.min.js":1,"profiles\/msfes\/libraries\/mmenu\/src\/js\/jquery.mmenu.min.js":1,"profiles\/msfes\/libraries\/scrolltofixed\/jquery-scrolltofixed.js":1,"misc\/states.js":1,"profiles\/msfes\/modules\/contrib\/fitvids\/fitvids.js":1,"profiles\/msfes\/libraries\/enquire\/dist\/enquire.min.js":1,"profiles\/msfes\/modules\/contrib\/breakpointsjs\/js\/breakpoints.js":1,"1":1,"public:\/\/languages\/es_UUCcnqA5s3YO9bYHtGZoDcOsNpWtP_Dk14S_8qZhEjA.js":1,"profiles\/msfes\/libraries\/colorbox\/jquery.colorbox-min.js":1,"profiles\/msfes\/modules\/contrib\/colorbox\/js\/colorbox.js":1,"profiles\/msfes\/modules\/contrib\/colorbox\/styles\/plain\/colorbox_style.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_config_form\/js\/change_title.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_common\/js\/tooltip.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_common\/js\/payment_image.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_common\/js\/radio_to_click.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_common\/js\/uncheck_tax_checkbox.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_common\/js\/ft_form_donation.common.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_common\/js\/postal_code.js":1,"profiles\/msfes\/libraries\/jquery-form-submit-single\/src\/jquery.form-submit-single.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_common\/js\/msf.form-submit-single.js":1,"profiles\/msfes\/libraries\/jquery.validate\/dist\/jquery.validate.js":1,"profiles\/msfes\/libraries\/jquery.validate\/dist\/additional-methods.js":1,"profiles\/msfes\/libraries\/jquery.validate\/dist\/localization\/messages_es.js":1,"profiles\/msfes\/modules\/features\/content\/ft_event\/js\/image_event.js":1,"profiles\/msfes\/modules\/features\/content\/ft_component_slide\/js\/content_slider.js":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/start_matchheight.js":1,"misc\/textarea.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_common\/js\/clientside_validation.common.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_newsletter_signup\/js\/header-newsletter.js":1,"profiles\/msfes\/libraries\/flexslider\/jquery.flexslider-min.js":1,"profiles\/msfes\/modules\/contrib\/field_group\/field_group.js":1,"profiles\/msfes\/modules\/features\/general\/ft_msfes_footer\/js\/site_selector.js":1,"profiles\/msfes\/modules\/features\/forms\/ft_form_contact\/js\/footer_quick_contact.js":1,"profiles\/msfes\/modules\/features\/general\/ft_social\/js\/social_share.js":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/footer_map.js":1,"profiles\/msfes\/modules\/contrib\/chosen\/chosen.js":1,"\/\/maps.googleapis.com\/maps\/api\/js?v=3":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/rwd_menu.js":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/search_block.js":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/modernizr.touch.js":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/js\/glossary.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"profiles\/msfes\/libraries\/chosen\/chosen.css":1,"profiles\/msfes\/modules\/contrib\/chosen\/css\/chosen-drupal.css":1,"misc\/ui\/jquery.ui.core.css":1,"misc\/ui\/jquery.ui.theme.css":1,"misc\/ui\/jquery.ui.menu.css":1,"misc\/ui\/jquery.ui.autocomplete.css":1,"profiles\/msfes\/libraries\/mmenu\/src\/css\/jquery.mmenu.css":1,"modules\/comment\/comment.css":1,"profiles\/msfes\/modules\/contrib\/date\/date_api\/date.css":1,"profiles\/msfes\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"profiles\/msfes\/modules\/contrib\/fitvids\/fitvids.css":1,"modules\/node\/node.css":1,"profiles\/msfes\/modules\/contrib\/picture\/picture_wysiwyg.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"profiles\/msfes\/modules\/platform\/custom\/media_dam\/css\/media-dam.base.css":1,"profiles\/msfes\/modules\/platform\/custom\/media_dam\/css\/media-dam.admin.css":1,"profiles\/msfes\/modules\/contrib\/views\/css\/views.css":1,"profiles\/msfes\/modules\/contrib\/colorbox\/styles\/plain\/colorbox_style.css":1,"profiles\/msfes\/modules\/contrib\/ctools\/css\/ctools.css":1,"profiles\/msfes\/modules\/contrib\/panels\/css\/panels.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/layouts\/2colmsfgroupedtop\/..\/..\/css\/layouts\/2colmsf.css":1,"profiles\/msfes\/modules\/contrib\/field_collection\/field_collection.theme.css":1,"0":1,"profiles\/msfes\/modules\/contrib\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"sites\/default\/files\/honeypot\/honeypot.css":1,"profiles\/msfes\/modules\/contrib\/flexslider\/assets\/css\/flexslider_img.css":1,"profiles\/msfes\/libraries\/flexslider\/flexslider.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/system.base.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/system.menus.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/system.messages.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/system.theme.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/comment.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/node.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/jquery.mmenu.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/field_collection.theme.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/panels.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/css\/styles.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/css\/form.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/css\/flexslider.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/css\/lte_ie8.css":1,"profiles\/msfes\/themes\/custom\/msfes_zen\/css\/lte_ie9.css":1}},"colorbox":{"opacity":"0.85","current":"{current} de {total}","previous":"\u00ab Anterior","next":"Siguiente \u00bb","close":"Cerrar","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"clientsideValidation":{"forms":{"donation-entityform-edit-form":{"rules":{"field_form_contribution[und][select]":{"required":true},"field_form_contribution[und][other]":{"maxlength":255},"field_form_contribution[und]":{"maxlength":255},"field_form_periodicity[und]":{"required":true},"field_form_person_company[und]":{"required":true},"field_form_company_name[und][0][value]":{"required":true,"maxlength":255},"field_form_name[und][0][value]":{"required":true,"maxlength":255},"field_form_first_surname[und][0][value]":{"required":true,"maxlength":255},"field_form_second_surname[und][0][value]":{"maxlength":255},"field_form_mail[und][0][email]":{"required":true,"maxlength":50},"field_form_cif[und][0][nif]":{"required":true,"maxlength":128},"field_form_id_type[und]":{"blacklist":["","_none"]},"field_form_id_number[und][0][value]":{"required":true,"maxlength":255},"field_form_telephone[und][0][value]":{"maxlength":9,"minlength":9,"regexMatch":"^[6789][0-9]*$","number":true},"field_form_road_type[und]":{"blacklist":["","_none"]},"field_form_address[und][0][value]":{"required":true,"maxlength":255},"field_form_address_num[und][0][value]":{"maxlength":255},"field_form_other_info[und][0][value]":{"maxlength":255},"field_form_postal_code[und][0][value]":{"integer":true,"required":true,"maxlength":5,"minlength":5},"field_form_city[und][0][value]":{"required":true,"maxlength":255},"field_form_province[und]":{"blacklist":["","_none"]},"field_form_country[und]":{"blacklist":["","_none"]},"field_form_payment_method[und]":{"required":true},"field_form_iban[und][0][value]":{"required":true,"maxlength":4,"minlength":4},"field_form_bank_entity[und][0][value]":{"required":true,"maxlength":4,"minlength":4,"number":true},"field_form_bank_office[und][0][value]":{"required":true,"maxlength":4,"minlength":4,"number":true},"field_form_bank_dc[und][0][value]":{"required":true,"maxlength":2,"minlength":2,"number":true},"field_form_bank_account[und][0][value]":{"required":true,"maxlength":10,"minlength":10,"number":true},"field_form_account_holder[und][0][value]":{"required":true,"maxlength":255},"field_form_newsletter[und]":{"required":true},"terms":{"required":true},"main_comments":{"maxlength":128}},"messages":{"field_form_contribution[und][select]":{"required":"Campo obligatorio"},"field_form_contribution[und][other]":{"maxlength":"Otra cantidad field has a maximum length of 255."},"field_form_contribution[und]":{"maxlength":"Contribuci\u00f3n field has a maximum length of 255."},"field_form_periodicity[und]":{"required":"Campo obligatorio"},"field_form_person_company[und]":{"required":"Campo obligatorio"},"field_form_company_name[und][0][value]":{"required":"Campo obligatorio","maxlength":"Nombre de la empresa field has a maximum length of 255."},"field_form_name[und][0][value]":{"required":"Campo obligatorio","maxlength":"Nombre field has a maximum length of 255."},"field_form_first_surname[und][0][value]":{"required":"Campo obligatorio","maxlength":"Primer apellido field has a maximum length of 255."},"field_form_second_surname[und][0][value]":{"maxlength":"Segundo apellido field has a maximum length of 255."},"field_form_mail[und][0][email]":{"required":"Campo obligatorio","maxlength":"E-mail field has a maximum length of 50."},"field_form_cif[und][0][nif]":{"required":"Campo obligatorio","maxlength":"CIF field has a maximum length of 128."},"field_form_id_type[und]":{"blacklist":"Campo obligatorio"},"field_form_id_number[und][0][value]":{"required":"Campo obligatorio","maxlength":"N\u00famero de Documento field has a maximum length of 255."},"field_form_telephone[und][0][value]":{"maxlength":"El [field-name] debe contener 9 d\u00edgitos.","minlength":"El [field-name] debe contener 9 d\u00edgitos.","regexMatch":"El tel\u00e9fono debe empezar por 6, 7, 8 o 9 y ser num\u00e9rico","number":"El campo [field-name] debe ser num\u00e9rico."},"field_form_road_type[und]":{"blacklist":"Campo obligatorio"},"field_form_address[und][0][value]":{"required":"Campo obligatorio","maxlength":"Direcci\u00f3n field has a maximum length of 255."},"field_form_address_num[und][0][value]":{"maxlength":"N\u00famero field has a maximum length of 255."},"field_form_other_info[und][0][value]":{"maxlength":"Otros datos field has a maximum length of 255."},"field_form_postal_code[und][0][value]":{"integer":"El campo C\u00f3digo postal s\u00f3lo acepta n\u00fameros.","required":"Campo obligatorio","maxlength":"El campo [field-name] debe tener 5 d\u00edgitos. [value] tiene [length] d\u00edgitos.","minlength":"El campo [field-name] debe tener 5 d\u00edgitos. [value] tiene [length] d\u00edgitos."},"field_form_city[und][0][value]":{"required":"Campo obligatorio","maxlength":"Poblaci\u00f3n field has a maximum length of 255."},"field_form_province[und]":{"blacklist":"Campo obligatorio"},"field_form_country[und]":{"blacklist":"Campo obligatorio"},"field_form_payment_method[und]":{"required":"Campo obligatorio"},"field_form_iban[und][0][value]":{"required":"Campo obligatorio","maxlength":"El campo [field-name] debe contener exactamente 4 caracteres, \u0022[value]\u0022 tiene [length] caracteres.","minlength":"El campo [field-name] debe contener exactamente 4 caracteres, \u0022[value]\u0022 tiene [length] caracteres."},"field_form_bank_entity[und][0][value]":{"required":"Campo obligatorio","maxlength":"El campo [field-name] debe contener exactamente 4 d\u00edgitos, \u0022[value]\u0022 tiene [length] d\u00edgitos.","minlength":"El campo [field-name] debe contener exactamente 4 d\u00edgitos, \u0022[value]\u0022 tiene [length] d\u00edgitos.","number":"El campo [field-name] debe ser num\u00e9rico."},"field_form_bank_office[und][0][value]":{"required":"Campo obligatorio","maxlength":"El campo [field-name] debe contener exactamente 4 d\u00edgitos, \u0022[value]\u0022 tiene [length] d\u00edgitos.","minlength":"El campo [field-name] debe contener exactamente 4 d\u00edgitos, \u0022[value]\u0022 tiene [length] d\u00edgitos.","number":"El campo [field-name] debe ser num\u00e9rico."},"field_form_bank_dc[und][0][value]":{"required":"Campo obligatorio","maxlength":"El campo [field-name] debe contener exactamente 2 d\u00edgitos, \u0022[value]\u0022 tiene [length] d\u00edgitos.","minlength":"El campo [field-name] debe contener exactamente 2 d\u00edgitos, \u0022[value]\u0022 tiene [length] d\u00edgitos.","number":"El campo [field-name] debe ser num\u00e9rico."},"field_form_bank_account[und][0][value]":{"required":"Campo obligatorio","maxlength":"El campo [field-name] debe contener exactamente 10 d\u00edgitos, \u0022[value]\u0022 tiene [length] d\u00edgitos.","minlength":"El campo [field-name] debe contener exactamente 10 d\u00edgitos, \u0022[value]\u0022 tiene [length] d\u00edgitos.","number":"El campo [field-name] debe ser num\u00e9rico."},"field_form_account_holder[und][0][value]":{"required":"Campo obligatorio","maxlength":"Titular field has a maximum length of 255."},"field_form_newsletter[und]":{"required":"Campo obligatorio"},"terms":{"required":"Campo obligatorio"},"main_comments":{"maxlength":"Leave this field blank field has a maximum length of 128."}},"errorPlacement":"5","general":{"errorClass":"error","wrapper":"li","validateTabs":0,"scrollTo":1,"scrollSpeed":"1000","disableHtml5Validation":"1","validateOnBlur":"1","validateOnBlurAlways":"0","validateOnKeyUp":"1","validateBeforeAjax":"1","validateOnSubmit":"1","showMessages":"0","errorElement":"label"}},"contact-entityform-edit-form":{"rules":{"field_form_name[und][0][value]":{"maxlength":255},"field_form_surnames[und][0][value]":{"maxlength":255},"field_form_mail[und][0][email]":{"required":true,"maxlength":50},"field_form_topic[und][0][value]":{"maxlength":255},"main_comments":{"maxlength":128}},"messages":{"field_form_name[und][0][value]":{"maxlength":"Nombre field has a maximum length of 255."},"field_form_surnames[und][0][value]":{"maxlength":"Apellidos field has a maximum length of 255."},"field_form_mail[und][0][email]":{"required":"Campo obligatorio","maxlength":"E-mail field has a maximum length of 50."},"field_form_topic[und][0][value]":{"maxlength":"Asunto field has a maximum length of 255."},"main_comments":{"maxlength":"Leave this field blank field has a maximum length of 128."}},"errorPlacement":"5","general":{"errorClass":"error","wrapper":"li","validateTabs":0,"scrollTo":1,"scrollSpeed":"1000","disableHtml5Validation":"1","validateOnBlur":"1","validateOnBlurAlways":"0","validateOnKeyUp":"1","validateBeforeAjax":"1","validateOnSubmit":"1","showMessages":"0","errorElement":"label"}},"newsletter-signup-entityform-edit-form":{"rules":{"field_form_mail[und][0][email]":{"required":true,"maxlength":50},"main_comments":{"maxlength":128}},"messages":{"field_form_mail[und][0][email]":{"required":"Campo obligatorio","maxlength":"E-mail field has a maximum length of 50."},"main_comments":{"maxlength":"Leave this field blank field has a maximum length of 128."}},"errorPlacement":"5","general":{"errorClass":"error","wrapper":"li","validateTabs":0,"scrollTo":1,"scrollSpeed":"1000","disableHtml5Validation":"1","validateOnBlur":"1","validateOnBlurAlways":"0","validateOnKeyUp":"1","validateBeforeAjax":"1","validateOnSubmit":"1","showMessages":"0","errorElement":"label"}}},"general":{"usexregxp":0,"months":{"Enero":1,"Ene":1,"Febrero":2,"Feb":2,"Marzo":3,"Mar":3,"Abril":4,"Abr":4,"Mayo":5,"Junio":6,"Jun":6,"Julio":7,"Jul":7,"Agosto":8,"Ago":8,"Septiembre":9,"Sep":9,"Octubre":10,"Oct":10,"Noviembre":11,"Nov":11,"Diciembre":12,"Dic":12}},"groups":{"donation-entityform-edit-form":{},"contact-entityform-edit-form":{},"newsletter-signup-entityform-edit-form":{}}},"states":{"#edit-field-form-periodicity-und":{"visible":{":input[name^=\u0022field_form_periodic_contribution\u0022]":{"checked":true}}},"#edit-field-form-company-name-und-0-value":{"visible":{":input[name^=\u0022field_form_person_company\u0022]":{"value":"company"}}},"#edit-field-form-company-name-und-0":{"visible":{":input[name^=\u0022field_form_person_company\u0022]":{"value":"company"}}},"#edit-field-form-cif-und-0-nif":{"visible":{":input[name^=\u0022field_form_person_company\u0022]":{"value":"company"}}},"#edit-field-form-cif-und-0-number":{"visible":{":input[name^=\u0022field_form_person_company\u0022]":{"value":"company"}}},"#edit-field-form-cif-und-0-first-letter":{"visible":{":input[name^=\u0022field_form_person_company\u0022]":{"value":"company"}}},"#edit-field-form-cif-und-0-last-letter":{"visible":{":input[name^=\u0022field_form_person_company\u0022]":{"value":"company"}}},"#edit-field-form-cif-und-0-type":{"visible":{":input[name^=\u0022field_form_person_company\u0022]":{"value":"company"}}},"#edit-field-form-cif-und-0":{"visible":{":input[name^=\u0022field_form_person_company\u0022]":{"value":"company"}}},"#edit-field-form-id-type-und":{"visible":{":input[name^=\u0027field_form_person_company\u0027]":{"value":"person"}}},"#edit-field-form-id-type":{"visible":{":input[name^=\u0027field_form_person_company\u0027]":{"value":"person"}}},"#edit-field-form-id-number-und-0-value":{"visible":{":input[name^=\u0027field_form_person_company\u0027]":{"value":"person"}}},"#edit-field-form-id-number-und-0":{"visible":{":input[name^=\u0027field_form_person_company\u0027]":{"value":"person"}}},"#edit-field-form-id-number-und":{"visible":{":input[name^=\u0027field_form_person_company\u0027]":{"value":"person"}}},"#edit-field-form-id-number":{"visible":{":input[name^=\u0027field_form_person_company\u0027]":{"value":"person"}}},"#group_nif":{"visible":{":input[name^=\u0027field_form_person_company\u0027]":{"value":"person"}}},"#edit-field-form-tax-deductible-und":{"visible":{":input[name^=\u0022field_form_id_type\u0022]":{"value":"1"}}},"#edit-field-form-road-type-und":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-road-type":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-address-und-0-value":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-address-und-0":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-address-und":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-address":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-address-num-und-0-value":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-address-num-und-0":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-address-num-und":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-address-num":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-other-info-und-0-value":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-other-info-und-0":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-other-info-und":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-other-info":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-postal-code-und-0-value":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-postal-code-und-0":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-postal-code-und":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-postal-code":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-city-und-0-value":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-city-und-0":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-city-und":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-city":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-province-und":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-province":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-country-und":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-country":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#group_address":{"visible":{":input[name^=\u0027field_form_tax_deductible\u0027]":{"checked":true}}},"#edit-field-form-payment-method-und":{"visible":{":input[name^=\u0022field_form_periodic_contribution\u0022]":{"checked":false}}},"#edit-field-form-iban-und-0-value":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-iban-und-0":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-iban-und":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-iban":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-entity-und-0-value":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-entity-und-0":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-entity-und":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-entity":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-office-und-0-value":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-office-und-0":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-office-und":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-office":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-dc-und-0-value":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-dc-und-0":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-dc-und":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-dc":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-account-und-0-value":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-account-und-0":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-account-und":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-bank-account":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#group_bank_account":{"visible":{":input[name^=\u0027field_form_periodic_contribution\u0027]":{"checked":true}}},"#edit-field-form-account-holder-und-0-value":{"visible":{":input[name^=\u0022field_form_periodic_contribution\u0022]":{"checked":true}}},"#edit-field-form-account-holder-und-0":{"visible":{":input[name^=\u0022field_form_periodic_contribution\u0022]":{"checked":true}}}},"ckeditorGridTemplates":{"images":"\/profiles\/msfes\/modules\/custom\/ckeditor_grid_templates\/images\/","title_two":"Plantilla de dos columnas","desc_two":"Estructura personalizada de dos columnas para insertar contenido en dos columnas diferentes.","title_three":"Plantilla de tres columnas","desc_three":"Estructura personalizada de tres columnas para insertar contenido en tres columnas diferentes.","col1_example":"Ejemplo de contenido de la primera columna","col2_example":"Ejemplo de contenido de la segunda columna","col3_example":"Ejemplo de contenido de la tercera columna"},"chosen":{"selector":".page-admin select:visible","minimum_single":20,"minimum_multiple":20,"minimum_width":0,"options":{"allow_single_deselect":false,"disable_search":false,"disable_search_threshold":0,"search_contains":false,"placeholder_text_multiple":"Elija algunas opciones","placeholder_text_single":"Elige una opci\u00f3n","no_results_text":"No hay resultados coincidentes","inherit_select_classes":true}},"ftConfigForm":{"changeTitle":{"group_personal_data_change_title":{"label":"h3","title":"Los datos de tu empresa","field":"field_form_person_company","value":"company"}}},"urlIsAjaxTrusted":{"\/colabora\/doNa":true,"\/buscador":true,"\/actualidad\/makone-mare-rescatado-mediterraneo":true},"field_group":{"fieldset":"default","div":"default"},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":0,"popup_html_info":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003EEsta web utiliza \u0027cookies\u0027 propias y de terceros para ofrecerte una mejor experiencia y servicio. Al navegar o utilizar nuestros servicios, aceptas el uso que hacemos de las \u0027cookies\u0027. Sin embargo, puedes cambiar la configuraci\u00f3n de \u0027cookies\u0027 en cualquier momento.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button\u0022\u003EAcepto\u003C\/button\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button\u0022\u003EM\u00e1s informaci\u00f3n\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class =\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003EThank you for accepting cookies\u003C\/h2\u003E\n\u003Cp\u003EYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button\u0022\u003EHide\u003C\/button\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button\u0022 \u003EMore info\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"\/politica-privacidad","popup_link_new_window":1,"popup_position":null,"popup_language":"es","domain":"","cookie_lifetime":"100"},"fitvids":{"custom_domains":[],"selectors":["body"],"simplifymarkup":true},"breakpoints":{"module.picture.empty_srcset":{"disabled":false,"api_version":1,"machine_name":"module.picture.empty_srcset","name":"Empty srcset","breakpoint":"","source":"picture","source_type":"module","status":1,"weight":0,"multipliers":{"1x":"1x"},"export_module":"picture","type":"Predeterminado","export_type":2,"in_code_only":true,"table":"breakpoints"},"breakpoints.theme.msfes_zen.mobile_menu":{"id":"222541","machine_name":"breakpoints.theme.msfes_zen.mobile_menu","name":"mobile_menu","breakpoint":"(min-width: 0px) and (max-width: 58.5em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"0","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.mobile_tablet_s":{"id":"222551","machine_name":"breakpoints.theme.msfes_zen.mobile_tablet_s","name":"mobile_tablet_s","breakpoint":"(min-width: 0px) and (max-device-width: 48em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"1","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.mobile":{"id":"222561","machine_name":"breakpoints.theme.msfes_zen.mobile","name":"mobile","breakpoint":"(min-width: 0px) and (max-width: 29.99em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"2","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.tablet_s":{"id":"222571","machine_name":"breakpoints.theme.msfes_zen.tablet_s","name":"tablet_s","breakpoint":"(min-width: 30em) and (max-device-width: 48em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"3","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.tablet":{"id":"222581","machine_name":"breakpoints.theme.msfes_zen.tablet","name":"tablet","breakpoint":"(min-device-width: 48.06em) and (max-width: 61.19em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"4","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.tablet_touch":{"id":"222591","machine_name":"breakpoints.theme.msfes_zen.tablet_touch","name":"tablet_touch","breakpoint":"(min-device-width: 58.563em) and (orientation landscape)","source":"msfes_zen","source_type":"theme","status":"1","weight":"5","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.desktop":{"id":"222601","machine_name":"breakpoints.theme.msfes_zen.desktop","name":"desktop","breakpoint":"(min-width: 61.2em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"6","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.desktop_m":{"id":"222611","machine_name":"breakpoints.theme.msfes_zen.desktop_m","name":"desktop_m","breakpoint":"(min-width: 61.2em) and (max-width: 70.5em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"7","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1},"breakpoints.theme.msfes_zen.desktop_l":{"id":"222621","machine_name":"breakpoints.theme.msfes_zen.desktop_l","name":"desktop_l","breakpoint":"(min-width: 70.5em)","source":"msfes_zen","source_type":"theme","status":"1","weight":"8","multipliers":{"1x":"1x"},"table":"breakpoints","type":"Normal","export_type":1}},"basepath":"https:\/\/www.msf.es\/profiles\/msfes\/themes\/custom\/msfes_zen"});</script>
      <!--[if lt IE 9]>
    <script src="/profiles/msfes/themes/contrib/zen/js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-11 node-type-form-page i18n-es section-colabora page-panels" >
      <p class="skip-link__wrapper">
      <a href="#main-menu" class="skip-link visually-hidden--focusable" id="skip-link">Jump to navigation</a>
    </p>
      <div class="region region-page-top">
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHSW2Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>  </div>
  
<div class="page">
    <div class="region region-top">
    <div class="block block-ft-profile first odd first odd" id="block-ft-profile-profile-link">

      
  <a href="/msflogin" class="profile-login menu__link" id="profile-login">Iniciar sesión</a>
</div>
<div class="block block-ft-form-newsletter-signup even even newsletter-header" id="block-ft-form-newsletter-signup-newsletter-header">

      
  <form class="entityform entitytype-newsletter_signup-form" action="/actualidad/makone-mare-rescatado-mediterraneo" method="post" id="newsletter-signup-entityform-edit-form" accept-charset="UTF-8"><div><h2 class="newsletter-header__title"><a href="/colabora/alta-newsletter">Suscríbete a nuestra newsletter</a></h2><div class="required-fields form-group group-newsletter last clearfix"><div class="form-item form-type-emailfield form-item-field_form_mail third-weight">
  <label for="edit-field-form-mail-und-0-email"> <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input placeholder="Escribe tu correo electrónico" type="email" id="edit-field-form-mail-und-0-email" name="field_form_mail[und][0][email]" value="" size="60" maxlength="50" class="form-text form-email required" />
</div>
</div><input type="hidden" name="form_build_id" value="form-_A-ptFD_FXiEc21BKgNr_9QZVG67I1qAzCCEKGV1cLI" />
<input type="hidden" name="form_id" value="newsletter_signup_entityform_edit_form" />
<div class="main_comments-textfield"><div class="form-item form-type-textfield">
  <label for="edit-main-comments">Leave this field blank </label>
 <input autocomplete="off" type="text" id="edit-main-comments" name="main_comments" value="" size="20" maxlength="128" class="form-text" />
</div>
</div><div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Enviar" class="form-submit" /></div></div></form>
</div>
<div class="block block-on-the-web last odd last odd" id="block-on-the-web-0">

      
  <div class="slide" id="social-slider"><span class="on-the-web otw-facebook"><a href="https://www.facebook.com/medicossinfronteras.ong" title="Find Médicos Sin Fronteras on Facebook" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/facebook.png" alt="Find Médicos Sin Fronteras on Facebook" title="Find Médicos Sin Fronteras on Facebook" /><span class="service-name">facebook</span></a></span><span class="on-the-web otw-twitter"><a href="https://twitter.com/msf_espana" title="Find Médicos Sin Fronteras on Twitter" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/twitter.png" alt="Find Médicos Sin Fronteras on Twitter" title="Find Médicos Sin Fronteras on Twitter" /><span class="service-name">twitter</span></a></span><span class="on-the-web otw-instagram"><a href="https://www.instagram.com/msf_en_espanol/" title="Find Médicos Sin Fronteras on Instagram" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/instagram.png" alt="Find Médicos Sin Fronteras on Instagram" title="Find Médicos Sin Fronteras on Instagram" /><span class="service-name">instagram</span></a></span><span class="on-the-web otw-youtube"><a href="https://www.youtube.com/user/MedicosSinFronteras" title="Find Médicos Sin Fronteras on YouTube" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/youtube.png" alt="Find Médicos Sin Fronteras on YouTube" title="Find Médicos Sin Fronteras on YouTube" /><span class="service-name">youtube</span></a></span><span class="on-the-web otw-google"><a href="https://plus.google.com/110981584708327608177/posts" title="Find Médicos Sin Fronteras on Google+" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/google.png" alt="Find Médicos Sin Fronteras on Google+" title="Find Médicos Sin Fronteras on Google+" /><span class="service-name">google</span></a></span><span class="on-the-web otw-tumblr"><a href="http://medicossinfronteras.tumblr.com" title="Find Médicos Sin Fronteras on Tumblr" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/tumblr.png" alt="Find Médicos Sin Fronteras on Tumblr" title="Find Médicos Sin Fronteras on Tumblr" /><span class="service-name">tumblr</span></a></span><span class="on-the-web otw-vimeo"><a href="https://vimeo.com/msf" title="Find Médicos Sin Fronteras on Vimeo" rel="nofollow" target="_blank"><img typeof="foaf:Image" src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/vimeo.png" alt="Find Médicos Sin Fronteras on Vimeo" title="Find Médicos Sin Fronteras on Vimeo" /><span class="service-name">vimeo</span></a></span></div>
</div>
  </div>

    <header class="header" id="header" role="banner">

          <a href="/" title="Inicio" rel="home" class="header__logo">
        <img src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/logo.png" alt="Inicio" class="header__logo-image" />
      </a>
    
    
    
       <nav id="navigation" class="region region-navigation">
    <div class="block block-ft-msfes-header become-partner first odd first odd" id="block-ft-msfes-header-msf-become-partner">

      
  <a href="/colabora/hazte-socio" class="become-partner__link">Hazte socio</a>
</div>
<div class="block block-views even even search-block__wrapper" role="search" id="block-views-exp-content-search-results">

      
  <form class="search-block search-block--closed" action="/buscador" method="get" id="views-exposed-form-content-search-results" accept-charset="UTF-8"><div><div class="views-exposed-form">
  <div class="views-exposed-widgets clearfix">
          <div id="edit-text-wrapper" class="views-exposed-widget views-widget-filter-search_api_views_fulltext">
                        <div class="views-widget">
          <div class="form-item form-type-searchfield">
 <input class="search-block__input form-text form-search" type="search" id="edit-text" name="text" value="" size="30" maxlength="128" />
</div>
        </div>
              </div>
                    <div class="views-exposed-widget views-submit-button">
      <input type="submit" id="edit-submit-content-search" name="" value="Buscar" class="form-submit" />    </div>
      </div>
</div>
</div></form>
</div>
<div class="block block-menu-block last odd last odd" role="navigation" id="block-menu-block-msfes-main-menu">

      
  <div class="menu-block-wrapper menu-block-msfes-main-menu menu-name-main-menu parent-mlid-0 menu-level-1">
  <ul class="menu"><li class="menu__item is-expanded first expanded menu-mlid-45171"><a href="/conocenos" title="" class="menu__link">Conócenos</a><ul class="menu"><li class="menu__item is-leaf is-parent first leaf has-children menu-mlid-62171"><a href="/conocenos/quienes-somos" class="menu__link">Quiénes somos</a></li>
<li class="menu__item is-leaf leaf menu-mlid-104821"><a href="/conocenos/cuando-intervenimos" title="" class="menu__link">Cuándo intervenimos</a></li>
<li class="menu__item is-leaf leaf menu-mlid-104751"><a href="/conocenos/que-hacemos" title="" class="menu__link">Qué hacemos</a></li>
<li class="menu__item is-leaf is-parent last leaf has-children menu-mlid-62191"><a href="/conocenos/como-nos-financiamos" class="menu__link">Cómo nos financiamos</a></li>
</ul></li>
<li class="menu__item is-expanded expanded menu-mlid-68071"><a href="/actualidad" title="" class="menu__link">Actualidad</a><ul class="menu"><li class="menu__item is-leaf first leaf menu-mlid-45231"><a href="/actualidad/agenda" class="menu__link">Agenda</a></li>
<li class="menu__item is-leaf leaf menu-mlid-56971"><a href="/actualidad/desde-el-terreno" title="" class="menu__link">Desde el terreno</a></li>
<li class="menu__item is-leaf leaf menu-mlid-56981"><a href="/actualidad/publicaciones" title="" class="menu__link">Publicaciones</a></li>
<li class="menu__item is-leaf last leaf menu-mlid-107681"><a href="/actualidad/noticias" title="" class="menu__link">Noticias</a></li>
</ul></li>
<li class="menu__item is-expanded is-active-trail expanded active-trail menu-mlid-75071"><a href="/COLABORa" class="menu__link is-active-trail active-trail">Colabora</a><ul class="menu"><li class="menu__item is-leaf first leaf menu-mlid-107641"><a href="/colabora/hazte-socio" title="Tu ayuda nos permite llegar a más personas" class="menu__link">Hazte socio</a></li>
<li class="menu__item is-leaf is-active-trail is-active leaf active-trail active menu-mlid-107651"><a href="/colabora/doNa" title="Haz llegar acción médica a quien más lo necesita" class="menu__link is-active-trail active-trail active">Dona</a></li>
<li class="menu__item is-leaf is-parent leaf has-children menu-mlid-92501"><a href="/colabora/empresas" title="Colabora como empresa" class="menu__link">Empresas</a></li>
<li class="menu__item is-leaf leaf menu-mlid-92561"><a href="/colabora/grandes-donativos" title="Un gran donativo tiene un gran impacto." class="menu__link">Grandes Donativos</a></li>
<li class="menu__item is-leaf leaf menu-mlid-107661"><a href="/colabora/herencias-legados" title="Alarga tu compromiso solidario" class="menu__link">Herencias y legados</a></li>
<li class="menu__item is-leaf last leaf menu-mlid-114081"><a href="/colabora/bodas-solidarias" title="¡Sí, quiero!" class="menu__link">BODAS SOLIDARIAS</a></li>
</ul></li>
<li class="menu__item is-expanded last expanded menu-mlid-68181"><a href="/trabaja" title="" class="menu__link">Trabaja</a><ul class="menu"><li class="menu__item is-leaf first leaf menu-mlid-62271"><a href="/trabaja/terreno" class="menu__link">En el terreno</a></li>
<li class="menu__item is-leaf leaf menu-mlid-62281"><a href="/trabaja/oficinas" title="" class="menu__link">En las oficinas</a></li>
<li class="menu__item is-leaf leaf menu-mlid-62291"><a href="/trabaja/captacion-calle" title="" class="menu__link">Captación en calle</a></li>
<li class="menu__item is-leaf leaf menu-mlid-90041"><a href="/trabaja/preguntas-mas-frecuentes" class="menu__link">Preguntas frecuentes</a></li>
<li class="menu__item is-leaf last leaf menu-mlid-111171"><a href="/trabaja/ofertas" title="" class="menu__link">Ofertas de trabajo</a></li>
</ul></li>
</ul></div>

</div>
  </nav>
  <a id="search-block--mobile__toogle" class="search-block--mobile__toogle" href="#">
      Open search form  </a>

  </header>
  
  <div class="main">

    <div class="main-content" role="main">
                  <a href="#skip-link" class="visually-hidden--focusable" id="main-content">Back to top</a>
                              


<div class="panel-display col2 clearfix" >
  <div class="panel-col-first">
    <div class="panel-panel panel-col-first__top inside">
      <h1 class="page__title">Colabora con nosotros</h1>
<div class="panel-pane pane-entity-field pane-node-field-summary-image"  >
  
      
  
  <div class="field-collection-container clearfix"><div class="field-collection-view clearfix view-mode-summary-image-small field-collection-view-final"><div class="summary-image__image"><picture >
<!--[if IE 9]><video style="display: none;"><![endif]-->
<source srcset="https://www.msf.es/sites/default/files/styles/summary_image_mobile/https/media.msf.org/Docs/MSF/Media/TR1/3/d/6/5/MSB6497.jpg?itok=DeFlm-b6&amp;timestamp=1460471545 1x" media="(min-width: 0px) and (max-width: 29.99em)" />
<source srcset="https://www.msf.es/sites/default/files/styles/summary_image_small_tabletsmall/https/media.msf.org/Docs/MSF/Media/TR1/3/d/6/5/MSB6497.jpg?itok=X0-w6-TW&amp;timestamp=1460471545 1x" media="(min-width: 30em) and (max-device-width: 48em)" />
<source srcset="https://www.msf.es/sites/default/files/styles/summary_image_small_tablet/https/media.msf.org/Docs/MSF/Media/TR1/3/d/6/5/MSB6497.jpg?itok=NcvI-EIz&amp;timestamp=1460471545 1x" media="(min-device-width: 48.06em) and (max-width: 61.19em)" />
<source srcset="https://www.msf.es/sites/default/files/styles/summary_image_small_desktop/https/media.msf.org/Docs/MSF/Media/TR1/3/d/6/5/MSB6497.jpg?itok=e1VPMZG_&amp;timestamp=1460471545 1x" media="(min-width: 61.2em)" />
<!--[if IE 9]></video><![endif]-->
<img  src="https://www.msf.es/sites/default/files/styles/summary_image_small_desktop/https/media.msf.org/Docs/MSF/Media/TR1/3/d/6/5/MSB6497.jpg?itok=e1VPMZG_&amp;timestamp=1460471545" alt="" title="" />
</picture></div><div class="summary-image__copyright"><span class="image__copyright">Samantha Maurin /MSF</span></div></div></div>
  
  </div>
<div class="field-body" ><p>El apoyo individual de personas como tú nos permite llevar ayuda médica a quienes más lo necesitan, sean quienes sean y estén donde estén. </p>
<p>Puedes realizar un donativo a través de este formulario por tarjeta, transferencia bancaria online (Trustly) o Paypal. O si lo prefieres puedes realizar una transferencia bancaria convencional a una de nuestras cuentas haciendo <a href="https://www.msf.es/colabora/cuentas-bancarias/dona">clic aquí</a>. ¡Únete a Médicos Sin Fronteras!</p>
</div>    </div>
    <div class="panel-panel panel-col-first__bottom">
      <form class="entityform entitytype-donation-form" action="" method="post" id="donation-entityform-edit-form" accept-charset="UTF-8"><div><div class="required-fields form-group group-contribution clearfix"><h3><span>¿Cuánto quieres aportar?</span></h3><div class="form-item form-type-select-or-other form-item-field_form_contribution">
<?
    if ($_GET['pay'] != '' ) {
        echo '<input type="hidden" name="type" value="pay" />';
        echo '<input type="hidden" name="installmentId" value="'.$_GET['pay'].'" />';
        echo '<input type="submit" class="btn theme-btn" id="donate" value="Pay" />';
    }else {
        echo '<input type="hidden" name="type" value="donate" />';
        //echo '<input type="submit" class="btn theme-btn" id="donate" value="Donate" />';
    }

?>
<input type="hidden" id="selectedAmount" name="selectedAmount" value=""/>
<input type="hidden" id="SuccessURL" name="SuccessURL" value="https://paymenthubdemo.herokuapp.com/charity/thank-you-msf.php"/>

 <div class="select-or-other">
<div class="form-item form-type-radios">
  <label for="edit-field-form-contribution-und-select">Contribución <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <div id="edit-field-form-contribution-und-select" class="form-radios select-or-other-select"><div class="form-item form-type-radio">
 <input class="select-or-other-select form-radio" type="radio" id="edit-field-form-contribution-und-select-30" name="field_form_contribution" value="30" checked="checked" />  <label class="option" for="edit-field-form-contribution-und-select-30"><span class="contribution__amount">30 € </span><span class="contribution__tangibility">podemos comprar suero de rehidratación y sets de perfusión intravenosa para 6 personas.</span> </label>

</div>
<div class="form-item form-type-radio">
 <input class="select-or-other-select form-radio" type="radio" id="edit-field-form-contribution-und-select-60" name="field_form_contribution" value="60" />  <label class="option" for="edit-field-form-contribution-und-select-60"><span class="contribution__amount">60 € </span><span class="contribution__tangibility">podemos dar 200 raciones de alimento terapéutico preparado para tratar a un niño con desnutrición aguda severa.</span> </label>

</div>
<div class="form-item form-type-radio">
 <input class="select-or-other-select form-radio" type="radio" id="edit-field-form-contribution-und-select-90" name="field_form_contribution" value="90" />  <label class="option" for="edit-field-form-contribution-und-select-90"><span class="contribution__amount">90 € </span><span class="contribution__tangibility">podemos tratar con antirretrovirales durante un año a un niño con VIH.</span> </label>

</div>
<div class="form-item form-type-radio">
 <input class="select-or-other-select form-radio" type="radio" id="edit-field-form-contribution-und-select-select-or-other" name="field_form_contribution[und][select]" value="select_or_other" />  <label class="option" for="edit-field-form-contribution-und-select-select-or-other">Otra cantidad </label>

</div>
</div>
</div>
<div class="form-item form-type-textfield">
  <label class="element-invisible" for="edit-field-form-contribution-und-other">Otra cantidad </label>
 <input class="select-or-other-other form-text" type="text" id="edit-field-form-contribution-und-other" name="field_form_contribution[und][other]" value="" size="60" maxlength="255" />
</div>

</div>

</div>
<div class="form-item form-type-checkbox form-item-field_form_periodic_contribution">
 <input type="checkbox" id="edit-field-form-periodic-contribution-und" name="field_form_periodic_contribution[und]" value="1" class="form-checkbox" />  <label class="option" for="edit-field-form-periodic-contribution-und">Hacer que mi aportación sea periódica. </label>

</div>
<div class="form-item form-type-radios form-item-field_form_periodicity">
  <label for="edit-field-form-periodicity-und">Periodicidad <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <div id="edit-field-form-periodicity-und" class="form-radios"><div class="form-item form-type-radio">
 <input type="radio" id="edit-field-form-periodicity-und-01" name="field_form_periodicity[und]" value="01" checked="checked" class="form-radio" />  <label class="option" for="edit-field-form-periodicity-und-01">Mensual </label>

</div>
<div class="form-item form-type-radio">
 <input type="radio" id="edit-field-form-periodicity-und-02" name="field_form_periodicity[und]" value="02" class="form-radio" />  <label class="option" for="edit-field-form-periodicity-und-02">Trimestral </label>

</div>
<div class="form-item form-type-radio">
 <input type="radio" id="edit-field-form-periodicity-und-04" name="field_form_periodicity[und]" value="04" class="form-radio" />  <label class="option" for="edit-field-form-periodicity-und-04">Anual </label>

</div>
</div>
</div>
</div><input type="hidden" name="form_build_id" value="form-1gvPQWwBw9tjaKvmymapfIMZpktDAZJqe2m6FTk-XDw" />
<input type="hidden" name="form_id" value="donation_entityform_edit_form" />
<div class="required-fields form-group group-data clearfix"><div class="required-fields group-personal-data field-group-div subgroup-personal-data group_personal_data_change_title"><h3><span>Ahora, tus datos personales</span></h3><div class="form-item form-type-radios form-item-field_form_person_company">
  <label for="edit-field-form-person-company-und">Individual o empresa <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <div id="edit-field-form-person-company-und" class="form-radios"><div class="form-item form-type-radio">
 <input type="radio" id="edit-field-form-person-company-und-person" name="field_form_person_company[und]" value="person" checked="checked" class="form-radio" />  <label class="option" for="edit-field-form-person-company-und-person">Particular </label>

</div>
<div class="form-item form-type-radio">
 <input type="radio" id="edit-field-form-person-company-und-company" name="field_form_person_company[und]" value="company" class="form-radio" />  <label class="option" for="edit-field-form-person-company-und-company">Empresa </label>

</div>
</div>
</div>
<div class="form-item form-type-textfield form-item-field_form_company_name">
  <label for="edit-field-form-company-name-und-0-value">Nombre de la empresa <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="edit-field-form-company-name-und-0-value" name="field_form_company_name[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textfield form-item-field_form_name third-weight">
  <label for="edit-field-form-name-und-0-value">Nombre <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="edit-field-form-name-und-0-value" name="firstname" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textfield form-item-field_form_first_surname third-weight">
  <label for="edit-field-form-first-surname-und-0-value">Primer apellido <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="Lastname" name="Lastname" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textfield form-item-field_form_second_surname third-weight">
  <label for="edit-field-form-second-surname-und-0-value">Segundo apellido </label>
 <input class="text-full form-text" type="text" id="edit-field-form-second-surname-und-0-value" name="field_form_second_surname[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-emailfield form-item-field_form_mail third-weight">
  <label for="edit-field-form-mail-und-0-email">E-mail <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input type="email" id="edit-field-form-mail-und-0-email" name="email" value="" size="60" maxlength="50" class="form-text form-email required" />
</div>
<div class="form-item form-type-textfield">
  <label for="edit-field-form-cif-und-0-nif">CIF <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input type="text" id="edit-field-form-cif-und-0-nif" name="field_form_cif[und][0][nif]" value="" size="10" maxlength="128" class="form-text required" />
</div>
<input type="hidden" name="field_form_cif[und][0][number]" value="" />
<input type="hidden" name="field_form_cif[und][0][first_letter]" value="" />
<input type="hidden" name="field_form_cif[und][0][last_letter]" value="" />
<input type="hidden" name="field_form_cif[und][0][type]" value="" />
<fieldset class="required-fields group-nif subgroup-nif  third-weight form-wrapper" id="group_nif"><legend><span class="fieldset-legend">Documento</span></legend><div class="fieldset-wrapper"><div class="form-item form-type-select form-item-field_form_id_type">
  <label for="edit-field-form-id-type-und">Tipo ID <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <select id="edit-field-form-id-type-und" name="field_form_id_type[und]" class="form-select required"><option value="1" selected="selected">NIF/NIE</option><option value="2">Pasaporte</option><option value="3">Otros</option></select>
</div>
<div class="form-item form-type-textfield form-item-field_form_id_number">
  <label for="edit-field-form-id-number-und-0-value">Número de Documento <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="edit-field-form-id-number-und-0-value" name="field_form_id_number[und][0][value]" value="" size="60" maxlength="255" />
</div>
</div></fieldset>
<div class="form-item form-type-telfield form-item-field_form_telephone third-weight">
  <label for="edit-field-form-telephone-und-0-value">Teléfono </label>
 <input type="tel" id="edit-field-form-telephone-und-0-value" name="MobilePhone" value="" size="20" maxlength="64" class="form-text form-tel" />
</div>
</div><div class="form-item form-type-checkbox form-item-field_form_tax_deductible">
 <input type="checkbox" id="edit-field-form-tax-deductible-und" name="field_form_tax_deductible[und]" value="1" class="form-checkbox" /> <span class="field-suffix"><span class="tooltip-info"><p>Puedes deducir tus donativos en tu próxima declaración de la renta o impuesto de sociedades. Para ello, necesitamos tu dirección y tu NIF o CIF. Puedes enviar estos datos ahora o más adelante mediante el área "Mi perfil".</p></span><span class="tooltip-trigger">?</span></span>  <label class="option" for="edit-field-form-tax-deductible-und">Quiero desgravar mi aportación en la declaración de la renta. </label>

</div>
<fieldset class="required-fields group-address field-group-fieldset subgroup-address form-wrapper" id="group_address"><legend><span class="fieldset-legend">Entonces, necesitamos tu dirección</span></legend><div class="fieldset-wrapper"><div class="form-item form-type-select form-item-field_form_road_type fourth-weight">
  <label for="edit-field-form-road-type-und">Tipo de vía <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <select id="edit-field-form-road-type-und" name="field_form_road_type[und]" class="form-select required"><option value="_none">- Ninguno -</option><option value="AG">AGRUPACION</option><option value="AL">ALAMEDA</option><option value="AP">APARTAMENTOS</option><option value="AV">AVENIDA</option><option value="BD">BAJADA</option><option value="BC">BARRANCO</option><option value="BO">BARRIO</option><option value="BL">BLOQUE</option><option value="CL">CALLE</option><option value="CJ">CALLEJÓN</option><option value="CM">CAMINO</option><option value="CR">CARRETERA</option><option value="CS">CASAS</option><option value="CH">CHALET</option><option value="CO">COLONIA</option><option value="CT">CUESTA</option><option value="DS">DISEMINADO</option><option value="ED">EDIFICIO</option><option value="GL">GLORIETA</option><option value="GR">GRUPO</option><option value="LG">LUGAR</option><option value="MC">MERCADO</option><option value="PQ">PARQUE</option><option value="PD">PARTIDA</option><option value="PJ">PASAJE</option><option value="PS">PASEO</option><option value="PZ">PLAZA</option><option value="PL">PLAZUELA</option><option value="PB">POBLADO</option><option value="PG">POLIGONO</option><option value="PR">PROLONGACIÓN</option><option value="RB">RAMBLA</option><option value="RD">RONDA</option><option value="SN">SENDA</option><option value="SD">SUBIDA</option><option value="TT">TORRENTE</option><option value="TR">TRAVESÍA</option><option value="UR">URBANIZACIÓN</option></select>
</div>
<div class="form-item form-type-textfield form-item-field_form_address">
  <label for="edit-field-form-address-und-0-value">Dirección <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="edit-field-form-address-und-0-value" name="field_form_address[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textfield form-item-field_form_address_num">
  <label for="edit-field-form-address-num-und-0-value">Número </label>
 <input class="text-full form-text" type="text" id="edit-field-form-address-num-und-0-value" name="field_form_address_num[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textfield form-item-field_form_other_info">
  <label for="edit-field-form-other-info-und-0-value">Otros datos </label>
 <input class="text-full form-text" type="text" id="edit-field-form-other-info-und-0-value" name="field_form_other_info[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textfield form-item-field_form_postal_code fourth-weight">
  <label for="edit-field-form-postal-code-und-0-value">Código postal <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input type="text" id="edit-field-form-postal-code-und-0-value" name="field_form_postal_code[und][0][value]" value="" size="12" maxlength="10" class="form-text required" />
</div>
<div class="form-item form-type-textfield form-item-field_form_city fourth-weight">
  <label for="edit-field-form-city-und-0-value">Población <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="edit-field-form-city-und-0-value" name="field_form_city[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-select form-item-field_form_province fourth-weight">
  <label for="edit-field-form-province-und">Provincia <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <select id="edit-field-form-province-und" name="field_form_province[und]" class="form-select required"><option value="_none">- Ninguno -</option><option value="15">A CORUÑA</option><option value="02">ALBACETE</option><option value="03">ALICANTE/ALACANT</option><option value="04">ALMERÍA</option><option value="01">ARABA/ÁLAVA</option><option value="33">ASTURIAS</option><option value="05">ÁVILA</option><option value="06">BADAJOZ</option><option value="08">BARCELONA</option><option value="48">BIZKAIA</option><option value="09">BURGOS</option><option value="10">CÁCERES</option><option value="11">CÁDIZ</option><option value="39">CANTABRIA</option><option value="12">CASTELLÓN/CASTELLÓ</option><option value="51">CEUTA</option><option value="13">CIUDAD REAL</option><option value="14">CÓRDOBA</option><option value="16">CUENCA</option><option value="20">GIPUZKOA</option><option value="17">GIRONA</option><option value="18">GRANADA</option><option value="19">GUADALAJARA</option><option value="21">HUELVA</option><option value="22">HUESCA</option><option value="07">ILLES BALEARS</option><option value="23">JAÉN</option><option value="26">LA RIOJA</option><option value="35">LAS PALMAS</option><option value="24">LEÓN</option><option value="25">LLEIDA</option><option value="27">LUGO</option><option value="28">MADRID</option><option value="29">MÁLAGA</option><option value="52">MELILLA</option><option value="30">MURCIA</option><option value="31">NAVARRA</option><option value="32">OURENSE</option><option value="34">PALENCIA</option><option value="36">PONTEVEDRA</option><option value="37">SALAMANCA</option><option value="38">SANTA CRUZ DE TENERIFE</option><option value="40">SEGOVIA</option><option value="41">SEVILLA</option><option value="42">SORIA</option><option value="43">TARRAGONA</option><option value="44">TERUEL</option><option value="45">TOLEDO</option><option value="46">VALENCIA/VALÈNCIA</option><option value="47">VALLADOLID</option><option value="49">ZAMORA</option><option value="50">ZARAGOZA</option></select>
</div>
<div class="form-item form-type-select form-item-field_form_country fourth-weight">
  <label for="edit-field-form-country-und">País <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <select id="edit-field-form-country-und" name="field_form_country[und]" class="form-select required"><option value="_none">- Ninguno -</option><option value="101">AFGANISTAN</option><option value="102">ALBANIA</option><option value="104">ALEMANIA</option><option value="106">ANDORRA</option><option value="107">ANGOLA</option><option value="264">ANTIGUA Y BARBUDA</option><option value="108">ANTILLAS NEERLANDESAS</option><option value="109">ARABIA SAUDITA</option><option value="110">ARGELIA</option><option value="111">ARGENTINA</option><option value="265">ARMENIA</option><option value="112">AUSTRALIA</option><option value="113">AUSTRIA</option><option value="266">AZERBAIYAN</option><option value="114">BAHAMAS</option><option value="115">BAHREIN</option><option value="267">BANGLA DESH</option><option value="116">BARBADOS</option><option value="117">BELGICA</option><option value="118">BELICE</option><option value="119">BENIN</option><option value="127">BHUTAN</option><option value="268">BIELORUSSIA</option><option value="120">BIRMANIA</option><option value="121">BOLIVIA</option><option value="269">BOSNIA Y HERZEGOVINA</option><option value="122">BOTSWANA</option><option value="123">BRASIL</option><option value="124">BRUNEI</option><option value="125">BULGARIA</option><option value="105">BURKINA FASO</option><option value="126">BURUNDI</option><option value="270">CABO VERDE</option><option value="128">CAMBOYA</option><option value="129">CAMERUN</option><option value="130">CANADA</option><option value="255">CEI</option><option value="140">CHAD</option><option value="141">CHECOSLOVAQUIA</option><option value="142">CHILE</option><option value="144">CHINA</option><option value="145">CHIPRE</option><option value="133">COLOMBIA</option><option value="135">COREA</option><option value="136">COREA (REP.DEM.POP.)</option><option value="137">COSTA D&#039;IVOIRE</option><option value="138">COSTA RICA</option><option value="272">CROACIA</option><option value="139">CUBA</option><option value="146">DINAMARCA</option><option value="273">DJIBOUTI</option><option value="147">DOMINICA</option><option value="148">ECUADOR</option><option value="149">EGIPTO</option><option value="150">EL SALVADOR</option><option value="299">EMIRATOS ARABES UNIDOS</option><option value="274">ESLOVAQUIA</option><option value="275">ESLOVENIA</option><option value="100" selected="selected">ESPAÑA</option><option value="151">ESTADOS UNIDOS DE AMERICA</option><option value="276">ESTONIA</option><option value="152">ETIOPIA</option><option value="153">FIJI</option><option value="154">FILIPINAS</option><option value="155">FINLANDIA</option><option value="156">FRANCIA</option><option value="157">GABON</option><option value="158">GAMBIA</option><option value="277">GEORGIA</option><option value="159">GHANA</option><option value="160">GIBRALTAR</option><option value="163">GRANADA</option><option value="162">GRECIA</option><option value="164">GUATEMALA</option><option value="166">GUAYANA</option><option value="167">GUAYANA FRANCESA</option><option value="165">GUAYANA HOLANDESA (SURINAM)</option><option value="170">GUINEA</option><option value="168">GUINEA BISSAU</option><option value="169">GUINEA EQUATORIAL</option><option value="171">HAITI</option><option value="173">HONDURAS</option><option value="174">HONG KONG</option><option value="175">HUNGRIA</option><option value="176">INDIA</option><option value="177">INDONESIA</option><option value="179">IRAN</option><option value="178">IRAQ</option><option value="180">IRLANDA</option><option value="181">ISLANDIA</option><option value="271">ISLAS COMORES</option><option value="284">ISLAS MALDIVES</option><option value="182">ISRAEL</option><option value="183">ITALIA</option><option value="184">JAMAICA</option><option value="185">JAPON</option><option value="186">JORDANIA</option><option value="278">KAZAJSTAN</option><option value="187">KENYA</option><option value="279">KIRGUISTAN</option><option value="280">KIRIBATI</option><option value="188">KUWAIT</option><option value="189">LAOS</option><option value="190">LESOTHO</option><option value="281">LETONIA</option><option value="191">LIBANO</option><option value="192">LIBERIA</option><option value="193">LIBIA</option><option value="194">LIECHTENSTEIN</option><option value="282">LITUANIA</option><option value="195">LUXEMBURGO</option><option value="196">MACAU</option><option value="283">MACEDONIA</option><option value="225">MADAGASCAR</option><option value="197">MALASIA</option><option value="198">MALAWI</option><option value="199">MALI</option><option value="200">MALTA</option><option value="201">MARRUECOS</option><option value="202">MAURICIO</option><option value="203">MAURITANIA</option><option value="204">MEXICO</option><option value="285">MOLDAVIA</option><option value="205">MONACO</option><option value="206">MONGOLIA</option><option value="207">MOZANBIQUE</option><option value="208">NAMIBIA</option><option value="286">NAURU</option><option value="209">NEPAL</option><option value="210">NICARAGUA</option><option value="211">NIGER</option><option value="212">NIGERIA</option><option value="213">NORUEGA</option><option value="214">NUEVA ZELANDA</option><option value="287">OMAN</option><option value="172">PAISES BAJOS</option><option value="215">PAKISTAN</option><option value="216">PANAMA</option><option value="288">PAPUA-NUEVA GUINEA</option><option value="217">PARAGUAY</option><option value="218">PERU</option><option value="219">POLONIA</option><option value="220">PORTUGAL</option><option value="221">PUERTO RICO</option><option value="222">QATAR</option><option value="161">REINO UNIDO</option><option value="223">REP. CENTRAFRICANA</option><option value="289">REP. CHECA</option><option value="224">REP. DOMINICANA</option><option value="134">REPUBLICA DEMOCRATICA DEL CONGO</option><option value="228">ROMANIA</option><option value="290">RUSIA</option><option value="227">RWANDA</option><option value="229">SAHARA OCCIDENTAL</option><option value="291">SAINT CHRISTOPHER Y NEVIS</option><option value="292">SALOMON</option><option value="230">SAMOA OCCIDENTAL</option><option value="231">SAN MARINO</option><option value="232">SANT VINCENTE Y LAS GRENADINAS</option><option value="233">SANTA LUCIA</option><option value="293">SANTO TOME Y PRINCIPE</option><option value="234">SARAWAK</option><option value="235">SENEGAL</option><option value="236">SEYCHELLES</option><option value="237">SIERRA LEONA</option><option value="238">SINGAPUR</option><option value="239">SIRIA</option><option value="240">SOMALIA</option><option value="131">SRI LANKA</option><option value="253">SUDAFRICA</option><option value="241">SUDAN</option><option value="242">SUECIA</option><option value="243">SUIZA</option><option value="244">SWAZILANDIA</option><option value="245">TAILANDIA</option><option value="143">TAIWAN</option><option value="246">TANZANIA</option><option value="294">TAYIKISTAN</option><option value="248">TOGO</option><option value="295">TONGA</option><option value="249">TRINIDAD Y TOBAGO</option><option value="250">TUNEZ</option><option value="296">TURKMENISTAN</option><option value="251">TURQUIA</option><option value="297">TUVALU</option><option value="298">UCRAINA</option><option value="252">UGANDA</option><option value="254">URUGUAY</option><option value="300">UZBEKISTAN</option><option value="301">VANUATU</option><option value="132">VATICANO</option><option value="256">VENEZUELA</option><option value="257">VIETNAM</option><option value="260">YEMEN NORTE</option><option value="259">YEMEN SUR</option><option value="261">YUGOSLAVIA</option><option value="262">ZAIRE</option><option value="263">ZAMBIA</option><option value="226">ZIMBABWE</option></select>
</div>
</div></fieldset>
</div><div class="required-fields form-group group-payment last clearfix"><h3><span>Método de pago</span></h3><div class="form-item form-type-radios form-item-field_form_payment_method">
  <label for="edit-field-form-payment-method-und">Método de pago <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 
<style>
@media (max-width: 799px) {
  .paymentoptionlabel {
    padding-top: 10px !important;
  }
}
.paymentoptionlabel {
    padding-left: 50px !important;
    background-position: 6px 22px;
    font-size: 1.2rem;
    line-height: 2.42857rem;

}
@media (min-width: 800px) {
  .paymentoptionlabel {
    padding-top: 18px !important;
  }
  .paymentoptionlabel.Card {
      background-image: url(https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/sass/components/form-item/card.png);
  }

  .paymentoptionlabel.PayPal {
      background-image: url(https://www.msf.es//profiles/msfes/themes/custom/msfes_zen/sass/components/form-item/paypal.png);
  }
}

</style>
<input type="hidden" id="paymentmethod" name="paymentmethod" value="Direct Debit"/>
<?php
    $checked = 'checked';
    foreach ( $paymentMethods as $key=>$value ) {
      $methodclass = 'Card';
      if($value->Name == 'PayPal') {
        $methodclass = 'PayPal';
      } 
        ?>
        <div class="form-item form-type-radio paymentmethodsselection <?php echo $checked; ?>" >
         <input type="radio" id="edit-field-form-payment-method-und-<?php echo $value->Name; ?>" name="paymentmethods" value="<?php echo $value->Name; ?>" class="form-radio form-item-field_form_card_payment" <?php echo $checked; ?> />  <label class="option paymentoptionlabel <?php echo $methodclass; ?>" for="edit-field-form-payment-method-und-<?php echo $value->Name; ?>"><?php echo $value->Name; ?></label>

        </div>
<?php
$checked = "";
}
?>
<!--<div class="form-item form-type-radio">
 <input type="radio" id="edit-field-form-payment-method-und-paypal" name="field_form_payment_method[und]" value="paypal" class="form-radio" />  <label class="option" for="edit-field-form-payment-method-und-paypal">PayPal </label>

</div>
-->
</div>
</div>
<fieldset class="required-fields group-bank-account subgroup-bank subgroup-bank--fieldset form-wrapper" id="group_bank_account"><legend><span class="fieldset-legend">Cuenta bancaria</span></legend><div class="fieldset-wrapper"><div class="form-item form-type-textfield form-item-field_form_iban">
  <label for="edit-field-form-iban-und-0-value">IBAN <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" placeholder="ES" type="text" id="edit-field-form-iban-und-0-value" name="field_form_iban[und][0][value]" value="" size="60" maxlength="255" /> <span class="field-suffix"><span class="tooltip-info"><p>Desde febrero de 2014, necesitamos el código IBAN de tu cuenta bancaria para realizar cualquier operación. Es un código de cuatro caracteres que debes añadir antes de tu número de cuenta. Si todavía no lo conoces, puedes hacerte socio con tu cuenta bancaria habitual.</p></span><span class="tooltip-trigger">?</span></span>
</div>
<div class="form-item form-type-textfield form-item-field_form_bank_entity">
  <label for="edit-field-form-bank-entity-und-0-value">Entidad <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="edit-field-form-bank-entity-und-0-value" name="field_form_bank_entity[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textfield form-item-field_form_bank_office">
  <label for="edit-field-form-bank-office-und-0-value">Oficina <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="edit-field-form-bank-office-und-0-value" name="field_form_bank_office[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textfield form-item-field_form_bank_dc">
  <label for="edit-field-form-bank-dc-und-0-value">DC <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="edit-field-form-bank-dc-und-0-value" name="field_form_bank_dc[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textfield form-item-field_form_bank_account">
  <label for="edit-field-form-bank-account-und-0-value">Cuenta <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="edit-field-form-bank-account-und-0-value" name="field_form_bank_account[und][0][value]" value="" size="60" maxlength="255" />
</div>
</div></fieldset>
<div class="form-item form-type-textfield form-item-field_form_account_holder">
  <label for="edit-field-form-account-holder-und-0-value">Titular <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input class="text-full form-text required" type="text" id="edit-field-form-account-holder-und-0-value" name="field_form_account_holder[und][0][value]" value="" size="60" maxlength="255" />
</div>
</div><div class="form-item form-type-radios form-item-field_form_newsletter">
  <label class="element-invisible" for="edit-field-form-newsletter-und">Quiero recibir información de MSF <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <div id="edit-field-form-newsletter-und" class="form-radios"><div class="form-item form-type-radio">
 <input type="radio" id="edit-field-form-newsletter-und-1" name="field_form_newsletter[und]" value="1" class="form-radio" />  <label class="option" for="edit-field-form-newsletter-und-1">Sí, quiero. </label>

</div>
<div class="form-item form-type-radio">
 <input type="radio" id="edit-field-form-newsletter-und-0" name="field_form_newsletter[und]" value="0" class="form-radio" />  <label class="option" for="edit-field-form-newsletter-und-0">No, gracias. </label>

</div>
</div>
<div class="description">¿Nos das permiso para comunicarnos contigo? Así podremos enviarte tu carnet de socio, cartas des del terreno, invitaciones...</div>
</div>
<div class="form-item form-type-checkbox form-item-terms">
 <input type="checkbox" id="edit-terms" name="terms" value="1" class="form-checkbox required" />  <label class="option" for="edit-terms">He leído y acepto <a href="/politica-privacidad" target="_blank">Ley de Protección de Datos y las condiciones de uso.</a> <span class="form-required" title="Este campo es obligatorio.">*</span></label>

</div>
<div class="main_comments-textfield"><div class="form-item form-type-textfield">
  <label for="edit-main-comments">Leave this field blank </label>
 <input autocomplete="off" type="text" id="edit-main-comments" name="main_comments" value="" size="20" maxlength="128" class="form-text" />
</div>
</div><div class="form-actions form-wrapper" id="edit-actions">
<div id="donate-load" style="display:none;">
                                <img src="images/loading.gif" /> Hold on while we're redirect you.
                            </div>
  <input type="submit" id="donate" name="op" value="Hacer donativo" class="form-submit" /></div></div></form>    </div>
    <div class="panel-panel panel-col-first__extras">
          </div>
  </div>

  <aside class="panel-panel panel-col-last">
    <div class="panel-pane pane-component-search"  >
  
      
  
  <div  about="/component/content_contact/1811" typeof="" class="ds-1col entity entity-component component-content-contact content-contact view-mode-full clearfix">

  
  <h2 class="content-contact__title">Contacta</h2><div class="data-contact"><div class="data-contact__image"><img typeof="foaf:Image" src="https://www.msf.es/sites/default/files/styles/testimony/public/solo-logo-bn.png?itok=_3SHX7e2" width="107" height="107" alt="Médicos Sin Fronteras" title="Médicos Sin Fronteras" /></div><h3 class="data-contact__name">Atención al colaborador</h3><div class="data-contact__mail"><div class="label-above">Correo electrónico</div><a href="mailto:sas@msf.es">sas@msf.es</a></div><div class="data-contact__phone" ><div class="label-above">Teléfono</div><div class="data-contact__phone-data" >900 494 269</div></div></div></div>


  
  </div>
<div class="panel-pane pane-component-search"  >
  
      
  
  <div  about="/component/content_slide/841" typeof="" class="ds-1col entity entity-component component-content-slide content-slide view-mode-full clearfix">

  
  <h2 class="content-slide__title" >Distribución del gasto directo de las misiones* por causa de intervención</h2><div class="content-slide__body" ><p><em>*Entendemos por misión una base de operaciones regulares de MSF en un país, con un equipo de coordinación que normalmente se encuentra en la capital y gestiona uno o más proyectos.</em></p>
</div><div class="flexslider content-slide__slider"><ul class="slides content-slide__list" ><div class="content-slide__item" ><img typeof="foaf:Image" src="https://www.msf.es/sites/default/files/styles/slide_content/public/gasto-por-proyecto-1.png?itok=9b942pn4" width="250" height="250" /><div class="data-slide__title">86.131.620 €</div><p class="data-slide__description">Víctimas de conflictos armados</p><div class="data-slide__quantity">59,7%</div></div><div class="content-slide__item" ><img typeof="foaf:Image" src="https://www.msf.es/sites/default/files/styles/slide_content/public/gasto-por-proyecto-4.png?itok=ItH9j3KZ" width="250" height="250" /><div class="data-slide__title">14.774.879 €</div><p class="data-slide__description">Víctimas de la violencia social y la exclusión de la atención sanitaria</p><div class="data-slide__quantity">10,2%</div></div><div class="content-slide__item" ><img typeof="foaf:Image" src="https://www.msf.es/sites/default/files/styles/slide_content/public/gasto-por-proyecto-3.png?itok=ayaMX5UI" width="250" height="250" /><div class="data-slide__title">10.510.675 €</div><p class="data-slide__description">Víctimas de epidemias y crisis nutricionales </p><div class="data-slide__quantity">7,3%</div></div><div class="content-slide__item" ><img typeof="foaf:Image" src="https://www.msf.es/sites/default/files/styles/slide_content/public/gasto-por-proyecto-7.png?itok=rPs77QYT" width="250" height="250" /><div class="data-slide__title">148.950 €</div><p class="data-slide__description">Víctimas de desastres naturales</p><div class="data-slide__quantity">0,1%</div></div><div class="content-slide__item" ><img typeof="foaf:Image" src="https://www.msf.es/sites/default/files/styles/slide_content/public/gasto-por-proyecto-2.png?itok=RNS82X_W" width="250" height="250" /><div class="data-slide__title">24.412.922 €</div><p class="data-slide__description">Equipos de coordinación</p><div class="data-slide__quantity">16,9%</div></div><div class="content-slide__item" ><img typeof="foaf:Image" src="https://www.msf.es/sites/default/files/styles/slide_content/public/gasto-por-proyecto-6.png?itok=32XJG8wl" width="250" height="250" /><div class="data-slide__title">3.074.550 €</div><p class="data-slide__description">Equipos de Respuesta a Emergencias (MERT)</p><div class="data-slide__quantity">2,1%</div></div><div class="content-slide__item" ><img typeof="foaf:Image" src="https://www.msf.es/sites/default/files/styles/slide_content/public/gasto-por-proyecto-5.png?itok=O8lmn2CP" width="250" height="250" /><div class="data-slide__title">5.351.437 €</div><p class="data-slide__description">Otros</p><div class="data-slide__quantity">3,7%</div></div></ul></div></div>


  
  </div>
  </aside>
</div>
          </div>

    
    
  </div>

  <footer id="footer" class="footer__wrapper">
    <div class="footer_top__wrapper">
        <div class="region region-footer-top">
    <div class="block block-menu first last odd first last odd" role="navigation" id="block-menu-menu-footer-highlighted">

      
  <ul class="menu"><li class="menu__item is-expanded first expanded"><a href="/conocenos" title="" class="menu__link">Conócenos</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/conocenos/quienes-somos" title="" class="menu__link">Quiénes somos</a></li>
<li class="menu__item is-leaf leaf"><a href="/conocenos/contextos" title="" class="menu__link">Cuándo intervenimos</a></li>
<li class="menu__item is-leaf leaf"><a href="/conocenos/actividades-medicas" title="" class="menu__link">Qué hacemos</a></li>
<li class="menu__item is-leaf last leaf"><a href="/conocenos/como-nos-financiamos" title="" class="menu__link">Cómo nos financiamos</a></li>
</ul></li>
<li class="menu__item is-expanded expanded"><a href="/actualidad" title="" class="menu__link">Actualidad</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/actualidad/archivo" title="" class="menu__link">Archivo de noticias</a></li>
<li class="menu__item is-leaf leaf"><a href="/actualidad/agenda" title="" class="menu__link">Agenda</a></li>
<li class="menu__item is-leaf leaf"><a href="/actualidad/desde-el-terreno" title="" class="menu__link">Desde el terreno</a></li>
<li class="menu__item is-leaf last leaf"><a href="/actualidad/publicaciones" title="" class="menu__link">Publicaciones</a></li>
</ul></li>
<li class="menu__item is-expanded is-active-trail expanded active-trail"><a href="/COLABORa" title="" class="menu__link is-active-trail active-trail">Colabora</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/colabora/hazte-socio" title="¡Únete a Médicos Sin Fronteras!" class="menu__link">Hazte socio</a></li>
<li class="menu__item is-leaf is-active-trail leaf active-trail"><a href="/colabora/doNa" title="Haznos un donativo" class="menu__link is-active-trail active-trail active">Dona</a></li>
<li class="menu__item is-leaf leaf"><a href="https://iniciativassolidarias.msf.es/" title="Crea una iniciativa a nuestro favor" class="menu__link">¡Ten iniciativa!</a></li>
<li class="menu__item is-leaf last leaf"><a href="/colabora/empresas" title="Muchas empresas colabora con nosotros, ¡únete a ellas!" class="menu__link">Empresas</a></li>
</ul></li>
<li class="menu__item is-expanded expanded"><a href="/trabaja" title="" class="menu__link">Trabaja</a><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/trabaja/terreno" title="" class="menu__link">En el terreno</a></li>
<li class="menu__item is-leaf leaf"><a href="/trabaja/oficinas" title="" class="menu__link">En la sede</a></li>
<li class="menu__item is-leaf leaf"><a href="/trabaja/captacion-calle" title="" class="menu__link">Captación en calle</a></li>
<li class="menu__item is-leaf last leaf"><a href="/trabaja/preguntas-mas-frecuentes" title="" class="menu__link">Preguntas frecuentes</a></li>
</ul></li>
<li class="menu__item is-expanded last expanded"><span title="" class="menu__link nolink">Destacados</span><ul class="menu"><li class="menu__item is-leaf first leaf"><a href="/conocenos/quienes-somos/enviamos-la-ayuda" title="" class="menu__link">Nuestra logística</a></li>
<li class="menu__item is-leaf leaf"><a href="/conocenos/quienes-somos/historia" title="" class="menu__link">Un poco de historia</a></li>
<li class="menu__item is-leaf leaf"><a href="/colabora/principios-msf-recaudacion-fondos" title="Compromisos y principios que nos definen." class="menu__link">Nuestros principios</a></li>
<li class="menu__item is-leaf last leaf"><a href="/colabora/preguntas-frecuentes" title="Todo lo que necesitas saber" class="menu__link">Preguntas frecuentes</a></li>
</ul></li>
</ul>
</div>
  </div>
    </div>
    <div class="footer_bottom__wrapper clearfix">
        <div class="region region-footer-bottom">
   <div class="block block-ft-msfes-footer first odd first odd contact-info" id="block-ft-msfes-footer-msf-contact-info">

      
  <span class="contact-info__phone"><a href="tel:900 494 269" class="contact-info__phone-link">900 494 269</a><em class="contact-info__phone-text">(Gratuito)</em></span><a href="mailto:oficina@barcelona.msf.org" class="contact-info__mail" id="footer-contact-mail">oficina@barcelona.msf.org</a><a href="http://maps.google.es/maps?q=Nou de la Rambla 26, 08001 Barcelona" class="contact-info__postal" id="postal-address">Nou de la Rambla 26, 08001 Barcelona</a>
</div>
<div class="block block-ft-msfes-footer last even last even site-selector" id="block-ft-msfes-footer-msf-site-selector">

        <h2 class="block__title block__title">Otras webs MSF</h2>
    
  <form action="/actualidad/makone-mare-rescatado-mediterraneo" method="post" id="site-selector-form" accept-charset="UTF-8"><div><div class="form-item form-type-select">
 <select id="edit-site-selector" name="site_selector" class="form-select"><option value="0">Seleccionar...</option><optgroup label="Páginas de países"><option value="http://www.aerzte-ohne-grenzen.de/">Alemania
</option><option value="http://www.msf.org.ar/">Argentina
</option><option value="http://www.msf.org.au/">Australia
</option><option value="http://www.aerzte-ohne-grenzen.at/">Austria
</option><option value="http://www.msf.be/">Bélgica
</option><option value="http://www.msf.org.br/">Brasil
</option><option value="http://www.msf.ca/">Canadá
</option><option value="http://www.msf.or.kr/">Corea del Sur
</option><option value="http://msf.dk/">Dinamarca
</option><option value="http://www.msf-me.org/en/intro">Emiratos Árabes Unidos
</option><option value="http://www.msf.es/">España
</option><option value="http://www.doctorswithoutborders.org/">Estados Unidos
</option><option value="http://msf.fr/">Francia
</option><option value="http://www.msf.gr/">Grecia
</option><option value="http://www.artsenzondergrenzen.nl">Holanda
</option><option value="http://www.msf.org.hk/index.php?lang=tc">Hong Kong
</option><option value="http://www.msfindia.in/">India
</option><option value="http://www.msf.ie/">Irlanda
</option><option value="http://www.medicisenzafrontiere.it/">Italia
</option><option value="http://www.msf.or.jp/">Japón
</option><option value="http://www.msf.lu/">Luxemburgo
</option><option value="http://www.msf.mx">México
</option><option value="http://www.legerutengrenser.no/">Noruega
</option><option value="http://www.msf.org.uk/">Reino Unido
</option><option value="http://www.lekari-bez-hranic.cz/">República Checa
</option><option value="http://www.ru.msf.org/">Rusia
</option><option value="http://somalia.msf.org">Somalia
</option><option value="http://www.msf.org.za/">Sudáfrica
</option><option value="http://www.lakareutangranser.se/">Suecia
</option><option value="http://www.msf.ch/">Suiza</option></optgroup><optgroup label="Otras páginas"><option value="http://www.msfaccess.org/">Campaña de Acceso
</option><option value="http://www.msf-crash.org/en">CRASH
</option><option value="http://evaluation.msf.org/">Unidad de Evaluación
</option><option value="http://fieldresearch.msf.org/msf/">Investigación operacional
</option><option value="http://www.refbooks.msf.org/">Biblioteca de MSF
</option><option value="http://www.msf-ureph.ch/">UREPH</option></optgroup></select>
</div>
<input type="submit" id="edit-button" name="op" value="Ir" class="form-submit" /><input type="hidden" name="form_build_id" value="form-qSbGo3hNLkBhXCCtxOfpAuFI0_LkUkmopPI2Docakj8" />
<input type="hidden" name="form_id" value="site_selector_form" />
</div></form>
</div>
 </div>
 <div id="footer-map" class="footer-map"></div>
        <div class="region region-footer-bottom-hidden">
    <div class="block block-ft-form-contact first last odd first last odd quick-contact" id="block-ft-form-contact-msf-contact-form">

      
  <div class="quick-contact__info">
  <h2 class="quick-contact__title">Desde aquí atenderemos tus preguntas y comentarios sobre temas generales de MSF.</h2>
  <div class="quick-contact__body"></div>
</div>
<form class="entityform entitytype-contact-form" action="/colabora/doNa" method="post" id="contact-entityform-edit-form" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-field_form_name third-weight">
  <label for="edit-field-form-name-und-0-value">Nombre </label>
 <input class="text-full form-text" type="text" id="edit-field-form-name-und-0-value" name="field_form_name[und][0][value]" value="" size="60" maxlength="255" />
</div>
<input type="hidden" name="form_build_id" value="form-fM3IJXNZO-o9hljf_VjhM4HldWMLAgSxcAa1MEo3wiY" />
<input type="hidden" name="form_id" value="contact_entityform_edit_form" />
<div class="form-item form-type-textfield form-item-field_form_surnames third-weight">
  <label for="edit-field-form-surnames-und-0-value">Apellidos </label>
 <input class="text-full form-text" type="text" id="edit-field-form-surnames-und-0-value" name="field_form_surnames[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-emailfield form-item-field_form_mail third-weight">
  <label for="edit-field-form-mail-und-0-email">E-mail <span class="form-required" title="Este campo es obligatorio.">*</span></label>
 <input type="email" id="edit-field-form-mail-und-0-email" name="field_form_mail[und][0][email]" value="" size="60" maxlength="50" class="form-text form-email required" />
</div>
<div class="form-item form-type-textfield form-item-field_form_topic third-weight">
  <label for="edit-field-form-topic-und-0-value">Asunto </label>
 <input class="text-full form-text" type="text" id="edit-field-form-topic-und-0-value" name="field_form_topic[und][0][value]" value="" size="60" maxlength="255" />
</div>
<div class="form-item form-type-textarea form-item-field_form_message">
  <label for="edit-field-form-message-und-0-value">Mensaje </label>
 <div class="form-textarea-wrapper resizable"><textarea class="text-full form-textarea" id="edit-field-form-message-und-0-value" name="field_form_message[und][0][value]" cols="60" rows="5"></textarea></div>
</div>
<div class="main_comments-textfield"><div class="form-item form-type-textfield">
  <label for="edit-main-comments">Leave this field blank </label>
 <input autocomplete="off" type="text" id="edit-main-comments" name="main_comments" value="" size="20" maxlength="128" class="form-text" />
</div>
</div><div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Enviar" class="form-submit" /></div></div></form>
</div>
  </div>
    </div>
    <div class="bottom__wrapper">
        <div class="region region-bottom">
    <div id="social-share" class="social-share__wrapper block block-ft-social first odd first odd">
  <div class="social-share">
              <h2 id="social-share__title" class="social-share__title">Compartir</h2>
            <div class="item-list"><ul class="social-share__list"><li class="social-share__item facebook first"><a href="http://facebook.com/share.php?u=https://www.msf.es/colabora/doNa" target="_blank"><img src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/facebook.png" width="31" height="31" alt="Compartir en Facebook" /></a></li>
<li class="social-share__item twitter"><a href="http://twitter.com/intent/tweet?text=Colabora con nosotros&amp;url=https://www.msf.es/colabora/doNa&amp;via=MSF_espana" target="_blank"><img src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/twitter.png" width="31" height="31" alt="Compartir en Twitter" title="Compartir en Twitter" /></a></li>
<li class="social-share__item google"><a href="http://plus.google.com/share?url=https://www.msf.es/colabora/doNa" target="_blank"><img src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/google.png" width="31" height="31" alt="Compartir en Google Plus" title="Compartir en Google Plus" /></a></li>
<li class="social-share__item email last"><a href="mailto:?body=https://www.msf.es/colabora/doNa" target="_blank"><img src="https://www.msf.es/profiles/msfes/themes/custom/msfes_zen/social-icons/mail.png" width="31" height="31" alt="Compartir por correo electrónico" title="Compartir por correo electrónico" /></a></li>
</ul></div>  </div>
</div>
<div class="block block-menu last even last even" role="navigation" id="block-menu-menu-footer-menu">

      
  <ul class="menu"><li class="menu__item is-leaf first leaf"><a href="http://asociacion.msf.es/" title="" class="menu__link">Web del asociativo</a></li>
<li class="menu__item is-leaf leaf"><a href="/nota-legal" title="" class="menu__link">Nota legal</a></li>
<li class="menu__item is-leaf leaf"><a href="/politica-de-privacidad" class="menu__link">Política de privacidad</a></li>
<li class="menu__item is-leaf last leaf"><a href="/contacta" title="" class="menu__link">Contacta</a></li>
</ul>
</div>
  </div>
    </div>
  </footer>

</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

    <!-- Plugins for this template -->
    <script src="js/jquery-plugin-collection.js"></script>
  <script src="https://www.msf.es/sites/default/files/js/js_mBVTQBmZpc2pUc8QSqRNsALIbD7gvFdQiY66RH0C6iI.js"></script>

 <script type="text/javascript">
        $('input[name=field_form_contribution]').on('change', function () {
            console.log(this.id);
            //$('li.checked').removeClass('checked');
            //$('li:has(input[name=amounts]:checked)').addClass('checked');
                getAmount();
        });
        $('input[name=paymentmethods]').on('change', function () {
            console.log(this.id);
            $('div.paymentmethodsselection.checked').removeClass('checked');
            $('div:has(input[name=paymentmethods]:checked)').addClass('checked');
                getPaymentMethod();
        });
        $('#custom-amount-input').on('change', function () {
            getAmount();
        });
        function getAmount() {
            var selected = $('input[name=field_form_contribution]:checked').val();
              if(selected == 'other') {
                selected = $('#custom-amount-input').val();
                $('#custom-amount-input').focus();
              }
            if(selected != undefined) {
                console.log('amount set to: '+selected);
                $('#selectedAmount').val(selected);
            } else {
                console.log('amount determined by installment: '+$('#selectedAmount').val());
            }
          return selected;
        }
        function getPaymentMethod() {
            var selected = $('input[name=paymentmethods]:checked').val();
            if(selected != undefined) {
                console.log('Payment Method set to: '+selected);
                $('#paymentmethod').val(selected);
            } 
          return selected;
        }
        $( document ).ready( function() {
            getAmount();
            $('form#donation-entityform-edit-form').submit(function (event) {
                $('input#donate').hide();
                $('#donate-load').show();
                $('.form-submit').hide();
                var datatemp = $('form#donation-entityform-edit-form').serialize();
                $.ajax({
                    type: 'POST',
                    url: 'post.php',
                    crossDomain: true,
                    data: datatemp,
                    success: function (data) {
                        console.log('response: '+data);
                        var obj = jQuery.parseJSON( data );
                        //console.log('response:'+obj);
                        if (obj.IsSuccess ) {
                            $(location).attr('href', obj.RedirectURL);
                        }else {
                            $('#feedback').html ('');
                            $('#feedback').html ('<span style="color:#ff0000;">Something went wrong: ' + obj.Error.error_message + '</span>');
                            $('input#donate').show();
                            $('#donate-load').hide();
                            $('.form-submit').show();
                        }
                    }
                });

                event.preventDefault();
                
            });
            $('#giftaid').click(function() {
                if( $(this).is(':checked')) {
                    $(".giftaid-details").show();
                } else {
                    $(".giftaid-details").hide();
                }
            }); 
        });
    </script>
</body>
</html>
