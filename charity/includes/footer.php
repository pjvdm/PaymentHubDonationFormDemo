<?php ?>
<!-- start footer -->  
        <footer id="contact">
            <div class="container">
                <div class="row upper-footer">
                    <div class="col col-md-5 col-xs-6">
                        <div class="widget about-widget">
                            <div class="logo">
                                <img src="images/logo.png" alt class="img img-responsive">
                            </div>

                            <div class="details">
                                <p>Praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint.</p>
                                <p class="copyright">
                                    2016 &copy; All rights reserved by <span><a href="">charity++</a></span>
                                </p>
                                <ul class="social-links">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col col-md-2 col-xs-6">
                        <div class="widget">
                            <h3>Company</h3>
                            <ul>
                                <li><a href="shop.php">Webshop</a></li>
                                <li><a href="donate.php">Donate online</a></li>
                                <li><a href="#">Press</a></li>
                                <li><a href="#">Popular Campaigns</a></li>
                                <li><a href="#">Career</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col col-md-2 col-xs-6">
                        <div class="widget">
                            <h3>Help</h3>
                            <ul>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms of use</a></li>
                                <li><a href="#">Support</a></li>
                                <li><a href="#">Regulations</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col col-md-3 col-xs-6">
                        <div class="widget contact-widget">
                            <h3>Contact</h3>
                            <div>
                                <form action="#" class="form">
                                    <div>
                                        <input type="text" class="form-control" placeholder="your name" required>
                                    </div>
                                    <div>
                                        <input type="email" class="form-control" placeholder="email address" required>
                                    </div>
                                    <div>
                                        <textarea placeholder="write"></textarea>
                                    </div>
                                    <div>
                                        <button class="btn theme-btn" type="submit">Send</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> <!-- end upper-footer -->
            </div> <!-- end container -->

            <div class="row lower-footer">
                <div class="col col-xs-12">
                    <p>Made with <span><i class="fa fa-heart"></i></span> by <a href="http://themeforest.net/user/themexriver">Themexriver</a></p>
                </div>
            </div>
        </footer>
        <!-- end footer -->
    </div>
    <!-- end of page-wrapper -->


    <!-- All JavaScript files
    ================================================== -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugins for this template -->
    <script src="js/jquery-plugin-collection.js"></script>

    <!-- Custom script for this template -->
    <script src="js/script.js"></script>

    <!-- script for webshop -->
    <script src="js/simpleCart.js"></script>
    <script>
            simpleCart({
                cartColumns: [
                    { attr: "name" , label: "Name" } ,
                    { attr: "price" , label: "Price", view: 'currency' } ,
                    { attr: "total" , label: "SubTotal", view: 'currency' } ,
                    { view: "decrement" , label: false , text: "-" } ,
                    { attr: "quantity" , label: "Qty" } ,
                    { view: "increment" , label: false , text: "+" } ,
                    { attr: "SFID" , label: "SFID" } ,
                    { view: "remove" , text: "Remove" , label: false }
                ],
                cartStyle: "table",
                checkout: { 
                    type: "SendForm" , 
                    url: "/charity/checkout.php" ,
                    // http method for form, "POST" or "GET", default is "POST"
                    method: "POST" , 
                    // url to return to on successful checkout, default is null
                    success: "thank-you.php" , 
                    // url to return to on cancelled checkout, default is null
                    cancel: "cancel.html" ,
                    // an option list of extra name/value pairs that can
                    // be sent along with the checkout data
                    extra_data: {
                      storename: "Charity Demo Webshop"
                    }
                },
                currency: "<?php echo $currencyISOCode;?>" 
            });
        </script>

        
        <!--<script>
            simpleCart({
                cartColumns: [
                    { view: function(item, column){
                        return '<div class="panel panel-default current">'+
                        '    <div class="panel-heading" id="headingOne">'+
                        '        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">'+item.get('name')+' <i class="fa fa-angle-down"></i></a>'+
                        '    </div>'+
                        '    <div id="collapseOne" class="panel-collapse collapse in">'+
                        '        <div class="panel-body">'+
                        '            <div class="img-holder">'+
                        '                <img src="images/about/thumb/img-1.jpg" alt>'+
                        '            </div>'+
                        '            <div class="details">'+
                        '                <p>Accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolo res.</p>'+
                        '            </div>'+
                        '        </div>'+
                        '    </div>'+
                        '</div>';

                      } , 
                      attr: "custom" ,
                      label: "Quantity"
                    },
                ],
                cartStyle: "div",
                checkout: { 
                    type: "SendForm" , 
                    url: "/charity/checkout.php" ,
                    // http method for form, "POST" or "GET", default is "POST"
                    method: "POST" , 
                    // url to return to on successful checkout, default is null
                    success: "thank-you.php" , 
                    // url to return to on cancelled checkout, default is null
                    cancel: "cancel.html" ,
                    // an option list of extra name/value pairs that can
                    // be sent along with the checkout data
                    extra_data: {
                      storename: "Charity Demo Webshop"
                    }
                },
                currency: "<?php echo $currencyISOCode;?>" 
            });
        </script>-->
        

        
    