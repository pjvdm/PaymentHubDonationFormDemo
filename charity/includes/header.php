<?php
    include ('../static.php');
    //print_r($_SESSION);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charity++ Responsive HTML Template">
    <meta name="author" content="themexriver">

    <!-- Page Title -->
    <title>StepOrange Nonprofit Demo</title>

    <!-- Favicon and Touch Icons -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <link href="images/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
    <link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

    <!-- Icon fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/flaticon.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Plugins for this template -->
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/jquery.fancybox.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <style>
    .currency:before {
        content: "<?php echo $currency; ?>";
    }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- start page-wrapper -->
    <div class="page-wrapper home-style-three" id="home">

        <!-- start preloader --> 
        <div class="preloader">
            <div class="middle">
                <i class="fi flaticon-animal"></i>
            </div>
        </div>
        <!-- end preloader -->   

        <!-- Start header -->
        <header id="header" class="header-style-three">
            <div class="topbar"> 
                <div class="upper-topbar">
                    <div class="container">
                        <div class="row">
                            <div class="col col-sm-9">
                                <div class="contact-info">
                                    <ul>
                                        <li><i class="fa fa-map-marker"></i> Orlando, FL 32830, United States</li>
                                        <li><i class="fa fa-mobile"></i> +191 2819 20 01 91</li>
                                        <li><i class="fa fa-envelope-o"></i> contact@charityplus.org</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col col-sm-3">
                                <div class="social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end upper-topbar -->

                <div class="lower-topbar">
                    <div class="container">
                        <div class="row">
                            <div class="col col-md-4">
                                <div class="logo">
                                    <a href="index.html"><img src="images/logo-2.png" alt class="img img-responsive"></a>
                                </div>
                            </div>
                            <div class="col col-md-8">
                                <div class="next-event">
                                    <div class="subject">
                                        <img src="images/cycling.png" alt>
                                        <h5>Charity cycling <span>2018</span></h5>
                                    </div>
                                    <div id="clock"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end lower-topbar -->

            </div> <!-- end top-bar -->

            <nav class="navigation navbar navbar-default" id="main-navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav">
                            <li class="current"><a href="#home">Home</a></li>
                            <li><a href="#urgetn-causes">Urgent causes</a></li>
                            <li><a href="#process">Process</a></li>
                            <li><a href="#recent">Recent</a></li>
                            <li><a href="#about">About</a></li>
                            <li><a href="#events">Events</a></li>
                            <li><a href="shop.php">Shop</a></li>
                            <li><a href="#contact">contact</a></li>
                        </ul>
                    </div><!-- end of nav-collapse -->

                    <div class="navbar-right social-links-mini-cart">
                        <ul class="mini-cart-wrapper">
                            <li>
                                <a href="#" class="mini-cart-btn">
                                    <i class="fi flaticon-bag-1"></i>
                                    <span class="item-count simpleCart_quantity"></span>
                                </a>
                                <ul class="mini-cart">
                                    <!--<li class="item">
                                        <div class="product-img">
                                            <img src="images/shop/thumb/img-1.jpg" alt="">
                                        </div>
                                        <div class="product-details">
                                            <h6>Name of the product</h6>
                                            <p>$255.5</p>
                                            <a href="#"><i class="fi flaticon-garbage"></i></a>
                                        </div>
                                    </li>
                                    <li class="item">
                                        <div class="product-img">
                                            <img src="images/shop/thumb/img-2.jpg" alt="">
                                        </div>
                                        <div class="product-details">
                                            <h6>Name of the product</h6>
                                            <p>$155.5</p>
                                            <a href="#"><i class="fi flaticon-garbage"></i></a>
                                        </div>
                                    </li>-->
                                    
                                    <li class="minicart-price-total">
                                        <div class="price-total">
                                            <span class="label-price-total">Items in cart</span>
                                            <div class="price-total-w">
                                                <span class="simpleCart_quantity"></span>
                                            </div>
                                        </div>
                                        <div class="price-total">
                                            <span class="label-price-total">Subtotal</span>
                                            <div class="price-total-w">
                                                <span class="simpleCart_total"></span>
                                            </div>
                                        </div>

                                        <div class="checkout-btn">
                                            <a class="btn theme-btn" href="/charity/viewCart.php" >View cart</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul> <!-- end mini-cart -->
                    </div>
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->