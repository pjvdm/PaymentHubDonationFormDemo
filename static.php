<?php
session_start();
//overall settings

ini_set('display_errors', 'Off');

define('CONSUMER_KEY', getenv('Consumer_Key'));
define('CONSUMER_SECRET', getenv('Consumer_Secret'));
define('REDIRECT_URI', getenv('Callback_URL'));
define('LOGIN_BASE_URL', getenv('Login_Base_Url'));
define ('REFRESH_TOKEN', getenv('REFRESH_TOKEN'));
define('API_TOKEN', getenv('API_TOKEN'));

define ('CLICK' , 'Click-2 Donate');

//oauth connection to Salesforce
function login () {
	$token_url = LOGIN_BASE_URL.'/services/oauth2/token';
    $post_fields = array(
        'code' => $code,
        'grant_type' => 'refresh_token',
        'client_id' => CONSUMER_KEY,
        'client_secret' => CONSUMER_SECRET,
        'refresh_token' => REFRESH_TOKEN,
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $token_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    
    // Make the API call, and then extract the information from the response
    $token_request_body = curl_exec($ch) 
        or die("1 Call to get token from code failed: '$token_url' - ".print_r($post_fields, true));
    $token_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if (($token_response_code<200)||($token_response_code>=300)||empty($token_request_body))
        die("2 Call to get token from code failed with $token_response_code: '$token_url' - ".print_r($post_fields, true)." - '$token_request_body'");
    $token_request_data = json_decode($token_request_body, true);
    
    return $token_request_data; 	
}

if ($_SESSION['access_token'] == '') {
    $data1 = login();

    $_SESSION['access_token'] = $data1['access_token'];
    $_SESSION['instance_url'] = $data1['instance_url'];    
}
/*
//currency symbol determination
$currency = $_SESSION['currentCurrency'];
$currencyISOCode = $_SESSION['currentCurrencyISOCode'];
if ( $_GET['action'] == 'currencyreset' || $currency == '') {
    $curl = curl_init($_SESSION['instance_url'].'/services/apexrest/v1/Currencies');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, false);
    
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: OAuth '.$_SESSION['access_token'], "Content-type: lication/json"));
    $curl_response = curl_exec($curl);
    if ($curl_response === false) {
        $info = curl_getinfo($curl);
        curl_close($curl);
        die('error occured during curl exec. Additioanl info: ' . var_export($info));
    }
    curl_close($curl);
    $temp = json_decode ( $curl_response );
    if($temp->isSuccess = false) {
       die('error while getting currency: '.$temp->error);
    }
    $currency = $temp->currencyWrapper->symbol_native;
    $_SESSION['currentCurrency'] = $currency;
    $currencyISOCode = $temp->currencyWrapper->code;
    $_SESSION['currentCurrencyISOCode'] = $temp->currencyWrapper->code;
    //echo '<span class="hide">new currency loaded: '.$currency.'</span>';
} else {
    //echo '<span class="hide">currency already loaded: '.$currency.'</span>';
}
*/

//webshop
//


/*
$data1 = login();

$_SESSION['access_token'] = $data1['access_token'];
$instance_url = $data1['instance_url'];
*/
?>