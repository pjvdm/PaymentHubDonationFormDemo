<?php
//define('CONSUMER_KEY', '3MVG9fTLmJ60pJ5JgBmlMvKyiVbk0hv3zm5onnsAxElEP5aRW2bzLJo9aalw7ptayZyoJfFZ.09RLz78nMuPy');
//define('CONSUMER_SECRET', '7323125884054284829');
//define('REDIRECT_URI', 'http://localhost:8888/auth/callback.php');
//define('LOGIN_BASE_URL', 'https://login.salesforce.com');

define('CONSUMER_KEY', getenv('Consumer_Key'));
define('CONSUMER_SECRET', getenv('Consumer_Secret'));
define('REDIRECT_URI', getenv('Callback_URL'));
define('LOGIN_BASE_URL', getenv('Login_Base_Url'));


session_start();

//st be returning us a code that we can exchange for
    // a proper access token
    $code = $_REQUEST['code'];
    // Make our first call to the API to convert that code into an access token that
    // we can use on subsequent API calls
    $token_url = LOGIN_BASE_URL.'/services/oauth2/token';
    $post_fields = array(
        'code' => $code,
        'grant_type' => 'authorization_code',
        'client_id' => CONSUMER_KEY,
        'client_secret' => CONSUMER_SECRET,
        'redirect_uri' => REDIRECT_URI,
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $token_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    
    // Make the API call, and then extract the information from the response
    $token_request_body = curl_exec($ch) 
        or die("Call to get token from code failed: '$token_url' - ".print_r($post_fields, true));
    $token_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if (($token_response_code<200)||($token_response_code>=300)||empty($token_request_body))
        die("Call to get token from code failed with $token_response_code: '$token_url' - ".print_r($post_fields, true)." - '$token_request_body'");
    $token_request_data = json_decode($token_request_body, true);
    if (empty($token_request_data))
        die("Couldn't decode '$token_request_data' as a JSON object");
    if (!isset($token_request_data['access_token'])||
        !isset($token_request_data['instance_url']))
        die("Missing expected data from ".print_r($token_request_data, true));
    // Save off the values we need for future use
    echo "<pre>";
    var_dump($token_request_data); die();
    $_SESSION['access_token'] = $token_request_data['access_token'];
    $_SESSION['instance_url'] = $token_request_data['instance_url'];
?>