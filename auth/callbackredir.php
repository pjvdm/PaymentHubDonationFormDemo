<?php
	session_start();
	$_SESSION['token'] = $_GET['access_token'];
	$_SESSION['id_url'] = $_GET['id'];
	include ('config.php');
	include ('header.php');
	?>
		<i class="fa fa-spinner fa-pulse centered" style="font-size: 100px;"></i>

		<script type="text/javascript">
		$( document ).ready(function() {
			$.ajax({
		        url: "post.php",
		        type: "POST",
		        cache: false,
		        data: "get=profile",
		        success : function(html){
		            location.href = 'index.php';
		        }
		    });
		});
	</script>
	<?
	include ('footer.php');
?>