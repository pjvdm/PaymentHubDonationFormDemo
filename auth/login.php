<?php
ini_set('display_errors', 'Off');

define('CONSUMER_KEY', getenv('Consumer_Key'));
define('CONSUMER_SECRET', getenv('Consumer_Secret'));
define('REDIRECT_URI', getenv('Callback_URL'));
define('LOGIN_BASE_URL', getenv('Login_Base_Url'));
define('API_TOKEN', getenv('API_TOKEN'));
?>
<a href="https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=<?php echo CONSUMER_KEY ?>&redirect_uri=<?php echo REDIRECT_URI ?>">Sign in with salesforce</a>
