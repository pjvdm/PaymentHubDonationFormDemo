<?
	include ('../static.php');
    //echo " Enter the Demo from StepOrange";
?>
<!--DOCTYPE html -->
<html><head>
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<meta name="description" content="Startups template">
	<meta name="keywords" content="Startups template">

	<link rel="shortcut icon" href="assets/img/favicon.ico">

	<link rel="apple-touch-icon" href="assets/img/apple-touch-icon.jpg">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/img/apple-touch-icon-72x72.jpg">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/img/apple-touch-icon-114x114.jpg">

	<link rel="stylesheet" type="text/css" href="assets/css/custom-animations.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style-orange-color.css">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
		<script src=""assets/js/html5shiv.js"></script>
		<script src=""assets/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<div id="page" class="page">

	<div class="bg bg1 window-height align-center light-text form-section lead-generation3 clearfix" style="min-height: 974px; padding-top: 0px; padding-bottom: 0px;">
			<div class="container centered-block">

				<div class="col-sm-6 col-sm-offset-3">
					<h2 class="editContent">JOIN OUR <br /><span class="highlight">Demo</span></h2>
					<p class="editContent" style="margin-bottom: 20px;">This data will only be used for the demo.</p>
					<form class="form lead-form form-light dark-text" method="post" action="" style="padding: 45px 20px 15px;" id="enterform" novalidate="novalidate">
						<div class="form-group">
							<div class="col-sm-3 col-xs-12">
								<label for="first_name" class="text-right-sm">First name </label>
							</div>
							<div class="col-sm-9 col-xs-12">
								<input placeholder="Firstname" id="first_name" type="text" name="FirstName" class="form-control required" aria-required="true" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAfBJREFUWAntVk1OwkAUZkoDKza4Utm61iP0AqyIDXahN2BjwiHYGU+gizap4QDuegWN7lyCbMSlCQjU7yO0TOlAi6GwgJc0fT/fzPfmzet0crmD7HsFBAvQbrcrw+Gw5fu+AfOYvgylJ4TwCoVCs1ardYTruqfj8fgV5OUMSVVT93VdP9dAzpVvm5wJHZFbg2LQ2pEYOlZ/oiDvwNcsFoseY4PBwMCrhaeCJyKWZU37KOJcYdi27QdhcuuBIb073BvTNL8ln4NeeR6NRi/wxZKQcGurQs5oNhqLshzVTMBewW/LMU3TTNlO0ieTiStjYhUIyi6DAp0xbEdgTt+LE0aCKQw24U4llsCs4ZRJrYopB6RwqnpA1YQ5NGFZ1YQ41Z5S8IQQdP5laEBRJcD4Vj5DEsW2gE6s6g3d/YP/g+BDnT7GNi2qCjTwGd6riBzHaaCEd3Js01vwCPIbmWBRx1nwAN/1ov+/drgFWIlfKpVukyYihtgkXNp4mABK+1GtVr+SBhJDbBIubVw+Cd/TDgKO2DPiN3YUo6y/nDCNEIsqTKH1en2tcwA9FKEItyDi3aIh8Gl1sRrVnSDzNFDJT1bAy5xpOYGn5fP5JuL95ZjMIn1ya7j5dPGfv0A5eAnpZUY3n5jXcoec5J67D9q+VuAPM47D3XaSeL4AAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-3 col-xs-12">
								<label for="last_name" class="text-right-sm">Last name </label>
							</div>
							<div class="col-sm-9 col-xs-12">
								<input id="last_name" type="text" name="LastName" class="form-control required" aria-required="true" placeholder="Lastname">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-3 col-xs-12">
								<label for="email" class="text-right-sm">E-mail </label>
							</div>
							<div class="col-sm-9 col-xs-12">
								<input id="email" type="email" name="Email" class="form-control required email" aria-required="true" placeholder="name@domain.ext">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-3 col-xs-12">
								<label for="phone" class="text-right-sm">Mobile </label>
							</div>
							<div class="col-sm-9 col-xs-12">
								<input id="phone" type="tel" name="Phone" class="form-control" placeholder="+316xxxxxxxx">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="submit" id="submitbutton" value="Send" class="btn btn-lg btn-solid btn-block">
							</div>
							<div class="col-sm-12" id="feedback">
							</div>
						</div>
						<input type="hidden" id="Scheme" name="Scheme" value="SEPA"/>
					</form>
				</div>
			</div>
		</div></div><!-- /#page -->

	<!--[if lt IE 9]> <script type="text/javascript" src="assets/js/jquery-1.11.3.min.js?ver=1"></script> <![endif]-->
	<!--[if (gte IE 9) | (!IE)]><!--> <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js?ver=1"></script> <!--<![endif]-->

	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.flexslider-min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mb.YTPlayer.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery-ui-slider.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.appear.js"></script>
	<script type="text/javascript" src="assets/js/toastr.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="assets/js/startuply.js"></script>

	<script type="text/javascript">
	$( document ).ready( function() {
    
    
    	$('form#enterform').submit(function (event) { 
	    	var array = $( 'form#enterform' ).serializeArray();
            var json = {};
            var datatemp = $('form#enterform').serialize();
            $('#feedback').html('');
            $('#submitbutton').attr('disabled', 'disabled');
            $('#submitbutton').addClass('disabled')
            $('#submitbutton').attr('value', 'Please wait');
            $.ajax({
                type: 'POST',
                url: 'signup.php',
                crossDomain: true,
                data: datatemp,
                success: function (data) {
					var obj = jQuery.parseJSON( data );
     	            
     	            if( obj.code == '100' ) {
						$('#submitbutton').attr('value', 'Done!');
						$( '#feedback' ).html( 'Thank you for participating' );
						toastr.success('Thank you for participating');
                    } else {
						$('#submitbutton').attr('value', 'Send');
						$('#submitbutton').removeClass('disabled')
						$('#submitbutton').removeAttr('disabled');
						toastr.error(obj.message);
                    	$( '#feedback' ).html( 'Something went wrong, please try again<br />' + obj.message );
                    }
					
                }
            });

            event.preventDefault();
            
    	});
	});
	</script>
</body></html>